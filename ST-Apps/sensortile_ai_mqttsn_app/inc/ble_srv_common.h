/**
 * Copyright (c)
 *
 */

/** @file
 *
 * @defgroup ble_sdk_srv_common Common service definitions
 * @{
 * @ingroup ble_sdk_srv
 * @brief Constants, type definitions, and functions that are common to all services.
 */

#ifndef BLE_SRV_COMMON_H__
#define BLE_SRV_COMMON_H__

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

 /** @defgroup BLE_GATT_HVX_TYPES GATT Handle Value operations
 * @{ */
#define BLE_GATT_HVX_INVALID               0x00  /**< Invalid Operation. */
#define BLE_GATT_HVX_NOTIFICATION          0x01  /**< Handle Value Notification. */
#define BLE_GATT_HVX_INDICATION            0x02  /**< Handle Value Indication. */
/** @} */

/**@brief Function for decoding a CCCD value, and then testing if notification is
 *        enabled.
 *
 * @param[in]   p_encoded_data   Buffer where the encoded CCCD is stored.
 *
 * @retval      TRUE If notification is enabled.
 * @retval      FALSE Otherwise.
 */
bool ble_srv_is_notification_enabled(uint8_t const * p_encoded_data);


/**@brief Function for decoding a CCCD value, and then testing if indication is
 *        enabled.
 *
 * @param[in]   p_encoded_data   Buffer where the encoded CCCD is stored.
 *
 * @retval      TRUE If indication is enabled.
 * @retval      FALSE Otherwise.
 */
bool ble_srv_is_indication_enabled(uint8_t const * p_encoded_data);

#ifdef __cplusplus
}
#endif

#endif // BLE_SRV_COMMON_H__

/** @} */
