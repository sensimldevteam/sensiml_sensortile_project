/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : sec_debug.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef _SEC_DEBUG_H_
#define _SEC_DEBUG_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


// variables
extern unsigned int fault_depth;



// functions
void save_assert_info(char* file, int line);
void invoke_soft_fault();


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _SEC_DEBUG_H_ */



