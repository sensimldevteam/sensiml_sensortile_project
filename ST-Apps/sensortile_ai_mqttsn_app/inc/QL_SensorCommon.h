/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : QL_SensorCommon.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __QL_SENSOR_COMMON_H__
#define __QL_SENSOR_COMMON_H__

#include "QL_Trace.h"

/*
 * Header File to declare the interfaces that both Clients and Drivers would use.
 * The Sensor IDs used by the Sensor Framework are defined here.
 * The Sensor Framework Initialization API is placed here.
 */

enum
{
	QL_SAL_SENSOR_ID_INVALID,
	QL_SAL_SENSOR_ID_ACCEL,
	QL_SAL_SENSOR_ID_ACCEL2,
	QL_SAL_SENSOR_ID_DOUBLE_TAP,
	QL_SAL_SENSOR_ID_PCG,
	QL_SAL_SENSOR_ID_GYRO,
    QL_SAL_SENSOR_ID_MAG,
    QL_SAL_SENSOR_ID_SENSIML_APP1,
    QL_SAL_SENSOR_ID_SENSIML_APP2
};

#define __PLATFORM_INIT__							

/* Sensor Framework API Init : This *must* be called after all drivers register themselves to the framework */
QL_Status QL_SensorFramework_Init(void);                                                

/*ql_SensorHubTimerCallback declarations*/
QL_Status vSensorHubCallbackInit(unsigned int periodInms);
void vSensorHubChangeTimerPeriod(unsigned int timeInms);
void vSensorHubStartTimer();
void vSensorHubStopTimer();

#endif /*__QL_SENSOR_COMMON_H__*/