/*
 * Copyright (c)
 */


#ifndef BLE_GATT_H__
#define BLE_GATT_H__


#ifdef __cplusplus
extern "C" {
#endif


#define BLE_GATT_STATUS_SUCCESS                           0x00  /**< Success. */
#define BLE_GATT_STATUS_UNKNOWN                           0x01  /**< Unknown or not applicable status. */
#define BLE_GATT_STATUS_ATTERR_INVALID                    0x02  /**< ATT Error: Invalid Error Code. */
#define BLE_GATT_STATUS_ATTERR_INVALID_HANDLE             0x03  /**< ATT Error: Invalid Attribute Handle. */
#define BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED         0x04  /**< ATT Error: Read not permitted. */
#define BLE_GATT_STATUS_ATTERR_WRITE_NOT_PERMITTED        0x05  /**< ATT Error: Write not permitted. */
#define BLE_GATT_STATUS_ATTERR_INVALID_PDU                0x06  /**< ATT Error: Used in ATT as Invalid PDU. */


#endif

/**
  @}
  @}
*/
