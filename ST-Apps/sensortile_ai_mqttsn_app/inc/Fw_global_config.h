/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *
 *    Licensed Materials - Property of QuickLogic Corp.
 *    Copyright (C) 2019 QuickLogic Corporation
 *    All rights reserved
 *    Use, duplication, or disclosure restricted
 *
 *    File   : Fw_global_config.h
 *    Purpose:
 *
 *=========================================================*/

#ifndef FW_GLOBAL_CONFIG_H_INCLUDED     /* Avoid multiple inclusion             */
#define FW_GLOBAL_CONFIG_H_INCLUDED

#include <stdint.h>

#include "stm32l476xx.h"

#define ENABLE_VOICE_SOLUTION   1

#define FEATURE_CLI_DEBUG_INTERFACE  1
#define FEATURE_CLI_FILESYSTEM       1

#define S3AI_FIRMWARE_MODE_RECOGNITION   ('R')
#define S3AI_FIRMWARE_MODE_COLLECTION    ('C')
#define S3AI_FIRMWARE_MODE_none           0

#define USE_FREERTOS_FAT         0  ///< for Merced
#define USE_FATFS                1  ///< for SensorTile
#define USE_IMU_FIFO_MODE        1  ///< 1 => use FIFO for reading sensor data

#if !defined(S3AI_FIRMWARE_MODE)
     /* allow for commandline define for automated builds on Linux */
/* There is not enough RAM to do both - collection & recognition, choose 1 */
//#define S3AI_FIRMWARE_MODE      S3AI_FIRMWARE_MODE_COLLECTION
#define S3AI_FIRMWARE_MODE   S3AI_FIRMWARE_MODE_RECOGNITION
// #define S3AI_FIRMWARE_MODE    S3AI_FIRMWARE_MODE_none
#endif

#define RECOG_VIA_BLE 1 //need to test for SensorTile

#define S3AI_FIRMWARE_IS_COLLECTION   (S3AI_FIRMWARE_MODE==S3AI_FIRMWARE_MODE_COLLECTION)
#define S3AI_FIRMWARE_IS_RECOGNITION  (S3AI_FIRMWARE_MODE==S3AI_FIRMWARE_MODE_RECOGNITION)
/* future may have other modes? */

#if S3AI_FIRMWARE_IS_COLLECTION
//#define DATA_CAPTURE_BUFFER_SIZE_K_BYTES   12 //140
#define HANDLER_STACK_SIZE                 (0x1000)  //leave top 4k for CSTACK to be used by the Interrupt Handlers
#define DATA_CAPTURE_BUFFER_SIZE_K_BYTES   ((SRAM2_SIZE - HANDLER_STACK_SIZE)/1024) //140
#define riff_bytebuffer                    ((uint8_t *)(SRAM2_BASE + HANDLER_STACK_SIZE))
#else
/*
 * In this case, Data collection is disabled
 *
 * Thus #define is "funny lookign" it is 3 english words.
 * If this macro is actually used, those 3 words will cause
 * a syntax error and code will not compile, that is the intent.
 */
#define DATA_CAPTURE_BUFFER_SIZE_K_BYTES  not enabled here
#endif


#if ( S3AI_FIRMWARE_IS_COLLECTION + S3AI_FIRMWARE_IS_RECOGNITION ) > 1
#error "S3AI does not have enough memory to support both at the same time"
#endif


#define uartHandlerUpdate(id,x)

#define DBG_flags_default 1 //  (DBG_FLAG_ble_cmd + DBG_FLAG_sensor_rate+DBG_FLAG_datasave_debug)
#define DBG_FLAGS_ENABLE 1
#if !DBG_FLAGS_ENABLE
#define DBG_flags 0
#else
extern uint32_t DBG_flags;
#endif

#define DBG_FLAG_recog_result   (0x00000001)
#define DBG_FLAG_q_drop         (0x00000002)
#define DBG_FLAG_ble            (0x00000004)
#define DBG_FLAG_ble_cmd        (0x00000008)
#define DBG_FLAG_ble_background (0x00000010)
#define DBG_FLAG_datasave_debug (0x00000020)
#define DBG_FLAG_ble_details    (0x00000040)
#define DBG_FLAG_data_collect   (0x00000080)
#define DBG_FLAG_sensor_rate    (0x00000100)
#define DBG_FLAG_ffe            (0x00000100)
#define DBG_FLAG_adc_task       (0x00000200)

#define QAI_CHILKAT  0

extern const char *SOFTWARE_VERSION_STR;

#define UUID_TOTAL_BYTES     16
extern uint8_t DeviceClassUUID[UUID_TOTAL_BYTES];

extern int FPGA_FFE_LOADED;
void wait_ffe_fpga_load(void);

#define ENABLE_PRINTF 1

/** Enable Host mode for Voice application, This standalone S3 mode */

#define HOST_VOICE  1


/** Define COMPANION_SENSOR macro to configure system in CO-PROCESSOR mode for double tap application */
//#define COMPANION_SENSOR

/** Define HOST_SENSOR macro to configure system in HOST mode  */
#define HOST_SENSOR 1

#if !defined(ENABLE_PRINTF)
#include <stdio.h> /* we require printf() to be defined first */
#define printf(x, ...) 	/* Uart is disabled in CO-PROCESSOR_VOICE for power numbers */
#endif

#define QL_LOG_INFO_150K(X,...)   printf(X,##__VA_ARGS__)
#define	QL_LOG_DBG_150K(X,...)	  printf(X,##__VA_ARGS__)
#define	QL_LOG_ERR_150K(X,...)	  printf(X,##__VA_ARGS__)
#define	QL_LOG_WARN_150K(X,...)	  printf(X,##__VA_ARGS__)
#define	QL_LOG_TEST_150K(X,...)	  printf(X,##__VA_ARGS__)

#define IMU_M4_DRIVERS     1  ///< enable IMU sensors (Accel, Gyro, ...)
                              ///< using M4 driver to probe and collect data from
                              ///< the sensors

#define IMU_FFE_DRIVERS    0  ///< option to enable IMU sensor data collection
                              ///< using onchip FFE (availble on EOS-S3 only)

#define IMU_DRIVERS        (IMU_M4_DRIVERS || IMU_FFE_DRIVERS)

/* enable via sw the FFE or disable it, TODO: Make this real instead of a hack */
#define SW_ENABLE_FFE   1
/* enable via sw the AUDIO or disable it, TODO: Make this real instead of a hack */
#define SW_ENABLE_AUDIO 1
/* enable the FFE or not, see SW_ENABLE_FFE only 1 should exist */
#define FFE_DRIVERS  	(IMU_FFE_DRIVERS)

/* do or do not perform dynamic frequency scaling */
#define CONST_FREQ	1

/* enable the LTC1859 driver */
#define LTC1859_DRIVER  0    ///< not supported for SensorTile

/* enable the AUDIO driver */
#define AUDIO_DRIVER    1    // Set 1 to enable audio sampling

/* if 0 load from SPI, if 1 load FFE/FPGA from SD card */
#define LOAD_FROM_SD    0

//Need these to support FIFO based sensor data
//#define FFE_MIN_TICK_PERIOD_MS 10  // FFE tick time period minimum in milli seconds
//#define FFE_MAX_TICK_PERIOD_MS 200 // FFE time period maximum in milli seconds
//#define MIN_SUPPORTED_SENSOR_ODR 24
//#define MAX_SUPPORTED_SENSOR_ODR 416


#define EOSS3_ASSERT( x )
#define SPI_CHANGES_0921        1   //All the new SPI changes
#define SENSOR_USE_FFE_I2C	1 //snamy.jeong@160523 using FFE i2c for sensor
#define SENSOR_BATCH_ENABLE	1

#if 0
/* 32 events are available for each task ------------------------------------*/
#define EVENT_READY             (1 << 0) //(1)
#define EVENT_WAIT_SHUB         (1 << 1) //(2)
#define EVENT_LED_DONE			(1 << 2) //(4)
#define EVENT_MY_EVENT1         (1 << 3) //(8)
#define EVENT_MY_EVENT2         (1 << 4) //(16)
#define EVENT_MY_EVENT3         (1 << 5) //(32)
#endif

#define FFE_BATCHSENSOR_MEMORY_MAP 123456

#define MQTTSN_OVER_UART    1 //current implementation is over UART
#define MQTTSN_OVER_BLE     (RECOG_VIA_BLE) //currently use BLE once Recogonition

#if (MQTTSN_OVER_UART == 1)
#define USE_FPGA_UART       0
#define FEATURE_FPGA_UART   1
//#define MQTTSN_UART         UART_ID_FPGA //UART_ID_HW  //Use debug UART for now
#endif

#define QLFS_NO_SPI_FLASH   1

#define SKIP_SUBSCRIBING    0  //debug only
#define DEFAULT_STORAGE_LOCATION    FREERTOS_SPI_SD

#define MQTTSN_UART      3 //just a number for sensortile   UART_ID_FPGA //UART_ID_HW  //Use debug UART for now

#if 1 //sensorTile has only SD card //QAI_CHILKAT
#define NUM_SUPPORTED_PATHS 1
#else
#define NUM_SUPPORTED_PATHS 2
#endif //QAI_CHILKAT


#if 0
#define ASSP_UARTTx uarttx
#define ASSP_UARTRx uartrx
#define assp_uartHandlerUpdate uartHandlerUpdate
#define assp_uartInit uartInit
#define ASSP_uart_read uart_read
#define ASSP_fillRxBuf fillRxBuf
#define ASSP_getRxBuf getRxBuf
#define ASSP_getRxBufSize getRxBufSize
#endif

/* Define this flag to Enable Internal LDO. If undefined, internal LDO will be disabled.*/
#define ENABLE_INTERNAL_LDO   1


/********************/

/* max frequency is 1600hz, we give a bit more just in case */
/* report rate is 10mSec, or 100 times per second */
#define SENSIML_FFE_MAX_DATARATE        1600
#define SENSIML_FFE_REPORT_RATE          100
#define SENSIML_FFE_MAX_BATCH_DATA_SZ ((SENSIML_FFE_MAX_DATARATE/ SENSIML_FFE_REPORT_RATE)+2)
#define SENSIML_FFE_MIN_BATCH_DATA_SZ (6)





/* this should always be the last #define in this file */
/* it insures that we have completely processed this entire file */
#define _EnD_Of_Fw_global_config_h  1


#endif
