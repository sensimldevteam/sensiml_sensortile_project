#if !defined(SENSOR_CMDS_H)
#define SENSOR_CMDS_H

#include "cli.h"

/* list of cli commands to simulate ble commands */
extern const struct cli_cmd_entry sensor_cmds[] ;

/* we have constructed the packet
 * our next step is to pretend that we have received the SPI packet.
 */
extern void do_dispatch( uint32_t numBytes );

#endif
