/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : mqttsn_uart.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

extern void mqttsn_uart_setup(void);
extern void mqttsn_uart_tx(const uint8_t *buf, int len);
extern int mqttsn_uart_rx_available(void);
extern int mqttsn_uart_rx(uint8_t *pBuf, int n);