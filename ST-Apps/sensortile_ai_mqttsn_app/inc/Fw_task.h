/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : Fw_task.h
 *    Purpose: 
 *                                                          
 *=========================================================*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FW_TASK_H
#define __FW_TASK_H

#if !defined( _EnD_Of_Fw_global_config_h )
#error "Include Fw_global_config.h first"
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
//#include <stdio.h>
//#include <qlsh_commands.h>

/* Exported types ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

/* FIXME: Get rid of the uint8 and use uint8_t instead */
typedef uint8_t   uint8;
typedef int8_t     sint8;
typedef uint16_t uint16;
typedef int16_t          sint16;
typedef uint32_t   uint32;
typedef int32_t       sint32;

/* Priority for each tasks ---------------------------------------*/
/* Higher values indicate higher priorities. */




#define FFE_TASK_STACK_SIZE	( configMINIMAL_STACK_SIZE )
#define SYS_TASK_STACK_SIZE DO NOT USE THIS



#endif /* __TASK_H */


