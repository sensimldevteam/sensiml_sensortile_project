/*
 * Sensor_Attributes.c
 *
 *  Copyright (c) 2018 QuickLogic Corporation.  All rights reserved.
 *
 *  All rights reserved
 *
 */
#include "Fw_global_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "Sensor_Attributes.h"
#include "SensorTile_motion_sensors.h"
#include "SensorTile_motion_sensors_ex.h"
   
#if    ( IMU_M4_DRIVERS && defined(STM32L476xx) ) // implementation for STM32L476xx

extern void dataTimerStart(void);
extern void dataTimerStop(void);

static int s_accel_state = 0;
static int s_gyro_state = 0;

static int16_t s_accel_range = 2; // indicates 2g, 4g, 8g or 16g for full-scale value
static int16_t s_gyro_range = 2000; // indicates 2000, 1000, 500, 250, 125 for full-scale value

static void Control_Sensor(unsigned int sensor_id, int enable)
{
  /* Set sensor ODR. */
  int32_t ret, sid;
  
  if (sensor_id == QL_SAL_SENSOR_ID_ACCEL) {
    sid = MOTION_ACCELERO;
  }
  else if (sensor_id == QL_SAL_SENSOR_ID_GYRO) {
     sid = MOTION_GYRO;
  }
  else
    return ;

  if (enable) 
  {
    ret = BSP_MOTION_SENSOR_Enable(LSM6DSM_0, sid);
    dataTimerStart();
  }
  else
  {
    ret = BSP_MOTION_SENSOR_Disable(LSM6DSM_0, sid);
    dataTimerStop();
  }
  
  if (ret == BSP_ERROR_NONE)
  {
     if (sid == MOTION_ACCELERO) s_accel_state = enable;
     else if (sid == MOTION_GYRO) s_gyro_state = enable;
     return ;
  }
  else
    return ;
}

static void Set_Sensor_State(unsigned int sensor_id, unsigned state)
{
   Control_Sensor(sensor_id, state);
}

static int Get_Sensor_State(unsigned int sensor_id)
{
  if (sensor_id == QL_SAL_SENSOR_ID_ACCEL)
    return s_accel_state;
  else if (sensor_id == QL_SAL_SENSOR_ID_GYRO)
    return s_gyro_state;
  else
    return 0;
}
    
static QL_Status Set_Sensor_ODR(unsigned int sensor_id, unsigned int odr_val)
{
  /* Set sensor ODR. */
  int32_t ret, sid;
  
  if (sensor_id == QL_SAL_SENSOR_ID_ACCEL) 
    sid = MOTION_ACCELERO;
  else if (sensor_id == QL_SAL_SENSOR_ID_GYRO)
     sid = MOTION_GYRO;
  else
    return QL_STATUS_ERROR;

  ret = BSP_MOTION_SENSOR_SetOutputDataRate(LSM6DSM_0, sid, (float)odr_val);
  if (ret == BSP_ERROR_NONE)
     return QL_STATUS_OK;
  else
    return QL_STATUS_ERROR;
}

QL_Status Set_AccelGyro_SensorEnable( int enable )
{
  Set_Sensor_State( QL_SAL_SENSOR_ID_ACCEL, enable );
  Set_Sensor_State( QL_SAL_SENSOR_ID_GYRO , enable );
  return QL_STATUS_OK;
}


QL_Status Set_AccelGyro_SensorODR( unsigned int odr_val)
{
  QL_Status ret = QL_STATUS_ERROR;

  unsigned int accel_state = 0;
  
#if (USE_IMU_FIFO_MODE)
  extern int disable_lsm6dsm_stream(void);
  uint8_t fifo_mode = (uint8_t) disable_lsm6dsm_stream();
  dataTimerStop();
#endif

  accel_state = Get_Sensor_State(QL_SAL_SENSOR_ID_ACCEL);
  if(ENABLE_SENSOR == accel_state)
  {
      ret = Set_Sensor_ODR(QL_SAL_SENSOR_ID_ACCEL, odr_val);
  }

  unsigned int gyro_state = 0;
  gyro_state = Get_Sensor_State(QL_SAL_SENSOR_ID_GYRO);
  if(ENABLE_SENSOR == gyro_state)
  {
      ret = Set_Sensor_ODR(QL_SAL_SENSOR_ID_GYRO, odr_val);
  }

#if (USE_IMU_FIFO_MODE)
  if (BSP_ERROR_NONE != BSP_MOTION_SENSOR_FIFO_Set_ODR_Value(LSM6DSM_0, odr_val))
  {
    return ret;
  }
  if (fifo_mode) {
    dataTimerStart();
  }
#endif

  return ret;
}

static int32_t accel_ranges[] = { 2, 4, 8, 16 };
static int32_t gyro_ranges[] = {2000, 1000, 500, 250, 125 };
QL_Status Set_Sensor_Range(unsigned int sensor_id, unsigned int range_index)
{
  int32_t ret, sid;
  int32_t full_scale_range ;
  
  if (sensor_id == QL_SAL_SENSOR_ID_ACCEL) {
    if (range_index > 3)
      range_index = 3;
    full_scale_range = accel_ranges[range_index];
    s_accel_range = full_scale_range;
    sid = MOTION_ACCELERO;
  }
  else if (sensor_id == QL_SAL_SENSOR_ID_GYRO) {
    if (range_index > 4)
      range_index = 4;
    full_scale_range = gyro_ranges[range_index];  
    s_gyro_range = full_scale_range;  
    sid = MOTION_GYRO;
  }
  else
    return QL_STATUS_ERROR;

  ret = BSP_MOTION_SENSOR_SetFullScale(LSM6DSM_0, sid, (int32_t)full_scale_range);
  if (ret == BSP_ERROR_NONE)
     return QL_STATUS_OK;
  else
    return QL_STATUS_ERROR;
}

QL_Status Get_Sensor_ODR(unsigned int sensor_id, unsigned int* odr_val)
{
  int32_t ret, sid;
  float f_odr_val = 0.f;
  
  if (sensor_id == QL_SAL_SENSOR_ID_ACCEL) 
    sid = MOTION_ACCELERO;
  else if (sensor_id == QL_SAL_SENSOR_ID_GYRO)
     sid = MOTION_GYRO;
  else
    return QL_STATUS_ERROR;

  ret = BSP_MOTION_SENSOR_GetOutputDataRate(LSM6DSM_0, sid, &f_odr_val);
  
  int k, i_odr_val = (int)f_odr_val;
  if (sid == MOTION_ACCELERO) {
    for (k = 0; k < 4; k++) {
      if (i_odr_val >= accel_ranges[k]) {
        break;
      }
    }
    *odr_val = k;
  } else if (sid == MOTION_GYRO)
  {
    for (k = 4; k >= 0; k--) {
      if (i_odr_val >= gyro_ranges[k]) {
        break;
      }
    }
    *odr_val = k;
  }
  if (ret == BSP_ERROR_NONE)
     return QL_STATUS_OK;
  else
    return QL_STATUS_ERROR;
}

QL_Status Get_Sensor_Range(unsigned int sensor_id, unsigned int* range)
{
  int32_t ret, sid;
  
  if (sensor_id == QL_SAL_SENSOR_ID_ACCEL) 
    sid = MOTION_ACCELERO;
  else if (sensor_id == QL_SAL_SENSOR_ID_GYRO)
     sid = MOTION_GYRO;
  else
    return QL_STATUS_ERROR;

  ret = BSP_MOTION_SENSOR_GetFullScale(LSM6DSM_0, sid, (int32_t *)range);
  if (ret == BSP_ERROR_NONE)
     return QL_STATUS_OK;
  else
    return QL_STATUS_ERROR;
}

QL_Status Sensor_Enable(unsigned int sensor_id, int enable)
{
  QL_Status ret = QL_STATUS_OK;
  if(QL_SAL_SENSOR_ID_ACCEL == sensor_id)
  {
    Control_Sensor(QL_SAL_SENSOR_ID_ACCEL, enable);
    if(DISABLE_SENSOR == enable)
    {
      /* If Accel disable is requested,
       * Disable Gyro also(if it is enabled). */
      unsigned int gyro_state = 0;
      gyro_state = Get_Sensor_State(QL_SAL_SENSOR_ID_GYRO);
      if(ENABLE_SENSOR == gyro_state)
          Control_Sensor(QL_SAL_SENSOR_ID_GYRO, DISABLE_SENSOR);
    }
  }
  else if(QL_SAL_SENSOR_ID_GYRO == sensor_id)
  {
    if(ENABLE_SENSOR == enable)
    {
       /* Check for Accel state, if it is enabled,
        * then only enable Gyro. If not, return Error */
       unsigned int accel_state = 0;
       accel_state = Get_Sensor_State(QL_SAL_SENSOR_ID_ACCEL);

       if(ENABLE_SENSOR == accel_state)
          Control_Sensor(QL_SAL_SENSOR_ID_GYRO, enable); //Accel is enabled, so enabling Gyro also.
       else
          return QL_STATUS_ERROR; //Accel is not active, Gyro can not be enabled alone. So, Returing Error.
    }
  }
  return ret;
}

int16_t get_accel_range(void)
{
  return s_accel_range * 980; // in 10's of milli-g
}

int16_t get_gyro_range(void)
{
  return s_gyro_range; // in degrees-per-second (DPS)
}

#endif /* ( IMU_M4_DRIVERS && defined(STM32L476xx) ) */
