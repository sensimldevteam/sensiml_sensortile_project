#include "Fw_global_config.h"
#include "cmsis_os.h"
#include "datalog_application.h"
#include "iop_messages.h"
/* Private define ------------------------------------------------------------*/

extern void dataTimer_Callback(void const *arg);
extern void dataTimerStart(void);
extern void dataTimerStop(void);
extern void Error_Handler(void);

extern int32_t LSM6DSM_Sensor_IO_ITConfig( void );

extern void imu_sensordata_read_callback(void);

/**
 * @brief  Configures sensor interrupts interface for LSM6DSM sensor.
 * @param  None
 * @retval BSP_ERROR_NONE in case of success
 * @retval COMPONENT_ERROR in case of failure
 */
int32_t LSM6DSM_Sensor_IO_ITConfig( void )
{

  /* At the moment this feature is only implemented for LSM6DSM */
  GPIO_InitTypeDef GPIO_InitStructureInt2;
  
  /* Enable INT2 GPIO clock */
  BSP_LSM6DSM_INT2_GPIO_CLK_ENABLE();
  
  /* Configure GPIO PINs to detect Interrupts */
  GPIO_InitStructureInt2.Pin = BSP_LSM6DSM_INT2;
  GPIO_InitStructureInt2.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStructureInt2.Speed = GPIO_SPEED_FAST;
  GPIO_InitStructureInt2.Pull  = GPIO_NOPULL;
  HAL_GPIO_Init(BSP_LSM6DSM_INT2_GPIO_PORT, &GPIO_InitStructureInt2);
  
  /* Enable and set EXTI Interrupt priority */
  HAL_NVIC_SetPriority(BSP_LSM6DSM_INT2_EXTI_IRQn, 0x08, 0x00);
  HAL_NVIC_EnableIRQ(BSP_LSM6DSM_INT2_EXTI_IRQn);
  
  return BSP_ERROR_NONE;
}

#include "datablk_mgr.h"
#include "process_ids.h"
#include "SensorTile_motion_sensors.h"

QAI_DataBlock_t  *pimu_data_block_prev = NULL;
int              imu_samples_collected = 0;

extern outQ_processor_t imu_isr_outq_processor;
#define  IMU_ISR_EVENT_NO_BUFFER  (1)   ///< error getting a new datablock buffer

void set_first_imu_data_block()
{
    /* Acquire an audio buffer */
  if (NULL == pimu_data_block_prev) 
  {
    datablk_mgr_acquire(imu_isr_outq_processor.p_dbm, &pimu_data_block_prev, 0);
  }
    configASSERT(pimu_data_block_prev); // probably indicates uninitialized datablock manager handle
    imu_samples_collected = 0;
  pimu_data_block_prev->dbHeader.Tstart = xTaskGetTickCount();
}

void imu_event_notifier(int pid, int event_type, void *p_event_data, int num_data_bytes)
{
  char *p_data = (char *)p_event_data;
  printf("[IMU Event] PID=%d, event_type=%d, data=%02x\n", pid, event_type, p_data[0]);
}

extern int imu_batch_size_get(void);
extern int imu_get_accel_odr(void);
extern int imu_get_gyro_odr(void);
extern int16_t get_accel_range(void);
extern int16_t get_gyro_range(void);

/* This function performs raw data to engineering value conversion.
 * Scale Accelerometer samples map such that samples units are in
 * centi-meter/sec^2 and the resulting 16-bit value accommodates
 * the case where the sensor range is 16G.
 *
 * The table below gives an example for various full-scale ranges.
 * 
 * |  Sensor   | Accel    |   Accel   |   Accel      | 
 * |  Range    | Value    | ADC Value | Scaled Value |
 * |           | (m/sec^2)|           | (cm /sec^2)  |
 * | --------- | -------- | --------- | ------------ |
 * |  +/-  2G  |   9.8    |   16384   |    980       |
 * |  +/-  4G  |   9.8    |    8192   |    980       |
 * |  +/-  8G  |   9.8    |    4096   |    980       |
 * |  +/- 16G  |   9.8    |    2048   |    980       |
 * 
 */
static void adjust_accel(BSP_MOTION_SENSOR_AxesRaw_t *p_accel_data)
{
    int32_t temp;
    int32_t s;
    short r;
 
    r = get_accel_range();
    
    s = p_accel_data->x;
    temp = (s * r) / 32768;
    p_accel_data->x = (short)temp;

    s = p_accel_data->y;
    temp = (s * r) / 32768;
    p_accel_data->y = (short)temp;

    s = p_accel_data->z;
    temp = (s * r) / 32768;
    p_accel_data->z = (short)temp;
}

/* Scale Gyro samples such that samples map to +/- 2000 dps
 * full-scale range. The table below gives an example for
 * various full-scale ranges.
 * 
 * |  Sensor   |   Gyro    |   Gyro    |   Gyro       | 
 * |  Range    | DPS Value | ADC Value | Scaled Value |
 * | --------- | --------- | --------- | ------------ |
 * |  +/- 2000 | 123.3125  |   2020    |    2020      |
 * |  +/- 1000 | 123.3125  |   4041    |    2020      |
 * |  +/-  500 | 123.3125  |   8081    |    2020      |
 * |  +/-  250 | 123.3125  |  16163    |    2020      |
 * |  +/-  125 | 123.3125  |  32326    |    2020      |
 * 
 */
static void adjust_gyro(BSP_MOTION_SENSOR_AxesRaw_t *p_gyro_data)
{
    int32_t temp;
    int32_t s;
    short r;
    int   shift = 0;
 
    r = get_gyro_range();
    switch (r) {
      case 2000: shift = 0; break;
      case 1000: shift = 1; break;
      case  500: shift = 2; break;
      case  250: shift = 3; break;
      case  125: shift = 4; break;
    }
    
    s = p_gyro_data->x;
    temp = (s >> shift);
    p_gyro_data->x = (short)temp;

    s = p_gyro_data->y;
    temp = (s >> shift);
    p_gyro_data->y = (short)temp;

    s = p_gyro_data->z;
    temp = (s >> shift);
    p_gyro_data->z = (short)temp;
  return ;
}

#if (USE_IMU_FIFO_MODE)
extern int32_t ql_lsm6dsm_read_reg(uint8_t reg, uint8_t *data, uint16_t len);

extern int16_t fifo_samples[];

uint32_t imu_fifo_samples_to_ticks(int num_fifo_samples)
{
   int fifo_odr = imu_get_accel_odr();
   return (configTICK_RATE_HZ * num_fifo_samples) / (6 * fifo_odr);
}

int  imu_sensordata_buffer_ready()
{
    int16_t *p_dest = (int16_t *) &pimu_data_block_prev->p_data[sizeof(int16_t)*imu_samples_collected];
    uint8_t fifo_status[4];
    int batch_size;

    int32_t err;
    uint16_t num_fifo_samples;
    
    batch_size = imu_batch_size_get() * 6;
    
    err = BSP_MOTION_SENSOR_FIFO_Get_Num_Samples(LSM6DSM_0, &num_fifo_samples);
    if (err != BSP_ERROR_NONE)
    {
      // configASSERT(err == BSP_ERROR_NONE); // error should be taken care
      return 0;
    }
    
    uint32_t t_end = xTaskGetTickCount();
    uint32_t t_start = t_end - imu_fifo_samples_to_ticks(num_fifo_samples);

    err = ql_lsm6dsm_read_reg(LSM6DSM_FIFO_STATUS1, (uint8_t *)fifo_status, 4);
    if (err != BSP_ERROR_NONE)
    {
      // configASSERT(err == BSP_ERROR_NONE); // error should be taken care
      return 0;
    }
    
    // set the drop count if an overrun occurred
    if (fifo_status[1] & 0x40) {
       pimu_data_block_prev->dbHeader.numDropCount++;
       num_fifo_samples = batch_size;
    }

    num_fifo_samples /= 6;
    num_fifo_samples *= 6;

    /* read batch_size samples from the FIFO */
    if (num_fifo_samples >= batch_size) {
       num_fifo_samples = batch_size;
    }
    else {
      num_fifo_samples = 0;
      return 0;
    }

    err = ql_lsm6dsm_read_reg(LSM6DSM_FIFO_DATA_OUT_L, (uint8_t *)fifo_samples, num_fifo_samples*2) ;
    if (err != BSP_ERROR_NONE)
    {
       // configASSERT(err == BSP_ERROR_NONE); // error should be taken care
       return 0;
    }
    t_end = t_start + imu_fifo_samples_to_ticks(num_fifo_samples);

    for (int k = 0; k < num_fifo_samples; k += 6) {
       /* LSM6DSM FIFO stores Gyroscope data first and then Accelerometer data
        * IMU datablock processor assumes Accelerometer data first and then
        * Gyroscope data.
        */
      
       /* Copy Gyroscope samples to IMU datablock */
       p_dest[3] = fifo_samples[k+0]; // Gyroscope X co-ordinate
       p_dest[4] = fifo_samples[k+1]; // Gyroscope Y co-ordinate
       p_dest[5] = fifo_samples[k+2]; // Gyroscope Z co-ordinate

       /* Copy Accelerometer samples to IMU datablock */
       p_dest[0] = fifo_samples[k+3]; // Accelerometer X co-ordinate
       p_dest[1] = fifo_samples[k+4]; // Accelerometer Y co-ordinate
       p_dest[2] = fifo_samples[k+5]; // Accelerometer Z co-ordinate
       
       // Apply scaling to the raw accel data samples
       adjust_accel((BSP_MOTION_SENSOR_AxesRaw_t *)(p_dest+0));

       // Apply scaling to the raw gyro data samples
       adjust_gyro((BSP_MOTION_SENSOR_AxesRaw_t *)(p_dest+3));

       p_dest += 6;
       imu_samples_collected += 6;
    }

    pimu_data_block_prev->dbHeader.numDataElements = imu_samples_collected;
    pimu_data_block_prev->dbHeader.numDataChannels = 6;
    pimu_data_block_prev->dbHeader.Tend = t_end;
    pimu_data_block_prev->dbHeader.Tstart = t_start;
    imu_samples_collected = 0;
    return 1;
}
#else
int  imu_sensordata_buffer_ready()
{
    BSP_MOTION_SENSOR_AxesRaw_t *p_dest = (BSP_MOTION_SENSOR_AxesRaw_t *) &pimu_data_block_prev->p_data[sizeof(int16_t)*imu_samples_collected];
    int ret;
    uint8_t accel_status, gyro_status;
    int batch_size;
    
    if (BSP_MOTION_SENSOR_Get_DRDY_Status(LSM6DSM_0, MOTION_ACCELERO, &accel_status) == BSP_ERROR_COMPONENT_FAILURE)
      return 0;
      
    if (BSP_MOTION_SENSOR_Get_DRDY_Status(LSM6DSM_0, MOTION_GYRO, &gyro_status) == BSP_ERROR_COMPONENT_FAILURE)
      return 0;
   
    if ((accel_status == 0) || (gyro_status == 0))
    {
      return 0;
    }
    /* Get Data from Sensors */  
    if ( BSP_MOTION_SENSOR_GetAxesRaw(LSM6DSM_0, MOTION_ACCELERO, p_dest ) == BSP_ERROR_COMPONENT_FAILURE )
    {
        p_dest->x = 0;
        p_dest->y = 0;
        p_dest->z = 0;
        ret = BSP_ERROR_COMPONENT_FAILURE;
    }
    if (ret == BSP_ERROR_COMPONENT_FAILURE)
       pimu_data_block_prev->dbHeader.numDropCount = 1;
    
    // Apply scaling to the raw accel data samples
    adjust_accel(p_dest);
    
    p_dest++;
    if ( BSP_MOTION_SENSOR_GetAxesRaw(LSM6DSM_0, MOTION_GYRO, p_dest ) == BSP_ERROR_COMPONENT_FAILURE )
    {
        p_dest->x = 0;
        p_dest->y = 0;
        p_dest->z = 0;
        ret = BSP_ERROR_COMPONENT_FAILURE;
    }

    if (ret == BSP_ERROR_COMPONENT_FAILURE)
       pimu_data_block_prev->dbHeader.numDropCount = 1;
    
    // Apply scaling to the raw gyro data samples
    adjust_gyro(p_dest);

    imu_samples_collected += 6;
    batch_size = imu_batch_size_get() * 6;
    
    if (imu_samples_collected >= batch_size) //  pimu_data_block_prev->dbHeader.numDataElements
    {
      pimu_data_block_prev->dbHeader.numDataElements = imu_samples_collected;
      pimu_data_block_prev->dbHeader.numDataChannels = 6;
      imu_samples_collected = 0;
      return 1;
    } 
    else
    {
      return 0;
    }
}
#endif

void imu_sensordata_read_callback(void)
{
    int gotNewBlock = 0;
    QAI_DataBlock_t  *pdata_block = NULL;
  
    if (!imu_sensordata_buffer_ready())
    {
      return;
    }
    /* Acquire a new data block buffer */
    datablk_mgr_acquire(imu_isr_outq_processor.p_dbm, &pdata_block, 0);
    if (pdata_block)
    {
        gotNewBlock = 1;
    }
    else
    {
        // send error message 
        // xQueueSendFromISR( error_queue, ... )
        if (imu_isr_outq_processor.p_event_notifier)
          (*imu_isr_outq_processor.p_event_notifier)(imu_isr_outq_processor.in_pid, IMU_ISR_EVENT_NO_BUFFER, NULL, 0);
        pdata_block = pimu_data_block_prev;
        pdata_block->dbHeader.Tstart = xTaskGetTickCount();
        pdata_block->dbHeader.numDropCount++;
    }

    if (gotNewBlock)
    {
        /* send the previously filled audio data to specified output Queues */     
        pimu_data_block_prev->dbHeader.Tend = pdata_block->dbHeader.Tstart;
        datablk_mgr_WriteDataBufferToQueues(&imu_isr_outq_processor, NULL, pimu_data_block_prev);
        pimu_data_block_prev = pdata_block;
    }
}
