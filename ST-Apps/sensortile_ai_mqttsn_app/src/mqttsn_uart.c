/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : mqttsn_uart.c
 *    Purpose: 
 *                                                          
 *=========================================================*/

#include "Fw_global_config.h"
#include <stdio.h>
#include <stdint.h>
#include "usbd_cdc_interface.h"

#if 1 //( USE_USB_UART  == 1)   

#define MAX_UART_RX_BUF_SIZE 2048
static unsigned char uart_init_done = 0;
static uint8_t uart_buffer_to_read[MAX_UART_RX_BUF_SIZE];
static int rx_rd_index, rx_wr_index;
static int rx_rd_count, rx_wr_count;

// These are functions from MQTTSN_SML that have dependency on SeneorTile
void mqttsn_uart_setup(void)
{
  uart_init_done = 1;
  rx_rd_index = 0;
  rx_wr_index = 0;
  rx_rd_count = 0;
  rx_wr_count = 0;    
  return;
}
void fill_uart_rx_buffer(uint8_t* pBuf, uint32_t bufLen)
{
  //pBuf[bufLen] = 0;  printf("Rx:%d:%s\n", bufLen,pBuf);
  int copy_count = bufLen;
  if ((rx_wr_index + copy_count) > MAX_UART_RX_BUF_SIZE)
  {
    copy_count = MAX_UART_RX_BUF_SIZE - rx_wr_index;
    memcpy(&uart_buffer_to_read[rx_wr_index], pBuf, copy_count);
    pBuf += copy_count;
    if(rx_rd_index > rx_wr_index)
    {
      printf("UART RX buffer overflow \n");
    }
    rx_wr_index = 0;
    copy_count = bufLen - copy_count;
  }
  memcpy(&uart_buffer_to_read[rx_wr_index], pBuf, copy_count);
  rx_wr_index += copy_count;
  if(rx_wr_index >= MAX_UART_RX_BUF_SIZE)
    rx_wr_index = 0;
  rx_wr_count += bufLen;
  return;
}

void mqttsn_uart_tx(uint8_t *pBuf, uint32_t bufLen)
{

    if(uart_init_done)
    {
        CDC_Fill_Buffer(( uint8_t * )pBuf, bufLen);\
    }
    else
    {
        printf("UART not initialised yet\n");
    }
    return;
}

int mqttsn_uart_rx_available(void)
{
  int count;
  if(rx_wr_index >= rx_rd_index)
  {
    count = rx_wr_index - rx_rd_index;
  }
  else
  {
    count = rx_wr_index + MAX_UART_RX_BUF_SIZE - rx_rd_index;
  }
  if((rx_wr_count - rx_rd_count) > MAX_UART_RX_BUF_SIZE) 
  {
    printf("UART RX Buf overflow\n");
    rx_wr_count = 0;
    rx_rd_count = 0;
    rx_rd_index = 0;
    rx_wr_index = 0;
    count = 0;
  }
    
  return count;
}

int mqttsn_uart_rx(uint8_t *pBuf, int n)
{
  int copy_count = n;
  if ((rx_rd_index + copy_count) > MAX_UART_RX_BUF_SIZE)
  {
    copy_count = MAX_UART_RX_BUF_SIZE - rx_rd_index;
    memcpy(pBuf, &uart_buffer_to_read[rx_rd_index], copy_count);
    pBuf += copy_count;
    rx_rd_index = 0;
    copy_count = n - copy_count;
  }
  memcpy(pBuf, &uart_buffer_to_read[rx_rd_index],  copy_count);
  rx_rd_index += copy_count;
  if(rx_rd_index >= MAX_UART_RX_BUF_SIZE)
    rx_rd_index = 0;
  rx_rd_count += n;
  return copy_count;
}
#endif

