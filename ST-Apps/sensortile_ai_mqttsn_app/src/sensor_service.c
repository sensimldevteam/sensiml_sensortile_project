/**
  ******************************************************************************
  * @file    sensor_service.c
  * @author  SRA - Central Labs
  * @version v2.1.0
  * @date    5-Apr-2019
  * @brief   Add bluetooth services using vendor specific profiles.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "hal.h"
#include "TargetFeatures.h"
#include "main.h"
#include "uuid.h"
#include "ble_profile.h"
#include "sensor_service.h"
#include "console.h"
#include "bluenrg_utils.h"
#include "bluenrg_l2cap_aci.h"
#include "uuid_ble_service.h"
#include "qai_dev_params.h"
#include "ble_iop_messages.h"
//#include "sensor.h"
#include "ble_gatt.h"
#include "ble_pme.h"
#include "ble_srv_common.h"


/* Exported variables ---------------------------------------------------------*/
int connected = FALSE;
uint8_t set_connectable = TRUE;

/* Imported Variables -------------------------------------------------------------*/
extern uint32_t ConnectionBleStatus;

extern TIM_HandleTypeDef    TimCCHandle;

extern uint8_t bdaddr[6];

extern ble_iop_connection_t				m_iop_connection;  // Collection service and Device  Information service handles

/* Private variables ------------------------------------------------------------*/

static tDevInfoService qai_devInfoService;


static uint16_t connection_handle = 0;  // /**< Connection handle for the active link. */

/* Private functions ------------------------------------------------------------*/
static void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle);
static void GAP_DisconnectionComplete_CB(void);
tBleStatus add_collection_service(ble_iop_connection_t *m_iop_connection);

/* Private define ------------------------------------------------------------*/

#ifdef ACC_BLUENRG_CONGESTION
#define ACI_GATT_UPDATE_CHAR_VALUE safe_aci_gatt_update_char_value
static int32_t breath;


/* @brief  Update the value of a characteristic avoiding (for a short time) to
 *         send the next updates if an error in the previous sending has
 *         occurred.
 * @param  servHandle The handle of the service
 * @param  charHandle The handle of the characteristic
 * @param  charValOffset The offset of the characteristic
 * @param  charValueLen The length of the characteristic
 * @param  charValue The pointer to the characteristic
 * @retval tBleStatus Status
 */
tBleStatus safe_aci_gatt_update_char_value(uint16_t servHandle,
                                           uint16_t charHandle,
                                           uint8_t charValOffset,
                                           uint8_t charValueLen,
                                           const uint8_t *charValue)
{
  tBleStatus ret = BLE_STATUS_INSUFFICIENT_RESOURCES;

  if (breath > 0)
  {
    breath--;
  }
  else
  {
    ret = aci_gatt_update_char_value(servHandle,charHandle,charValOffset,charValueLen,charValue);

    if (ret != BLE_STATUS_SUCCESS)
    {
      breath = ACC_BLUENRG_CONGESTION_SKIP;
    }
  }

  return (ret);
}

#else /* ACC_BLUENRG_CONGESTION */
#define ACI_GATT_UPDATE_CHAR_VALUE aci_gatt_update_char_value
#endif /* ACC_BLUENRG_CONGESTION */


/**
* @brief  Add the PME service using a vendor specific profile
* @param  None
* @retval tBleStatus Status
*/
tBleStatus add_pme_service(ble_iop_connection_t *m_iop_connection)
{
  tBleStatus ret;

  uint8_t uuid[16];
  int32_t NumberChars = 4;
  uint16_t handle = 0;
  // Add user description
  //uint16_t descUUID = CHAR_USER_DESC_UUID;
  //char buf[8] = "PME-RES";
  //uint16_t charUsrDesc = 0;

  // Initialize the service structure.
  m_iop_connection->m_pme.conn_handle = BLE_CONN_HANDLE_INVALID;
  m_iop_connection->m_pme.evt_handler = ble_pme_evt_handler;

  // Add PME service
  PME_SERVICE_UUID(uuid);
  ret = aci_gatt_add_serv(UUID_TYPE_128, uuid, PRIMARY_SERVICE,
                          1+3*NumberChars,
                          &handle);
  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }

  m_iop_connection->m_pme.service_handle =  handle;
  STLBLE_PRINTF("\n m_iop_connection->pme.service_handle =%d", m_iop_connection->m_pme.service_handle);

  PME_PME_RESULTS_CHAR_UUID(uuid);

  ret =  aci_gatt_add_char(m_iop_connection->m_pme.service_handle, UUID_TYPE_128, uuid, 2*sizeof(ble_pme_result_t),
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP
                             |GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP,
                           MIN_ENCRYPTION_KEY_SIZE, 1, &m_iop_connection->m_pme.pme_result_handle);

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }

  PME_FEAT_VEC_RESULTS_CHAR_UUID(uuid);

  ret =  aci_gatt_add_char(m_iop_connection->m_pme.service_handle, UUID_TYPE_128, uuid, sizeof(ble_pme_result_w_fv_t),
                           CHAR_PROP_NOTIFY ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP
                              |GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &m_iop_connection->m_pme.feat_vector_handle);

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }

//  memcpy(buf, "feat-vec", sizeof(buf));
//  descUUID = CHAR_USER_DESC_UUID;
//
//  /* add the valid descriptor */
//   ret = aci_gatt_add_char_desc(m_iop_connection->m_pme.service_handle,
//                                         m_iop_connection->m_pme.feat_vector_handle,
//                                         UUID_TYPE_16,
//                                         (uint8_t *)&descUUID,
//                                          8,
//                                          8,
//                                          (void *)&buf, //TBR
//                                          ATTR_PERMISSION_NONE,
//                                          ATTR_ACCESS_READ_ONLY,
//                                          GATT_DONT_NOTIFY_EVENTS,
//                                          MIN_ENCRYPTION_KEY_SIZE,
//                                          0x00,
//                                          &charUsrDesc);
//   if (ret != BLE_STATUS_SUCCESS)
//   {
//     while(1);
//      goto fail;
//   }

   return BLE_STATUS_SUCCESS;

fail:
  STLBLE_PRINTF("Error while adding PME service.\n");
  return BLE_STATUS_ERROR;
}


tBleStatus Add_DIS_Charac()
{
  tBleStatus ret;
  uint8_t uuid[2];
   // Add user description
  uint16_t descUUID = CHAR_USER_DESC_UUID;
  char buf[] = BLE_FIRMWARE_REV;
  uint16_t charUsrDesc = 0;

   {
    HOST_TO_LE_16(uuid,FIRMWARE_REVISION_UUID);

    ret = aci_gatt_add_char(qai_devInfoService.devInfoServHandle,
                            UUID_TYPE_16,
                            uuid,
                            FIRMWARE_REVISION_STRING_LEN_MAX,
                            CHAR_PROP_READ,
                            ATTR_PERMISSION_NONE,
                            GATT_DONT_NOTIFY_EVENTS,
                            10,
                            CHAR_VALUE_LEN_VARIABLE,
                            &qai_devInfoService.firmwareRevisionCharHandle);
    if(ret != BLE_STATUS_SUCCESS)
      goto ADD_DIS_CHARAC_FAIL;

	STLBLE_PRINTF("4 - DIS] firmware revision Handle: %04X !!\n",
                     qai_devInfoService.firmwareRevisionCharHandle);
  }

    descUUID = CHAR_USER_DESC_UUID;

  /* add the valid descriptor */
   ret = aci_gatt_add_char_desc(qai_devInfoService.devInfoServHandle,
                                         qai_devInfoService.firmwareRevisionCharHandle,
                                         UUID_TYPE_16,
                                         (uint8_t *)&descUUID,
                                          8,
                                          8,
                                          (void *)&buf, //TBR
                                          ATTR_PERMISSION_NONE,
                                          ATTR_ACCESS_READ_ONLY,
                                          GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP
                                            | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                                          MIN_ENCRYPTION_KEY_SIZE,
                                          0x00,
                                          &charUsrDesc);
   if (ret != BLE_STATUS_SUCCESS)
   {
      goto ADD_DIS_CHARAC_FAIL;
   }


    HOST_TO_LE_16(uuid,MANUFACTURER_NAME_UUID);

    ret = aci_gatt_add_char(qai_devInfoService.devInfoServHandle,
                            UUID_TYPE_16,
                            uuid,
                            MANUFACTURER_NAME_STRING_LEN_MAX,
                            CHAR_PROP_READ,
                            ATTR_PERMISSION_NONE,
                            GATT_DONT_NOTIFY_EVENTS,
                            10,
                            CHAR_VALUE_LEN_VARIABLE,
                            &qai_devInfoService.manufacturerNameCharHandle);
    if(ret != BLE_STATUS_SUCCESS)
      goto ADD_DIS_CHARAC_FAIL;

	STLBLE_PRINTF("7 -[DIS] manufacturer name Handle: %04X !!\n",
                     qai_devInfoService.manufacturerNameCharHandle);

    descUUID = CHAR_USER_DESC_UUID;

    memcpy(buf, DEVICE_MFG, sizeof(DEVICE_MFG));
  /* add the valid descriptor */
   ret = aci_gatt_add_char_desc(qai_devInfoService.devInfoServHandle,
                                         qai_devInfoService.manufacturerNameCharHandle,
                                         UUID_TYPE_16,
                                         (uint8_t *)&descUUID,
                                          10,
                                          10,
                                          (void *)&buf, //TBR
                                          ATTR_PERMISSION_NONE,
                                          ATTR_ACCESS_READ_ONLY,
                                          GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP
                                            | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                                          MIN_ENCRYPTION_KEY_SIZE,
                                          0x00,
                                          &charUsrDesc);
   if (ret != BLE_STATUS_SUCCESS)
   {
      goto ADD_DIS_CHARAC_FAIL;
   }

  return BLE_STATUS_SUCCESS;

ADD_DIS_CHARAC_FAIL:
  STLBLE_PRINTF("[DIS] Add_DIS_Charac() Error while adding characteristics.\n");
  return ret;
}

/**
* @brief  Add the Collection service using a vendor specific profile
* @param  None
* @retval tBleStatus Status
*/
tBleStatus QAI_addDeviceInformationService(void)
{
  tBleStatus ret;
  /* we have to add the device information service */
  uint8_t uuid[2];
  HOST_TO_LE_16(uuid,DEVICE_INFORMATION_SERVICE_UUID);

  ret = aci_gatt_add_serv(UUID_TYPE_16, uuid, PRIMARY_SERVICE, 1 + 3*MAX_DIS_LEN, &qai_devInfoService.devInfoServHandle);
  if(ret!=BLE_STATUS_SUCCESS)
      goto fail;
  STLBLE_PRINTF("Device Information Service is added successfully \n");

  STLBLE_PRINTF("Device Information Service Handle: %04X\n",
                      qai_devInfoService.devInfoServHandle);

    ret = Add_DIS_Charac();
    if(ret!=BLE_STATUS_SUCCESS)
      goto fail;
    STLBLE_PRINTF ("Device Information characteristics added successfully \n");

  return BLE_STATUS_SUCCESS;

  fail:
  STLBLE_PRINTF("Error while adding Device Information service.\n");
  return BLE_STATUS_ERROR;
}


/**
 * @brief  Puts the device in connectable mode.
 * @param  None
 * @retval None
 */
int setConnectable(void)
{

    char local_name[11] = {AD_TYPE_COMPLETE_LOCAL_NAME,DEVICE_NAME};
    uint8_t manuf_data[26] = {
    2,0x0A,0x00 /* 0 dBm */, // Trasmission Power
    11,0x09,DEVICE_NAME, // Complete Name
    13,
    0xff,
    0x01,
    0x00, /* BLE MAC start */
    0x00,
    0x00,
    0x00,
    0x00,
    0x00, /* BLE MAC stop */
  };

  /* BLE MAC */
  manuf_data[20] = bdaddr[5];
  manuf_data[21] = bdaddr[4];
  manuf_data[22] = bdaddr[3];
  manuf_data[23] = bdaddr[2];
  manuf_data[24] = bdaddr[1];
  manuf_data[25] = bdaddr[0];

  /* disable scan response */
  hci_le_set_scan_resp_data(0,NULL);

  tBleStatus retStatus = aci_gap_set_discoverable(ADV_IND, APP_ADV_INTERVAL, APP_ADV_INTERVAL,
#ifndef STATIC_BLE_MAC
                           STATIC_RANDOM_ADDR,
#else /* STATIC_BLE_MAC */
                           PUBLIC_ADDR,
#endif /* STATIC_BLE_MAC */
                           NO_WHITE_LIST_USE,
                           sizeof(local_name), local_name, 0, NULL, MIN_CONN_INTERVAL, MAX_CONN_INTERVAL);
  if(retStatus != BLE_STATUS_SUCCESS)
  {
      STLBLE_PRINTF("\n aci_gap_set_discoverable() failed: retStat =%d", retStatus);
  }

  /* Send Advertising data */ // Note: We don't need to update advertising data for now so commenting the follwing lines
//  retStatus = aci_gap_update_adv_data(26, manuf_data);
//  if(retStatus != BLE_STATUS_SUCCESS)
//  {
//      STLBLE_PRINTF("\n aci_gap_update_adv_data() failed: retStat =%d", retStatus);
//  }
  return retStatus;
}


/**
 * @brief  This function is called when there is a LE Connection Complete event.
 * @param  uint8_t addr[6] Address of peer device
 * @param  uint16_t handle Connection handle
 * @retval None
 */
static void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle)
{
  connected = TRUE;
  connection_handle = handle;

#ifdef ENABLE_USB_DEBUG_CONNECTION
  STLBLE_PRINTF(">>>>>>CONNECTED %x:%x:%x:%x:%x:%x\r\n",addr[5],addr[4],addr[3],addr[2],addr[1],addr[0]);
#endif /* ENABLE_USB_DEBUG_CONNECTION */

  ConnectionBleStatus=0;

}

/**
 * @brief  This function is called when the peer device get disconnected.
 * @param  None
 * @retval None
 */
static void GAP_DisconnectionComplete_CB(void)
{
  connected = FALSE;

#ifdef ENABLE_USB_DEBUG_CONNECTION
  STLBLE_PRINTF("<<<<<<DISCONNECTED\r\n");
#endif /* ENABLE_USB_DEBUG_CONNECTION */

  /* Make the device connectable again. */
  set_connectable = TRUE;

  ConnectionBleStatus=0;

  if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_1) != HAL_OK){
    /* Stopping Error */
    Error_Handler();
  }

  // Enable the advertising will be done in the main BLE task
  //setConnectable(); //) // if failed to advertise try again
  //{
  //  set_connectable = TRUE;
  //}
}

/**
* @brief  This function is called when there is a Bluetooth Read request
* @param  uint16_t handle Handle of the attribute
* @retval None
*/
void Read_Request_CB(uint16_t handle)
{

  //EXIT:
  if(connection_handle != 0)
    aci_gatt_allow_read(connection_handle);
}

/**
* @brief  This function is called whenever there is an ACI event to be processed.
* @note   Inside this function each event must be identified and correctly
*         parsed.
* @param  void *pckt Pointer to the ACI packet
* @retval None
*/
void HCI_Event_CB(void *pckt)
{
  uint8_t attValLen;
  tBleStatus retval = BLE_STATUS_SUCCESS;
  uint16_t attrHandle;

  hci_uart_pckt *hci_pckt = pckt;
  hci_event_pckt *event_pckt = (hci_event_pckt*)hci_pckt->data;

  if(hci_pckt->type != HCI_EVENT_PKT)
  {
    return;
  }

  switch(event_pckt->evt)
  {

  case EVT_CONN_COMPLETE:
    {

    }
  case EVT_DISCONN_COMPLETE:
    {
      GAP_DisconnectionComplete_CB();
      STLBLE_PRINTF("HCI_Event_CB()  \n\nDISCONNECTED\r\n");
    }
    break;
  case EVT_LE_META_EVENT:
    {
      evt_le_meta_event *evt = (void *)event_pckt->data;

      switch(evt->subevent)
      {
      case EVT_LE_CONN_COMPLETE:
        {
          evt_le_connection_complete *cc = (void *)evt->data;
          GAP_ConnectionComplete_CB(cc->peer_bdaddr, cc->handle);
          if (cc->status == BLE_STATUS_SUCCESS)
          {
            /* store connection handle */
            m_iop_connection.m_pme.conn_handle = cc->handle;
            STLBLE_PRINTF("HCI_Event_CB() EVT_LE_CONN_COMPLETE %d handle\n", cc->handle);
          }
        }
        break;

      default:
        STLBLE_PRINTF("\n Unknown EVT_LE_META_EVENT event =%x", evt->subevent);
        break;

      }


    }
    break;
  case EVT_VENDOR:
    {

      evt_blue_aci *blue_evt = (void*)event_pckt->data;
      STLBLE_PRINTF("\n EVT Vendor ecode === %x\n", blue_evt->ecode);
      switch(blue_evt->ecode)
      {
      case EVT_BLUE_GATT_READ_PERMIT_REQ:
        {
          evt_gatt_read_permit_req *pr = (void*)blue_evt->data;
          Read_Request_CB(pr->attr_handle);
        }
        break;

      case EVT_BLUE_GATT_ATTRIBUTE_MODIFIED:
        {
          evt_gatt_attr_modified_IDB05A1 *evt = (evt_gatt_attr_modified_IDB05A1*)blue_evt->data;
          STLBLE_PRINTF("\n ATTRIBUTE_MODIFIED *** evt->attr_handle=%d, len=%d", evt->attr_handle, evt->data_length);

          if((evt->attr_handle == m_iop_connection.m_pme.pme_result_handle + 2)
              || (evt->attr_handle == m_iop_connection.m_pme.feat_vector_handle + 2))
           // PME results handle
          {
             ble_pme_on_write(&m_iop_connection.m_pme, evt->attr_handle, evt->data_length, evt->att_data);
          }
        }
        break;

      default:
        STLBLE_PRINTF("\n @@@@ Unknown event ecode=%x\n", blue_evt->ecode);
      }
    }
    break;
  }
}


/******************* (C) COPYRIGHT 2016 STMicroelectronics *****END OF FILE****/
