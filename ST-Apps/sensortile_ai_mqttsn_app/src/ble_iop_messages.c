
#include "Fw_global_config.h"
#include "main.h"
#include "iop_messages.h"
#include "ble_iop_messages.h"
#include "ble_pme.h"
#include "DataCollection.h"
#include "ble_pme_defs.h"
#include "dbg_uart.h"


#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                           /**< Size of timer operation queues. */
#define SPI_WRITE_TIMEOUT_VAL           APP_TIMER_MIN_TIMEOUT_TICKS //=5ms

// To be on safer side keeping it 64 bytes  ideally most of the command message at current implementaion should be 20 byte  iop_msg_filename_t is one exception
#define DCL_MAX_CMD_MSG_SIZE            64

static uint8_t dclMsg[(DCL_MAX_CMD_MSG_SIZE + 3)];

extern ble_iop_connection_t				m_iop_connection;
extern uint8_t *my2_ble_callback( uint8_t *pData );

//#define configASSERT1( x ) if( ( x ) == 0 ) { STLBLE_PRINTF("configASSERT1 File:%s, Line:%d\n",__FILE__, __LINE__ ); }

void ble_iop_msg_parse(uint8_t * data)
{
    if(data == NULL)
    {
        STLBLE_PRINTF("ble_iop_msg_parse(): NULL Error");
        return ;
    }
    uint8_t len = data[0];
    uint8_t * p_data = &data[0];
    iop_msg_header_t *header = (iop_msg_header_t *)&data[1];
    iop_msg_pme_result_t * p_result;
    iop_msg_pme_fv_result_t * p_fv_res;
    uint8_t sdf[20];
    memset(sdf, 0, 20);

    #if QUICKAI_UART_DEBUG
    dbg_str_int("IOP rx L\n",len);
    dbg_str_int("CMD: %d \n", header->cmd);
    #endif

    if (len < 2)
    {
        return;
    }

    switch(header->cmd)
    {

        case IOP_MSG_RECO_CLASSIFICATION_DATA:
            p_result = (iop_msg_pme_result_t *)&data[1];
            ble_pme_result_t pme_results;
            memcpy(&pme_results, &p_result->pme_results, sizeof(ble_pme_result_t));
            ble_pme_results_set(&m_iop_connection.m_pme, &pme_results);
        break;

        case IOP_MSG_RECO_FEATURE_DATA:
            p_fv_res = ( iop_msg_pme_fv_result_t *)&data[1];
            ble_pme_result_w_fv_t pme_fv_results;
            memcpy(&pme_fv_results, &p_fv_res->pme_fv_results, sizeof(ble_pme_result_w_fv_t));
            #if QUICKAI_UART_DEBUG
            dbg_strhex32("model: ", p_fv_res->pme_fv_results.context);
            dbg_strhex32("\nclass: ", p_fv_res->pme_fv_results.classification);
            #endif
            ble_pme_feature_vector_set(&m_iop_connection.m_pme, &pme_fv_results);
            break;

        default:
            break;
    }
    return;
}

void iop_send_recognition_toggle(bool enable, ble_pme_notif_t toggle_type)
{
    iop_msg_recognition_toggle_t toggle;
    switch(toggle_type)
    {
        case RECO_CLASS_ONLY:
            toggle.header.cmd = enable ? IOP_MSG_RECO_CLASSIFICATION_START : IOP_MSG_RECO_CLASSIFICATION_STOP;
            break;
        case RECO_CLASS_FV:
            toggle.header.cmd = enable ? IOP_MSG_RECO_FEATURES_START : IOP_MSG_RECO_FEATURES_STOP;
            break;
        default:
            toggle.header.cmd = enable ? IOP_MSG_RECO_CLASSIFICATION_START : IOP_MSG_RECO_CLASSIFICATION_STOP;
            break;
    }
    toggle.enable_recognition = enable;
    dclMsg[0] = sizeof(iop_msg_recognition_toggle_t) + 1;
    memcpy((uint8_t *)&dclMsg[1], &toggle, sizeof(iop_msg_recognition_toggle_t));
    my2_ble_callback((uint8_t*) &dclMsg );
}
