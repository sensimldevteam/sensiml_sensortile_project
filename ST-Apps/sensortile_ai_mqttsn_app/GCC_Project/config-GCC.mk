#
# GCC Configuration options for Quick-AI SDK
#

DASH_G=-g0
DASH_O=-O3

#Assembler flags
export AS_FLAGS= -mcpu=cortex-m4 -mthumb -mlittle-endian -mfloat-abi=hard -mfpu=fpv4-sp-d16 $(DASH_O) -fmessage-length=0 \
        -fsigned-char -ffunction-sections -fdata-sections  $(DASH_G) -MMD -MP

#Preprocessor macros
export MACROS=-D__FPU_PRESENT \
        -DSTM32L476xx \
        -DUSE_HAL_DRIVER \
        -DUSE_STM32L4XX_NUCLEO \
        -DARM_MATH_CM4 \
        -DARM_MATH_MATRIX_CHECK \
        -DARM_MATH_ROUNDING \
        -DconfigUSE_STATS_FORMATTING_FUNCTIONS \
        -DconfigUSE_TRACE_FACILITY \
        -DGCC_MAKE \
        -DSENSIML_RECOGNITION \
        -DKBSIM \
        -D$(TOOLCHAIN) \
        -D__GNU_SOURCE \
        -D_DEFAULT_SOURCE \

export OPT_FLAGS=-fmerge-constants -fomit-frame-pointer -fcrossjumping -fexpensive-optimizations -ftoplevel-reorder
export LIBSENSIML_DIR=$(PROJ_ROOT)$(DIR_SEP)ST-Apps$(DIR_SEP)$(APP_NAME)$(DIR_SEP)knowledgepack$(DIR_SEP)sensiml
export LIBCMSIS_GCC_DIR=$(PROJ_ROOT)$(DIR_SEP)Libraries$(DIR_SEP)CMSIS$(DIR_SEP)lib$(DIR_SEP)GCC

# use IAR library


export INCLUDE_DIRS=-I"$(PROJ_DIR)/../inc" \
	-I"$(PROJ_DIR)/../../../Libraries/QLFS/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/QLUART/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/Audio/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/IMU/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/KPD/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/SensorFramework/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/SensorFramework/drivers/M4/LSM6DSL" \
	-I"$(PROJ_DIR)/../../../Libraries/Utils/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/UTFT/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/SysFlash/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/BLE/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/SDCard/inc" \
	-I"$(PROJ_DIR)/../knowledgepack/sensiml/inc" \
	-I"$(PROJ_DIR)/../knowledgepack/inc" \
	-I"$(PROJ_DIR)/../IOP_MQTTSN/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/MQTTSN/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/MQTTSN_SML/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/riff_file/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/cli/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/ADC/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/FreeRTOS_FAT/portable/QL" \
	-I"$(PROJ_DIR)/../../../Libraries/DatablockManager/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/DatablockProcessor/inc" \
	-I"$(STSW_STLKT01)/Drivers/CMSIS/Include" \
	-I"$(STSW_STLKT01)/Drivers/CMSIS/Device/ST/STM32L4xx/Include" \
	-I"$(STSW_STLKT01)/Drivers/STM32L4xx_HAL_Driver/Inc" \
	-I"$(STSW_STLKT01)/Drivers/BSP/SensorTile" \
	-I"$(STSW_STLKT01)/Drivers/BSP/Components/Common" \
	-I"$(STSW_STLKT01)/Drivers/BSP/Components/lsm6dsm" \
	-I"$(STSW_STLKT01)/Drivers/BSP/Components/lps22hb" \
	-I"$(STSW_STLKT01)/Drivers/BSP/Components/lsm303agr" \
	-I"$(STSW_STLKT01)/Drivers/BSP/Components/hts221" \
	-I"$(STSW_STLKT01)/Drivers/BSP/Components/stc3115" \
	-I"$(STSW_STLKT01)/Drivers/BSP/Components/pcm1774" \
	-I"$(STSW_STLKT01)/Middlewares/ST/STM32_USB_Device_Library/Class/AUDIO/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/STM32_USB_Device_Library/Class/CustomHID/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/STM32_USB_Device_Library/Class/DFU/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/BlueNRG-MS/hci/hci_tl_patterns/Basic" \
	-I"$(STSW_STLKT01)/Middlewares/ST/BlueNRG-MS/includes" \
	-I"$(STSW_STLKT01)/Middlewares/ST/BlueNRG-MS/profiles/Peripheral/Inc" \
	-I"$(STSW_STLKT01)/Middlewares/ST/BlueNRG-MS/utils" \
	-I"$(STSW_STLKT01)/Middlewares/ST/BlueNRG-MS/STM32L4xx_HAL_BlueNRG_Drivers/inc" \
	-I"$(STSW_STLKT01)/Middlewares/Third_Party/FreeRTOS/Source/include" \
	-I"$(STSW_STLKT01)/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" \
	-I"$(STSW_STLKT01)/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" \
	-I"$(STSW_STLKT01)/Middlewares/Third_Party/FatFs/src" \
	-I"$(STSW_STLKT01)/Middlewares/Third_Party/FatFs/src/option" \
	-I"$(STSW_STLKT01)/Middlewares/Third_Party/FatFs/src/drivers" \

# C compiler flags
export CFLAGS= $(MACROS) \
        -mcpu=cortex-m4 -mthumb -mlittle-endian -mfloat-abi=hard -mfpu=fpv4-sp-d16 \
        $(DASH_O) $(OPT_FLAGS) -fmessage-length=0 -lm \
        -fsigned-char -ffunction-sections -fdata-sections  $(DASH_G) -std=c99 -MMD -MD -MP


export LD_FLAGS_1= -mcpu=cortex-m4 -mthumb -mlittle-endian -mfloat-abi=hard -mfpu=fpv4-sp-d16 \
	$(DASH_O) $(OPT_FLAGS) -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections \
	$(DASH_G) -T "$(PROJ_DIR)/$(OUTPUT_FILE).ld" -Xlinker --gc-sections -Wall -Werror \
	-Wl,--fatal-warnings -Wl,-Map,"$(OUTPUT_PATH)/$(OUTPUT_FILE).map" \
    --specs=nano.specs -u _printf_float --specs=nosys.specs -Wl,--no-wchar-size-warning \
    -o "$(OUTPUT_PATH)/$(OUTPUT_FILE).out" \
	-L$(LIBCMSIS_GCC_DIR) -L$(LIBSENSIML_DIR) -lsensiml -lm -larm_cortexM4lf_math


export ELF2BIN_OPTIONS=-O binary

#
# Export the files and Directoris that work for both Windows and Linux
# The DIR_SEP is needed only for OS specific command, whereas make can deal with any
#
export COMMON_STUB =$(PROJ_DIR)$(DIR_SEP)makefiles$(DIR_SEP)Makefile_common

export APP_DIR        = $(PROJ_ROOT)$(DIR_SEP)ST-Apps$(DIR_SEP)$(APP_NAME)$(DIR_SEP)src

export LIB_DIR        = $(PROJ_ROOT)$(DIR_SEP)Libraries
export CLI_DIR        = $(LIB_DIR)$(DIR_SEP)cli$(DIR_SEP)src
export MQTTSN_DIR       = $(LIB_DIR)$(DIR_SEP)MQTTSN$(DIR_SEP)src
export MQTTSN_SML_DIR   = $(LIB_DIR)$(DIR_SEP)MQTTSN_SML$(DIR_SEP)src
export RIFF_FILE_DIR  = $(LIB_DIR)$(DIR_SEP)riff_file$(DIR_SEP)src
export SENSOR_DIR       = $(LIB_DIR)$(DIR_SEP)SensorFramework$(DIR_SEP)src
export FRAMEWORKLIB_DIR = $(LIB_DIR)$(DIR_SEP)SensorFramework$(DIR_SEP)lib
export FFE_DIR          = $(LIB_DIR)$(DIR_SEP)SensorFramework$(DIR_SEP)drivers$(DIR_SEP)FFE
export HYBRID_DIR       = $(LIB_DIR)$(DIR_SEP)SensorFramework$(DIR_SEP)drivers$(DIR_SEP)Hybrid
export M4_DIR           = $(LIB_DIR)$(DIR_SEP)SensorFramework$(DIR_SEP)drivers$(DIR_SEP)M4$(DIR_SEP)LSM6DSL

export POWER_DIR        = $(LIB_DIR)$(DIR_SEP)Power$(DIR_SEP)src
export QLUART_DIR       = $(LIB_DIR)$(DIR_SEP)QLUART$(DIR_SEP)src
export SYSFLASH_DIR     = $(LIB_DIR)$(DIR_SEP)SysFlash$(DIR_SEP)src
export IOP_DIR        = $(LIB_DIR)$(DIR_SEP)IOP$(DIR_SEP)src
export QUICKAI_DIR    = $(PROJ_ROOT)$(DIR_SEP)BSP$(DIR_SEP)QuickAI$(DIR_SEP)src
export AUDIO_DIR      = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)Audio$(DIR_SEP)src
export AI_DIR         = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)AI$(DIR_SEP)src
export ADC_DIR        = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)ADC$(DIR_SEP)src
export IMU_DIR        = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)IMU$(DIR_SEP)src
export BLE_DIR        = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)BLE$(DIR_SEP)src
export KPD_DIR        = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)KPD$(DIR_SEP)src
export UTILS_DIR         = $(LIB_DIR)$(DIR_SEP)Utils$(DIR_SEP)src
export UTILS_DIR         = $(LIB_DIR)$(DIR_SEP)Utils$(DIR_SEP)src
export FREERTOS_FAT_DIR  = $(LIB_DIR)$(DIR_SEP)FreeRTOS_FAT
export FREERTOS_FAT_COMMON_DIR  = $(LIB_DIR)$(DIR_SEP)FreeRTOS_FAT$(DIR_SEP)portable$(DIR_SEP)common
export FREERTOS_FAT_QL_DIR  = $(LIB_DIR)$(DIR_SEP)FreeRTOS_FAT$(DIR_SEP)portable$(DIR_SEP)QL
export DBM_DIR   = $(LIB_DIR)$(DIR_SEP)DatablockManager$(DIR_SEP)src
export DBP_DIR = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)DatablockProcessor$(DIR_SEP)src

export ST_HAL_DIR      = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)STM32L4xx_HAL_Driver$(DIR_SEP)Src
export ST_FREERTOS_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)Third_Party$(DIR_SEP)FreeRTOS$(DIR_SEP)Source
export ST_FATFS_DIR  = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)Third_Party$(DIR_SEP)FatFs$(DIR_SEP)src
export ST_FATFS_OPTION_DIR  = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)Third_Party$(DIR_SEP)FatFs$(DIR_SEP)src$(DIR_SEP)option
export ST_HTS221_COMP_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)BSP$(DIR_SEP)Components$(DIR_SEP)hts221
export ST_LPS22HB_COMP_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)BSP$(DIR_SEP)Components$(DIR_SEP)lps22hb
export ST_LSM303AGR_COMP_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)BSP$(DIR_SEP)Components$(DIR_SEP)lsm303agr
export ST_LSM6DSM_COMP_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)BSP$(DIR_SEP)Components$(DIR_SEP)lsm6dsm
export ST_PCM1774_COMP_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)BSP$(DIR_SEP)Components$(DIR_SEP)pcm1774
export ST_STC3115_COMP_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)BSP$(DIR_SEP)Components$(DIR_SEP)stc3115
export ST_SENSORTILE_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)BSP$(DIR_SEP)SensorTile
export ST_SYSTEM_STM32L4xx_DIR = $(STSW_STLKT01)$(DIR_SEP)Drivers$(DIR_SEP)CMSIS$(DIR_SEP)Device$(DIR_SEP)ST$(DIR_SEP)STM32L4xx$(DIR_SEP)Source$(DIR_SEP)Templates

export ST_USBD_CORE_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)ST$(DIR_SEP)STM32_USB_Device_Library$(DIR_SEP)Core$(DIR_SEP)Src
export ST_USBD_CLASS_CDC_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)ST$(DIR_SEP)STM32_USB_Device_Library$(DIR_SEP)Class$(DIR_SEP)CDC$(DIR_SEP)Src

export ST_BLE_HCI_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)ST$(DIR_SEP)BlueNRG-MS$(DIR_SEP)hci
export ST_BLE_HCI_CONTROLLER_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)ST$(DIR_SEP)BlueNRG-MS$(DIR_SEP)hci$(DIR_SEP)controller
export ST_BLE_HCI_TL_PATTERNS_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)ST$(DIR_SEP)BlueNRG-MS$(DIR_SEP)hci$(DIR_SEP)hci_tl_patterns$(DIR_SEP)Basic
export ST_BLE_HAL_DRIVERS_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)ST$(DIR_SEP)BlueNRG-MS$(DIR_SEP)STM32L4xx_HAL_BlueNRG_Drivers$(DIR_SEP)src
export ST_BLE_UTILS_DIR = $(STSW_STLKT01)$(DIR_SEP)Middlewares$(DIR_SEP)ST$(DIR_SEP)BlueNRG-MS$(DIR_SEP)utils
