SensorTile AI Application Project
=================================

This demo project performs SensiML data collection and recognition tasks 
for the SensorTile evaluation platform using the STSW-STLKT01 SDK 
(Refer: https://www.st.com/en/embedded-software/stsw-stlkt01.html)


Requirements for building the IAR project:
------------------------------------------
1. Must have installed the SensorTile SDK.
2. Must define the following IAR custom variable to point to the directory
   where the SensorTile SDK is installed.

   STSW_STLKT01: 

    This custom IAR workspace variable is used by the "sensortile_ai_mqttsn_app"
    IAR project. This variable must be defined in the 
        "sensortile_ai_mqttsn_app.custom_argvars" file
    to point to the STSW-STLKT01 SDK location.

Procedure:

1. Copy "1_sensortile_ai_app.custom_argvars" file to "sensortile_ai_mqttsn_app.custom_argvars"
2. Locate the variable name STSW-STLKT01 in the file "sensortile_ai_mqttsn_app.custom_argvars"
   and modify its associated value to point to the location of the SensorTile SDK 
   installation directory.

Example:

   If the SensorTile SDK is installed in the following directory,

     C:\Users\STsoftware\SensorTile\STSW-STLKT01_V2.1.1

   then replace the following line in the file "sensortile_ai_mqttsn_app.custom_argvars"

    <value>C:\SensorTile\STSW-STLKT01_V2.1.1</value>
  
   with

    <value>C:\Users\STsoftware\SensorTile\STSW-STLKT01_V2.1.1</value>

3. Alternatively, 
     Open the IAR project "sensortile_ai_mqttsn_app.eww", then navigate to 
     Tools -> Configure Custom Argument Variables...
     Then select Workspace tab and then locate the variable STSW_STLKT01
     under the group "SENSIML". Select "Edit Variable..." and update the
     value to point to the SensorTile SDK installation directory.

     Close the project and reopen for the changes to take effect.

4. In case build errors persist, close the IAR embedded workbench IDE
   and reopen for the changes to take effect.

