#include "Fw_global_config.h"
#include "sml_output.h"
#include "kb.h"
#include "ble_pme_defs.h"
#include "iop_messages.h"
#include "Recognition.h"
#include "dbg_uart.h"

#define SERIAL_OUT_CHARS_MAX 512

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

static ble_pme_result_t recent_result;
static ble_pme_result_w_fv_t recent_fv_result;

static char serial_out_buf[SERIAL_OUT_CHARS_MAX];
static char *p_serial_out = serial_out_buf;

void SendLastRecognition()
{
    iop_send_pme_results(&recent_result);
}

void SendLastRecognitionWithFeatures()
{
    iop_send_feature_vector_results(&recent_fv_result);
}

static void sml_output_ble(uint16_t model, uint16_t classification)
{
    int current_state = GetRecognitionCurrentState();
    switch(current_state)
    {
        default:
            break;
        case RECOG_STATE_RUN:
            SendLastRecognition();
            break;
        case RECOG_STATE_RUN_W_FV:
            SendLastRecognitionWithFeatures();
            break;
        case RECOG_STATE_RUN_W_S:
            break;
    }
}

static void sml_output_led(uint16_t model, uint16_t classification)
{
    //Unused for right now.
}

static void sml_output_serial(uint16_t model, uint16_t classification)
{
    if( !(DBG_flags & DBG_FLAG_recog_result) )
    {
        return;
    }
    snprintf(serial_out_buf, sizeof(serial_out_buf)-1,
           "{\"ModelNumber\":0,\"Classification\":%d,\"FeatureLength\":%d,\"FeatureVector\":[",classification, recent_fv_result.fv_len);
    dbg_str(serial_out_buf);
    for(int j=0; j < recent_fv_result.fv_len; j++)
    {
        dbg_int(recent_fv_result.feature_vector[j]);
        if(j < recent_fv_result.fv_len -1)
        {
            dbg_ch(',');
        }
    }
    dbg_ch(']');
    dbg_ch('}');
    dbg_ch('\n');
}

uint32_t sml_output_results(uint16_t model, uint16_t classification)
{
    recent_result.context = model;
    recent_result.classification = classification;
    recent_fv_result.context = model;
    recent_fv_result.classification = classification;
    kb_get_feature_vector(model, recent_fv_result.feature_vector, &recent_fv_result.fv_len);

    sml_output_ble(model, classification);
sml_output_serial(model, classification);
    return 0;
}

uint32_t sml_output_init(void * p_module)
{
    //unused for now
}