#ifndef __SENSIML_SENSOR_CONFIG_H__
#define __SENSIML_SENSOR_CONFIG_H__

#include <stdint.h>
#include "dcl_commands.h"
#include "sensor_config.h"


#define SML_DEVICE_COMMAND_NUM_MSGS 3

const struct sensor_config_msg recognition_config[] = {
	SENSOR_CONFIG_CLEAR_MSG(),
	SENSOR_CONFIG_IMU_MSG(SENSOR_ENG_VALUE_ACCEL,104,0x14),
	SENSOR_CONFIG_DONE_MSG()
};

#endif //__SENSIML_SENSOR_CONFIG_H__