#ifndef _KB_DEFINES_H_
#define _KB_DEFINES_H_
#include "kb_typedefs.h"
#ifdef DSW_BUILD
#include "infra/log.h"
#endif
#include "stdio.h"
#ifdef __cplusplus
extern "C" {
#endif

#ifndef SML_KP_DEBUG
#define SML_KP_DEBUG 0
#endif
#define KB_LOG_LEVEL 1

#if SML_KP_DEBUG
	#if defined(WIN32) || defined(KBSIM)
		#define dbgprintlev(level, ...) if(level <= KB_LOG_LEVEL) {printf(__VA_ARGS__);printf("\n");}
		#define pr_info(logger, ...) {printf(__VA_ARGS__); printf("\n");}
	#elif defined(DSW_BUILD)
		#define dbgprintlev(level, ...) if(level <= kb_log_level) pr_info(LOG_MODULE_MAIN, __VA_ARGS__)
	#elif NRF_LOG_ENABLED
		#define dbgprintlev(level, ...) if(level <= KB_LOG_LEVEL) {NRF_LOG_INFO(__VA_ARGS__);}
	#else
    		#define dbgprintlev(level,...)
	#endif

#else
    #define dbgprintlev(level,...)
#endif //DEBUG

#ifdef __cplusplus
}
#endif

#endif //_KB_DEFINES_H_
