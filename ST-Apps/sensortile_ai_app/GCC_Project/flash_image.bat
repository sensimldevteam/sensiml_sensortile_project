@echo off
REM Give jlink.exe along with path. If the exe is there in the PATH, just give JLink.exe. 
set JLINKCMD="JLink.exe"
%JLINKCMD% -Device STM32L476JG -If SWD -Speed 4000 -commandFile "LoadSensorTile.jlink"
pause