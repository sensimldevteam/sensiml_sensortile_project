/*==========================================================
*                                                          
*-  Copyright Notice  -------------------------------------
*                                                          
*    Licensed Materials - Property of QuickLogic Corp.     
*    Copyright (C) 2019 QuickLogic Corporation             
*    All rights reserved                                   
*    Use, duplication, or disclosure restricted            
*                                                          
*    File   : iop_messages.c
*    Purpose: 
*                                                          
*=========================================================*/
#ifndef _BLE_COLLECTION_H_
#define _BLE_COLLECTION_H_

#include "dcl_commands.h"
#include "ble_pme.h"
#include "ble_collection.h"



// @todo Indta BLE related messages need to move to correct place later

#define BLE_GATTS_AUTHORIZE_TYPE_INVALID    0x00  /**< Invalid Type. */
#define BLE_GATTS_AUTHORIZE_TYPE_READ       0x01  /**< Authorize a Read Operation. */
#define BLE_GATTS_AUTHORIZE_TYPE_WRITE      0x02  /**< Authorize a Write Request Operation. */


#define MIN_ENCRYPTION_KEY_SIZE             16    // Encryption key size
#define MAX_FILE_PREFIX_LEN                 20


/**< The UUID of the Motion Service. */
#define BLE_UUID_COLLECTION_SERVICE			0x0100
#define BLE_GATT_ATT_MTU_DEFAULT    		23
#define BLE_MOTION_MAX_DATA_LEN (BLE_GATT_ATT_MTU_DEFAULT - 3)
/**< Maximum length of data (in bytes) that can be transmitted to the peer by the Motion service module. */

#define BLE_MOTION_CONFIG_MPUF_MIN			5    //< Minimum motion processing frequency [Hz]. (4 Hz minimum to mpu_set_sample_rate())
#define BLE_MOTION_CONFIG_MPUF_MAX			6666 //< Maximum motion processing frequency [Hz].
#define BLE_MOTION_CONFIG_WOM_MAX			1    //< Wake on motion off.
#define BLE_MOTION_CONFIG_SD_STORE_MAX		1    //< SD Storage On.
#define BLE_MOTION_CONFIG_SENSOR_USE_MAX	1   	//< SD Storage On.
#define QUICKAI_BLE_UART_ENABLE 			1  //1= Enable QUICKAI board BLE as a UART transport
#define BLE_MOTION_SEND_SAMPLES 	1

#define BLE_COLLECTION_CFG_DATA_SZ (BLE_MOTION_MAX_DATA_LEN - 1)

typedef enum
{
	BLE_COLLECTION_EVT_CONFIG_RECEIVED,
	BLE_COLLECTION_EVT_NOTIF_RAW,
	BLE_COLLECTION_EVT_NOTIF_CONFIG,
	BLE_COLLECTION_EVT_NOTIF_SCALED,
	BLE_COLLECTION_EVT_FILE_NAME,
	BLE_AUDIO_NOTIF_RECORD_ENABLED,
	BLE_COLLECTION_EVT_NOTIF_DATETIME,
}BLE_COLLECTION_EVT_type_t;

/* Forward declaration of the ble_collection_t type. */
typedef struct ble_motion_s ble_collection_t;


/***************************************************************
 * PACKED Structures used for BLE. Needs packing so we don't
 * go beyond 20 byte size limit in nrf51.
 ***************************************************************/



typedef PACKED( struct
{
	int16_t x;
	int16_t y;
	int16_t z;
}) ble_motion_raw_accel_t;

typedef PACKED( struct
{
	int16_t x;
	int16_t y;
	int16_t z;
}) ble_motion_raw_gyro_t;

typedef PACKED( struct
{
	ble_motion_raw_accel_t   accel;
	ble_motion_raw_gyro_t    gyro;
}) ble_motion_raw_t;

//#include "iop_messages.h"

typedef PACKED (struct
{
	uint8_t		cfg_type;
	uint8_t 	cfg_data[BLE_COLLECTION_CFG_DATA_SZ];
})iop_collection_config_t;


/**@brief Motion Service event handler type. */
typedef void (*BLE_COLLECTION_EVT_handler_t) (ble_collection_t        * p_tms,
									   BLE_COLLECTION_EVT_type_t evt_type,
									   uint8_t          * p_data,
									   uint16_t           length);

/**@brief Motion Service initialization structure.
 *
 * @details This structure contains the initialization information for the service. The application
 * must fill this structure and pass it to the service using the @ref ble_collection_init function.
 */
typedef struct
{
	iop_collection_config_t      * p_init_config;
	BLE_COLLECTION_EVT_handler_t   evt_handler; /**< Event handler to be called for handling received data. */
} ble_collection_init_t;

/**@brief Motion Service structure.
 *
 * @details This structure contains status information related to the service.
 */
struct ble_motion_s
{
	uint8_t                  uuid_type;                    /**< UUID type for Motion Service Base UUID. */
	uint16_t                 service_handle;               /**< Handle of Motion Service (as provided by the S110 SoftDevice). */
	uint16_t                 filename_handles;             /**< Handles related to the config characteristic (as provided by the SoftDevice). */
	uint16_t                 config_handles;               /**< Handles related to the config characteristic (as provided by the SoftDevice). */
	uint16_t                 raw_handles;                  /**< Handles related to the raw data characteristic (as provided by the SoftDevice). */
	uint16_t                 conn_handle;                  /**< Handle of the current connection (as provided by the S110 SoftDevice). BLE_CONN_HANDLE_INVALID if not in a connection. */
	bool                     is_raw_notif_enabled;         /**< Variable to indicate if the peer has enabled notification of the characteristic.*/
	bool                     is_command_config_notif_en;         /**< Variable to indicate if the peer has enabled notification of the characteristic.*/
	BLE_COLLECTION_EVT_handler_t    evt_handler;                  /**< Event handler to be called for handling received data. */
};

// BLE related defines - Needs to move to BLE files later
/**@brief GATT Authorization parameters. */
typedef struct
{
  uint16_t          gatt_status;        /**< GATT status code for the operation, see @ref BLE_GATT_STATUS_CODES. */
  uint8_t           update : 1;         /**< If set, data supplied in p_data will be used to update the attribute value.
                                             Please note that for @ref BLE_GATTS_OP_WRITE_REQ operations this bit must always be set,
                                             as the data to be written needs to be stored and later provided by the application. */
  uint16_t          offset;             /**< Offset of the attribute value being updated. */
  uint16_t          len;                /**< Length in bytes of the value in p_data pointer, see @ref BLE_GATTS_ATTR_LENS_MAX. */
  const uint8_t    *p_data;             /**< Pointer to new value used to update the attribute value. */
} ble_gatts_authorize_params_t;

/**@brief GATT Read or Write Authorize Reply parameters. */
typedef struct
{
  uint8_t                               type;   /**< Type of authorize operation, see @ref BLE_GATTS_AUTHORIZE_TYPES. */
  union {
    ble_gatts_authorize_params_t        read;   /**< Read authorization parameters. */
    ble_gatts_authorize_params_t        write;  /**< Write authorization parameters. */
  } params;                                     /**< Reply Parameters. */
} ble_gatts_rw_authorize_reply_params_t;
// - BLE defines ends

uint32_t ble_collection_toggle(bool enable);
uint32_t ble_capture_filename_apply(char *filename, uint16_t length);
uint32_t ble_motion_raw_set(ble_collection_t *p_collection, ble_motion_raw_t *p_data);
void ble_motion_on_write(ble_collection_t * p_collection,  uint16_t attr_handle, uint8_t data_length, uint8_t *p_data);
void ble_collection_data_handler(ble_collection_t *p_collection, uint16_t attr_handle, BLE_COLLECTION_EVT_type_t evt_type, uint16_t length, uint8_t * p_data);

#endif // _BLE_COLLECTION_H_
