#if !defined(DCL_COMMANDS_H)
#define DCL_COMMANDS_H
/*
 * This file is *SHARED* between multiple tools.
 * This file must be compileable using only "stdint.h"
 *
 * An edits to this file requires that all tools be updated.
 *
 * Tools is a long list of things.
 * Current list as of: Mar 4 2019
 * ================================
 * Tools =  All Embedded Proto Apps, all platforms.
 * Tools =  BLE Interface from PC to Device.
 * Tools =  SensiML DCL application (PC APP)
 * Tools =  SensiML Android Application (Mobile App)
 */
#include <stdint.h>

typedef enum iop_sensor_config_minor_cmds {
    /**
    * disables all sensors
    *
    * Also "clears" configuration error flag.
    */
    IOP_COLLECTION_CLEAR_SENSORS = 0x00,
    
    /**
    * adds (enabling) a sensor
    * Sensor details are:
    *   32bit sensor_id
    *   32bit rate_hz
    *
    *  Followed by upto 8 bytes of 'sensor specific data'
    *
    * If configuration is invalid/not known/etc
    * then "error_number" is set to some NONZERO value (TBD)
    */
    IOP_COLLECTION_ADD_SENSOR = 0x01,
    
    /**
    * Set DateTime, Unix.
    */
    IOP_COLLECTION_SET_UNIX_DATETIME = 0x02,
    
    /**
    * Get supported sensors list.
    * Returns a 0 terminated list of supported sensors
    */
    IOP_COLLECTION_GET_SENSOR_LIST = 0x03,
    
    /**
    * Tells device all configuration is complete.
    * No payload
    *   response is 2 32bit values
    *     uint32_t error_number; // 0 means no error
    *     uint32_t extended_details; // more details if possible.
    *
    * This can be sent multiple times...
    * error is cleared by sending a clear sensor
    */
    IOP_COLLECTION_CONFIG_DONE = 0x04,
    
    /**
    * RESERVED VALUES BELOW THIS LINE.
    * These are used by the BLE implimenation
    */
    IOP_COLLECTION_RESERVED_1 = 0xF0,
    IOP_COLLECTION_RESERVED_2 = 0xF1,
    IOP_COLLECTION_RESERVED_3 = 0xF2,
    IOP_COLLECTION_RESERVED_4 = 0xF3,
    IOP_COLLECTION_RESERVED_5 = 0xF4,
    IOP_COLLECTION_RESERVED_6 = 0xF5,
    IOP_COLLECTION_RESERVED_7 = 0xF6,
    IOP_COLLECTION_RESERVED_8 = 0xF7,
    IOP_COLLECTION_RESERVED_9 = 0xF8,
    IOP_COLLECTION_RESERVED_10 = 0xF9,
    IOP_COLLECTION_RESERVED_11 = 0xFA,
    
    /**
    *  DCL -> SetStorage, translated in BLE
    */
    IOP_COLLECTION_RESERVED_DCL_SET_STORAGE = 0xFB,
    /**
    * DCL -> GetStatus, translated in BLE
    */
    IOP_COLLECTION_RESERVED_DCL_GET_STATUS = 0xFC,
    /**
    * DCL -> RecordDataStart, translated in BLE
    */
    IOP_COLLECTION_RESERVED_DCL_RECORD_START = 0xFD,
    /**
    * DCL -> RecordDataStop, translated in BLE
    */
    IOP_COLLECTION_RESERVED_DCL_RECORD_STOP = 0xFE,
    
//    IOP_UNKNOWN_COMMAND = 0xFF,
    
    /* 8bit enum */
} sensor_config_iop_cmd_t;

typedef enum dcl_cmds {
        /**
         * disables all sensors
         *
         * Also "clears" configuration error flag.
         */
        DCL_COMMAND_CLEAR_SENSORS = 0x00,

        /**
         * adds (enabling) a sensor
         * Sensor details are:
         *   32bit sensor_id
         *   32bit rate_hz
         *
         *  Followed by upto 8 bytes of 'sensor specific data'
         *
         * If configuration is invalid/not known/etc
         * then "error_number" is set to some NONZERO value (TBD)
         */
         DCL_COMMAND_ADD_SENSOR = 0x01,

        /**
         * Set DateTime, Unix.
         */
        DCL_COMMAND_SET_UNIX_DATETIME = 0x02,

              /**
               * Get supported sensors list.
               * Returns a 0 terminated list of supported sensors
               */
        DCL_COMMAND_GET_SENSOR_LIST = 0x03,

        /**
         * Tells device all configuration is complete.
         * No payload
         *   response is 2 32bit values
         *     uint32_t error_number; // 0 means no error
         *     uint32_t extended_details; // more details if possible.
         *
         * This can be sent multiple times...
         * error is cleared by sending a clear sensor
         */
        DCL_COMMAND_CONFIG_DONE = 0x04,


        /*
         * NOT SURE WHAT YOU WANT HERE
         */
        DCL_COMMAND_CONFIG_GET_STATUS = 0x05,


        DCL_COMMAND_RESERVED_1 = 0xF0,
        DCL_COMMAND_RESERVED_2 = 0xF1,
        DCL_COMMAND_RESERVED_3 = 0xF2,
        DCL_COMMAND_RESERVED_4 = 0xF3,
        DCL_COMMAND_RESERVED_5 = 0xF4,
        DCL_COMMAND_RESERVED_6 = 0xF5,
        DCL_COMMAND_RESERVED_7 = 0xF6,

        DCL_COMMAND_CLEAR_ERR_BIT = 0xF7,

        DCL_COMMAND_GET_SENSOR_LIST_RSP = 0xF8,


        DCL_COMMAND_RESERVED_DCL_GET_VERSION = 0xF9,
        DCL_COMMAND_RESERVED_DCL_GET_COMP_DT = 0xFA,

        /**
         *  DCL -> SetStorage, translated in BLE
         */
        DCL_COMMAND_RESERVED_DCL_SET_STORAGE = 0xFB,
         /**
          * DCL -> GetStatus, translated in BLE
          */
        DCL_COMMAND_RESERVED_DCL_GET_STATUS = 0xFC,
         /**
          * DCL -> RecordDataStart, translated in BLE
          */
        DCL_COMMAND_RESERVED_DCL_RECORD_START = 0xFD,
         /**
          * DCL -> RecordDataStop, translated in BLE
          */
        DCL_COMMAND_RESERVED_DCL_RECORD_STOP = 0xFE,

        IOP_UNKNOWN_COMMAND = 0xFF,

        /* 8bit enum */
} sensor_config_cmd_t;


#define SENSOR_MAKE_ID_32BIT(A,B,C,D)  \
  (((A) << 24) + ((B)<<16) + ((C)<<8) + ((D)<<0))
#define SENSOR_MAKE_AUDIO_ID(  RATE, NCHANNELS )   \
  (('A' << 24) + ('U' << 16) + ((NCHANNELS-1) << 14) + ((RATE)/100))

/* About SENSOR_ENG_VALUE_IMU_agm - it is only used in "get list of supported sensors".
* and when the AGM sensors are ganged
*
* The low 8bits represents which sensor is present (set=1) or not present (clear=0)
* bits 0 - accel
* bits 1 - gyro
* bits 2 - magnetometer
*/
#define SENSOR_ENG_VALUE_IMU_agm_base           SENSOR_MAKE_ID_32BIT( 'I', 'M', 'U',  0 )
#define SENSOR_ENG_VALUE_IMU_agm_a_bit          (1)
#define SENSOR_ENG_VALUE_IMU_agm_g_bit          (2)
#define SENSOR_ENG_VALUE_IMU_agm_m_bit          (4)

/* audio is 8 or 16 bit data, in a continous stream of sample values
 * data may be monoral, or stereo - etc (1 channel, or 2 channel)
 * and comes at some configured rate, ie: 8khz or 16khz or ... ??hz
 */
#define SENSOR_AUDIO                            SENSOR_MAKE_ID_32BIT( 'A', 'U', 'D', 'O' )

/* mayhewe board */
#define SENSOR_ADC_LTC_1859_MAYHEW		SENSOR_MAKE_ID_32BIT( 'L', 'T', 0, (1859*10)+0 )

//I'll need this set of values/names from you Duane.
//This is my placeholder right now.
//Setting to 32 bit value to assume -fshort-enums (and compiler specific variants) are used, forcing 32-bit.
#define SENSOR_ENG_VALUE_ACCEL			SENSOR_MAKE_ID_32BIT( 'I', 'M', 'U', 'A' )
#define SENSOR_ENG_VALUE_GYRO			SENSOR_MAKE_ID_32BIT( 'I', 'M', 'U', 'G' )
#define SENSOR_ENG_VALUE_MAGNETOMETER	SENSOR_MAKE_ID_32BIT( 'I', 'M', 'U', 'M' )
#define SENSOR_PDM_MONO_AUDIO_8KHZ		SENSOR_MAKE_AUDIO_ID( 8000, 1 ),
#define SENSOR_PDM_STEREO_AUDIO_8KHZ	SENSOR_MAKE_AUDIO_ID( 8000, 2 ),
#define SENSOR_PDM_MONO_16KHZ			SENSOR_MAKE_AUDIO_ID( 16000, 1 ),
#define SENSOR_PDM_STEREO_16KHZ			SENSOR_MAKE_AUDIO_ID( 16000, 2 ),
       /* mayhewe board */
#define SENSOR_ADC_LTC_1859_A			SENSOR_MAKE_ID_32BIT( 'L', 'T', 0, (1859*10)+0 )
       /* some future same chip, different config */
#define SENSOR_ADC_LTC_1859_B			SENSOR_MAKE_ID_32BIT( 'L', 'T', 0, (1859*10)+1 )
typedef uint32_t sensor_id_t;

typedef struct
{
  uint32_t  unix_time_seconds;
} sensor_datetime_t;

//As we discussed.
typedef struct{
  sensor_id_t sensor_id;
  uint32_t    rate_hz;  /* rate = 0 is disabled */
} sensor_common_t;

//As discussed.
typedef struct{
    sensor_common_t common;
    /**
     * Range * 10,
     *   ie: 20 = 2g, 40 = 4g, 5 = 0.5 g
     */
    uint32_t sensor_range;
} sensor_imu_t;

typedef enum {
        /* bitfield for storage;
         *   value = 0 no storage
         *   bitvalue 1 = send over ble
         *   bitvalue 2 = store on sd card (removable media)
         *   bitvalue 4 = store on usb_disk (future)
         *   bitvalue 8 .. tbd.
         */
        STORAGE_OFF = 0x00, //effectively the same as sample rate -> 0. or just dont have an off?
        STORAGE_BLE = 1,
        STORAGE_SD_CARD = 2,
        STORAGE_USB_DISK = 4, /* future */
        STORAGE_BOARD_DEFINED = 5, /* future */

        /* note: Bits [31:28] are reserved see status */
        STORE_STATUS_RUNNING = (1 << 28),
        STORE_STATUS_STORING = (1 << 29),
        STORE_STATUS_OVERRUN = (1 << 30)
} sensor_storage_bitfield_t;

typedef struct {
  sensor_common_t common;
  sensor_storage_bitfield_t   storage;
} sensor_storage_msg_t;

/**
 * @brief sent from MERCED to BLE as response to status request.
 */
typedef struct{
  /* bitfield from: "sensor_storage_bitfield_t" */
  uint32_t storage_status_bits;

  /* number of bytes written to storage */
  uint32_t bytes_stored;

  /**
   * Error numbers - use the standard UNIX names and values for most errors
   *
   * SEE: http://www-numi.fnal.gov/offline_software/srt_public_context/WebDocs/Errors/unix_system_errors.html
   *
   * Idea is not the true unix purpose, but the general name is good enough
   *
   *   "EINVAL" - means it is invalid.
   *   "E2BIG" (too many params) -  or maybe ENOSPC (no space) too many sensor settings sent.
   *   point is there are lots of names ...
   */
  uint32_t			error_num;  // errors need to be defined.
  uint32_t			error_info; // future

} sensor_status_t;

/**
 * @brief audio sensor config
 */
typedef struct {
  sensor_common_t common;
  uint8_t nbits; /* 8 or 16 */
  /* config values:
   * Currently defined as mono and stereo, (mic0 and mic1)
   *
   * 0 - means default configuration for the microphone.
   * non-zero is platform specific microphone configuration.
   */
  uint8_t mic_config[8];
} sensor_audio_t;

/**
 * @brief config structure for LTC8159 chip
 *
 * Values are from the LTC8159 data sheet.
 * see: https://www.analog.com/media/en/technical-documentation/data-sheets/185789fb.pdf
 * Page: 15, "INPUT DATA WORD", bits [7:0] are used, [15:8] unused.
 * Thus, bit[7] maps to the SGL/DIFF bit, and bit[0] is the sleep bit.
 */

typedef struct {
  sensor_common_t common;
  uint16_t chnl_config[4];
} sensor_adc_ltc8159_a_t;

/**
 * @brief Filename Command
 *
 * Actual filename is constructed as follows.
 * Step 1 - Search for last "." in filename_template.
 *          If no "." found, use end of filename.
 *
 * Step 2 - insert current date/time in ISO8601 format
 *
 * NOTE: Device does not know timezone, it assumes "local"
 *
 * For example, givein:  "foo.txt" as a filename
 * And a date/time of: 4:03:44pm, March 4, 2019 PST
 *
 * The filename will be foo_20190304T160344.txt
 */

typedef struct {
  uint8_t filename_template[18];
} sensor_filename_t;

/**
 * @brief - Start Command packet.
 *
 * When a START command is sent, data capture begins.
 * This may take a moment to actually start
 *
 * If saving to a file is supported, then
 * this GUID is included in the JSON header in RFF block 0.
 *
 * The purpose is so that the host controling the capture
 * can relate a start command and the resulting file seperately.
 */

typedef struct {
  uint8_t guid_bytes[16];
} sensor_start_msg_t;

/**
 * @brief - Get supported Sensors Response.
 */
typedef struct {
  uint32_t sensor_list[4];
} sensor_list_t;

typedef struct {
  uint8_t first_16_bytes[16];
} sensor_error_t;

typedef struct {
  uint8_t dummy_not_used;
} sensor_stop_msg_t;


/**
 * @brief BLE Message Structure
 *
 * There are 3 important components of this structure:
 *
 * #1 - The message type
 * #2 - The union of many types.
 * #3 - The packed array of bytes.
 *
 * All packets are 20 bytes (BLE limitation), byte0 is the message type.
 *
 * The bytes of the message are otherwise "packed" (no padding) starting
 * at byte1, through byte19.
 *
 * This structure purosely is not labeled as "packed" becuase
 * every tool chain requires different decoration to mark a structure
 * as a packed or unpacked element.
 *
 * If a structure is properly designed, they require no padding.
 * And all sensor packets are designed this way.
 *
 * The Process - Opon Receipt of a Message, how to unpack.
 * ============================================
 *
 * On the embedded platform, when a  message is received, it is a 20 byte
 * block of data, the bytes are placed in the "packed" structure element.
 *
 * Byte0, is copied (placed) in the msg_type element.
 *
 * Then bytes1 to byte20 (see below) are copied into the union. Thus,
 * everything when accessed via the union is aligned and fits properly
 *
 * Note: But the structures are designed "correctly" padding does not occur.
 * Except for byte 0, the msg type they are designed correctly.
 *
 * The Process - Handling the Message
 * ============================================
 *
 * After unpacking, a simple switch/case is used on the  "msg_type" element.
 *
 * The Process - sending a reply.
 * ============================================
 *
 * Step 1 - The msg_type byte is bitwise inverted, and stored at byte 1
 *
 * Example: The command 0x42 would be come 0xbd.
 *
 * Step 2 - The "union" (above) is copied as bytes to the packed element
 * starting at byte 1.
 *
 * The resulting 20 byte sequence is sent via BLE.
 *
 * Issues & Error Handling
 * ============================================
 *
 * The entire transport chain host -> ble_chip (ove-the-air)
 * ble-chip -> target_platform is lossy, packets sometimes get dropped
 * due to total numbers of packets, the entire process is more akin to
 * the TCP/IP packet type "UDP" - fast but unreliable.
 *
 * Using this method, the host can get an end-to-end affirmative reply
 * from the end target by examining the bytes of the returned message, and
 * knowing that byte0 has been inverted by the end device.
 *
 * The Process - unsupported commands & parameter errors.
 * ============================================
 *
 * The host may send an unsupported command, or a supported command
 * but with values or parameters that are invalid. This can occur
 * if the device does not support the requested sensor type.
 *
 * To handle an error, the msg_type (device to host) is set to 0xff.
 * And the first 16 bytes of the entire offending message is copied
 * into the body of the error reply.
 *
 * For example: If the host sent "0x42 0x01 0x02 0x03" and
 * Command 0x42 was supported by the target - then:
 *
 * A successful reply would be 0xbd 0x01 0x02 0x 03"
 * Note here, the 0x42 inverted is 0xbd.
 *
 * An unsuccessful (illegal command, or parameter value) the reply will be
 * this byte sequence: 0xff 0x42 0x01 0x02 0x03 ...
 * Note here, the original packet is shifted by 1 byte, and the original
 * command byte 0x42 is present
 *
 */

struct sensor_message {
  uint8_t msg_type; /* really: sensor_config_cmd_t */

  union sensor_union_1 {
    sensor_common_t common;
    sensor_error_t         error;
    sensor_list_t          sensor_list;
    sensor_status_t        status;
    sensor_storage_msg_t   storage;
    sensor_imu_t           imu_config;
    sensor_adc_ltc8159_a_t ltc1859_a;
    sensor_datetime_t datetime;
    sensor_start_msg_t start;
    sensor_stop_msg_t stop;
    sensor_audio_t    audio;
    /* this is "overly-large" so that this union is always > 20 bytes */
    uint8_t safety[ 30 ];
  } unpacked;

  /**
   * This holds the raw byte stream
   *
   * NOTE: the size is 30,to match safety above.
   * In reality the message is never more then 20 bytes.
   * Byte 0 = command/msg type
   */
  uint8_t packed[ 30 ];
};

/**
* @brief config structure for LTC1859 chip
*
* Values are from the LTC1859 data sheet.
* see: https://www.analog.com/media/en/technical-documentation/data-sheets/185789fb.pdf
* Page: 15, "INPUT DATA WORD", bits [7:0] are used, [15:8] unused.
* Thus, bit[7] maps to the SGL/DIFF bit, and bit[0] is the sleep bit.
*/

typedef struct {
    sensor_common_t common; /* 8bytes */
    uint8_t chnl_config[8]; /* 8bytes */
} sensor_adc_ltc1859_a_t;

/**
* @brief BLE Sensor Config Commands
*
* All packets are 20 bytes (BLE limitation), 
* byte0 is MAJOR message type.
* For sensor config, it is always: IOP_MSG_SENSOR_CONFIG
*
* byte1 - is a MINOR message type, this code cares about this value.
*
* Rather then decorate structures with pack & unpack
* we use a few unions and byte copy the structures.
*
*/

struct sensor_config_msg {
    uint8_t msg_type; /* really: sensor_config_cmd_t */
    
    union sensor_union {
        /* this is "overly-large" so that this union is always > 20 bytes */
        char                   as_char[30];
        uint8_t                as_u8[ 30 ];
        uint16_t               as_u16[ 30 / sizeof(uint16_t) ];
        uint32_t               as_u32[ 30 / sizeof(uint16_t) ];
        
        sensor_common_t        sensor_common;
        sensor_imu_t           imu_config;
        sensor_adc_ltc1859_a_t ltc1859_a;
        sensor_datetime_t      datetime;
        sensor_audio_t         audio;
    } unpacked;
    
};

/* for the embedded side */
extern struct sensor_config_msg sensor_config_msg;

void sensor_msg_unpack( struct sensor_message *pMsg );
void sensor_msg_pack( struct sensor_message *pMsg );

#endif /* #define DCL_COMMANDS_H */
