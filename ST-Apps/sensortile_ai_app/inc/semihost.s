        MODULE  ?asmutil

        ;; Forward declaration of sections.
        SECTION CSTACK:DATA:NOROOT(3)
    
        THUMB
        PUBWEAK __semihost_call
        SECTION .text:CODE:REORDER:NOROOT(2)
__semihost_call
        push {r0,lr}
        bkpt #0xab
        pop {r1,pc}

        END
