#ifndef __PROCESS_AUDIO_H__
#define __PROCESS_AUDIO_H__

/* Audio AI processing element functions */
extern void audio_ai_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     );

extern void audio_isr_callback(void);

#endif /* __PROCESS_AUDIO_H__ */