/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : eoss3_hal_time.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __EOSS3_HAL_TIME_H
#define __EOSS3_HAL_TIME_H

#include <time.h>

//struct tm
//{       /* date and time components */
//	int tm_sec;
//	int tm_min;
//	int tm_hour;
//	int tm_mday;
//	int tm_mon;
//	int tm_year;
//	int tm_wday;
//	int tm_yday;
//	int tm_isdst;
//};
//Tim (FAT) typedef signed long time_t;

struct tm *eoss3_gmtime_r(const time_t *timer, struct tm *tmbuf);
struct tm *eoss3_localtime_r(const time_t *timer, struct tm *tmbuf);
time_t mktime(struct tm *tmbuf);
time_t eoss3_time(time_t *t);
int eoss3_sys_settime(time_t secs_since_epoch);
void periodic_cb(void);
void update_calendar(void);

intptr_t eoss3_lw_timer_start( void );
int      eoss3_lw_timer_is_expired( intptr_t token, int msecs );
int      eoss3_lw_timer_remain( intptr_t token, int msecs );
int      eoss3_lw_timer_consumed( intptr_t token );


#endif	/* __EOSS3_HAL_TIME_H */