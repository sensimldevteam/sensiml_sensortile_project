/* sensor message macros */
#if !defined(SENSOR_CONFIG_H)
#define SENSOR_CONFIG_H

/* these are used to initialize
 * const struct sensor_config_msg 
 */

/* forward */
struct sensor_config_msg;

#define SENSOR_CONFIG_ARRAY(...) {__VA_ARGS__}


#define SENSOR_CONFIG_IMU_MSG(SENSOR_ID, RATE, RANGE) \
{ \
    .msg_type=(uint8_t)IOP_COLLECTION_ADD_SENSOR, \
    .unpacked.imu_config.common.sensor_id=(uint32_t)SENSOR_ID,\
    .unpacked.imu_config.common.rate_hz=(uint32_t)RATE, \
    .unpacked.imu_config.sensor_range =(uint8_t)RANGE \
}

#define SENSOR_CONFIG_AUDIO_MSG(ID, RATE, NBITS, MIC_CFG) \
{ \
    .msg_type=(uint8_t)IOP_COLLECTION_ADD_SENSOR, \
    .unpacked.audio.common.sensor_id=(uint32_t)ID,\
    .unpacked.audio.common.rate_hz=(uint32_t)RATE, \
    .unpacked.audio.nbits=(uint8_t)NBITS, \
    .unpacked.audio.mic_config = MIC_CFG \
}

#define SENSOR_CONFIG_ADC_MSG(ID, RATE, CHANNEL_CFG) \
{ \
    .msg_type=(uint8_t)IOP_COLLECTION_ADD_SENSOR, \
    .unpacked.ltc1859_a.common.sensor_id=(uint32_t)ID,\
    .unpacked.ltc1859_a.common.rate_hz=(uint32_t)RATE, \
    .unpacked.ltc1859_a.chnl_config = CHANNEL_CFG \
}

#define SENSOR_CONFIG_CLEAR_MSG() \
{ \
    .msg_type=(uint8_t)IOP_COLLECTION_CLEAR_SENSORS\
}

/* NOTE: All canned sequences *MUST* end with *this* message */
#define SENSOR_CONFIG_DONE_MSG() \
{ \
    .msg_type=(uint8_t)IOP_COLLECTION_CONFIG_DONE\
}

extern const struct sensor_config_msg datacapture_default_config[];

typedef struct 
{
    bool isIMUEnabled;
    bool isAccelEnabled;
    bool isGyroEnabled;
    bool isMagEnabled;
    bool isAudioEnabled;
    bool isADCEnabled;   
}SensorEnableStatus;

/* configure all sensors */
extern void configure_all_sensors(void);

void sensor_config_apply_sequence( const struct sensor_config_msg *pCfg, SensorEnableStatus *pStatus );

extern void wait_for_sensor_config(void);


extern void sensor_clear(SensorEnableStatus *pStatus);
extern void sensor_add(SensorEnableStatus *pStatus);

#endif
