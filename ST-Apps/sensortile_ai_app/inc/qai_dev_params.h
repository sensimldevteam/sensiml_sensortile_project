/**
  ******************************************************************************
  * @file    qai_dev_params.h
  * @author  Indra
  * @version v0.0.1
  * @date    26-June-2019
  * @brief
  ******************************************************************************
  * @
  *
  ******************************************************************************
  */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      180                                         /**< The advertising timeout (in units of seconds). */

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                           /**< Size of timer operation queues. */


enum
{
    UNIT_0_625_MS = 625,                                /**< Number of microseconds in 0.625 milliseconds. */
    UNIT_1_25_MS  = 1250,                               /**< Number of microseconds in 1.25 milliseconds. */
    UNIT_10_MS    = 10000                               /**< Number of microseconds in 10 milliseconds. */
};

#define MSEC_TO_UNITS(TIME, RESOLUTION) (((TIME) * 1000) / (RESOLUTION))            /**< Macro for converting milliseconds to ticks. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(7.5, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
//#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)            /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(7.5, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
//#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

/**
* device information service context
* contains the handles of all the
* characteristics added to the device
* information service as specified by bitmask of
* deviceInfoCharToBeAdded field. The value of these
* characteristics cannot be set by the
* application and will have to be set
* at the profile level itself. These values are
* hard coded at the profile level. It is assumed that
* the software, firmware revisions correspond to
* the revision of the profile code
*/
typedef struct _tDevInfoService
{
  /**
  * This flag keeps track of the characteristics
  * that have to be added to the device information service
  */
  uint16_t deviceInfoCharToBeAdded;

  /**
  * handle of the device information service
  */
  uint16_t devInfoServHandle;

  /**
  * handle of the system ID characteristic
  */
  uint16_t systemIDCharHandle;

  /**
  * handle of the model number characteristic
  */
  uint16_t modelNumCharHandle;

  /**
  * handle of the serial number characteristic
  */
  uint16_t serialNumCharHandle;

  /**
  * handle of the firmware revision characteristic
  */
  uint16_t firmwareRevisionCharHandle;

  /**
  * handle of the hardware revision characteristic
  */
  uint16_t hardwareRevisionCharHandle;

  /**
  * handle of the software revision characteristic
  */
  uint16_t softwareRevisionCharHandle;

  /**
  * handle of the manufacturer name characteristic
  */
  uint16_t manufacturerNameCharHandle;

  /**
  * handle of the IEEE Certification characteristic
  */
  uint16_t ieeeCretificationCharHandle;

  /**
  * handle of the PNP ID characteristic
  */
  uint16_t pnpIDCharHandle;
}tDevInfoService;