/**@file
 *
 * @defgroup ble_sdk_srv_pme SensiML PME Service
 * @{
 * @ingroup  ble_sdk_srv
 * @brief    SensiML PME Service implementation.
 *
 * @details The SensiML PME Service is a simple GATT-based service with TX and RX characteristics.
 *          Data received from the peer is passed to the application, and the data received
 *          from the application of this service is sent to the peer as Handle Value
 *          Notifications. This module demonstrates how to implement a custom GATT-based
 *          service and characteristics using the S110 SoftDevice. The service
 *          is used by the application to send and receive ASCII text strings to and from the
 *          peer.
 *
 * @note The application must propagate S110 SoftDevice events to the SensiML PME Service module
 *       by calling the ble_pme_on_ble_evt() function from the ble_stack_handler callback.
 */

#ifndef BLE_PME_H__
#define BLE_PME_H__

#include "kb.h"
//#include "kb_defines.h"
#include "ble_pme_defs.h"


#ifndef BLE_GATT_ATT_MTU_DEFAULT
#define BLE_GATT_ATT_MTU_DEFAULT    23
#endif
#define BLE_UUID_PME_SERVICE 0x1100                      /**< The UUID of the SensiML PME Service. */
#define BLE_PME_MAX_DATA_LEN (BLE_GATT_ATT_MTU_DEFAULT - 3) /**< Maximum length of data (in bytes) that can be transmitted to the peer by the SensiML PME service module. */


#define SML_ADD_FEATURE_VECTOR 1

#if SML_ADD_FEATURE_VECTOR
//#define MAX_VECTOR_SIZE 128
//#define BLE_PME_MAX_FV_FRAME_SZ MAX_VECTOR_SIZE

#ifdef __GNUC__
	#ifdef PACKED
		#undef PACKED
	#endif

	#define PACKED(TYPE) TYPE __attribute__ ((packed))

#else

	#ifdef PACKED
		#undef PACKED
	#endif

	#define PACKED(TYPE) __packed TYPE
#endif

#endif
#if SML_ADD_SEGMENT_DATA

#define BLE_PME_MAX_SEG_FRAME_SZ 514
typedef struct
{
    uint16_t seg_len; //Actual length to read
    uint16_t data[BLE_PME_MAX_SEG_FRAME_SZ - 2]; //Max features reporting out is 128
} ble_pme_segment_t;

#endif

typedef enum
{
    BLE_PME_EVT_RCVD,
    BLE_PME_EVT_NOTIF_RESULTS,
}ble_pme_evt_type_t;

/* Forward declaration of the ble_pme_t type. */
typedef struct ble_pme_s ble_pme_t;

typedef struct evt_gatt_attr_modified_IDB05A1 evt_gatt_attr_modified_IDB05A1_t;

/**@brief SensiML PME Service event handler type. */
typedef void (*ble_pme_evt_handler_t) (ble_pme_t        * p_pme,
                                       ble_pme_evt_type_t evt_type,
                                       uint8_t          * p_data,
                                       uint16_t           length);

///**@brief SensiML PME Service initialization structure.
// *
// * @details This structure contains the initialization information for the service. The application
// * must fill this structure and pass it to the service using the @ref ble_pme_init function.
// */
//typedef struct
//{
//    ble_pme_evt_handler_t   evt_handler; /**< Event handler to be called for handling received data. */
//} ble_pme_init_t;

typedef enum {
	RECO_CLASS_ONLY=0,
	RECO_CLASS_FV,
	RECO_CLASS_SEG
} ble_pme_notif_t;

/**@brief SensiML PME Service structure.
 *
 * @details This structure contains status information related to the service.
 */
struct ble_pme_s
{
    uint8_t                  uuid_type;                    /**< UUID type for SensiML PME Service Base UUID. */
    uint16_t                 service_handle;               /**< Handle of SensiML PME Service (as provided by the S110 SoftDevice). */
    uint16_t                 pme_result_handle;             /**< Handles related to the pme results characteristic */
#if SML_ADD_FEATURE_VECTOR
    uint16_t                 feat_vector_handle;             /**< Handles related to the pme results characteristic */
#endif
#if SML_ADD_SEGMENT_DATA
    uint16_t                 segment_handle;             /**< Handles related to the pme results characteristic */
#endif
    uint16_t                 conn_handle;                  /**< Handle of the current connection (as provided by the S110 SoftDevice). BLE_CONN_HANDLE_INVALID if not in a connection. */
    ble_pme_evt_handler_t    evt_handler;                  /**< Event handler to be called for handling received data. */
    bool                     is_pme_notif_enabled;         /**< Variable to indicate PME result notifications enabled.*/
	ble_pme_notif_t          pme_notif_type;         /**< Variable to indicate PME result notifications enabled.*/
};



/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the S132 SoftDevice.
 *
 * @param[in] p_pme     PME Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
//void ble_pme_on_write(ble_pme_t * p_pme, evt_gatt_attr_modified_IDB05A1_t * p_ble_evt);
void ble_pme_on_write(ble_pme_t * p_pme,  uint16_t attr_handle, uint8_t data_length, uint8_t *p_data) ;
void ble_pme_evt_handler(ble_pme_t *p_pme, ble_pme_evt_type_t evt_type, uint8_t *p_data, uint16_t length);
uint32_t ble_pme_results_set(ble_pme_t * p_pme, ble_pme_result_t * p_data);

/**@brief Function for sending pme results data.
 *
 * @details This function sends the input orientation as an orientation characteristic notification to the peer.
 *
 * @param[in] p_pme       Pointer to the SensiML PME Service structure.
 * @param[in] p_data      Pointer to the results data.
 *
 * @retval NRF_SUCCESS If the string was sent successfully. Otherwise, an error code is returned.
 */
//uint32_t ble_pme_results_set(ble_pme_t * p_pme, ble_pme_result_t * p_data);

#if SML_ADD_FEATURE_VECTOR
uint32_t ble_pme_feature_vector_set(ble_pme_t * p_pme, ble_pme_result_w_fv_t * p_data);
#endif
#if SML_ADD_SEGMENT_DATA
uint32_t ble_pme_segment_set(ble_pme_t * p_pme, ble_pme_segment_t * p_data);
#endif

#endif // ble_pme_H__

/** @} */
