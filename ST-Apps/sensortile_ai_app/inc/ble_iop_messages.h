/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *
 *    Licensed Materials - Property of QuickLogic Corp.
 *    Copyright (C) 2019 QuickLogic Corporation
 *    All rights reserved
 *    Use, duplication, or disclosure restricted
 *
 *    File   : iop_messages.h
 *    Purpose:
 *
 *=========================================================*/

#ifndef __BLE_IOP_MESSAGES_H__
#define __BLE_IOP_MESSAGES_H__
//#include <stdint.h>
//#include <stdio.h>
#include "iop_messages.h"
#include "ble_iop_messages.h"
#include "ble_collection.h"
#include "ble_pme.h"
#include "dcl_commands.h"



#define IOP_MSG_FILENAME_LENGTH 20
#define BLE_MOTION_BUFFER_SAMPLES 			6

#define MAX_BLE_PKT_SIZE                    20  // @TODO Indra



typedef struct
{
	uint8_t cmd;
} iop_msg_header_t;

typedef struct
{
	iop_msg_header_t header;
	struct sensor_message sensor_msg;
} iop_msg_sensor_config_msg_t;

typedef struct
{
	iop_msg_header_t header;
	char filename[IOP_MSG_FILENAME_LENGTH];
} iop_msg_filename_t;

typedef struct
{
	iop_msg_header_t header;
	sensor_datetime_t datetime;
} iop_msg_datetime_t;

typedef struct
{
	iop_msg_header_t header;
	bool enable_motion;
} iop_msg_motion_toggle_t;

typedef struct
{
	iop_msg_header_t header;
	uint8_t guid_data[19];
} iop_msg_record_toggle_t;

typedef struct
{
	iop_msg_header_t header;
	uint8_t num_blocks;
	ble_motion_raw_t data[BLE_MOTION_BUFFER_SAMPLES];
} iop_msg_motion_data_t;

typedef struct
{
	iop_msg_header_t header;
	uint8_t num_blocks;
	ble_motion_raw_t data[4 * BLE_MOTION_BUFFER_SAMPLES];
} iop_msg_motion_data_ffe_batch_t;

typedef struct
{
	iop_msg_header_t header;
	bool enable_recognition;
} iop_msg_recognition_toggle_t;

typedef PACKED( struct
{
	iop_msg_header_t header;
	ble_pme_result_t pme_results;
})iop_msg_pme_result_t;

typedef PACKED( struct
{
	iop_msg_header_t header;
	ble_pme_result_w_fv_t pme_fv_results;
})iop_msg_pme_fv_result_t;

typedef struct
{
	ble_collection_t m_collection;
	ble_pme_t m_pme;
} ble_iop_connection_t;



///*
//* IMU data is in this form
//*/
//struct iop_xyz_data {
//    uint16_t x, y, z;
//};

//struct iop_imu_data {
//    /* this dummy value is here for alignment
//    * and is special cased & removed later */
//    uint8_t dummy_value;
//    uint8_t n_values;
//    struct iop_xyz_data motion[ 12 ];
//};



/* Structure for raw message between server and client */
struct iop_raw_sc_message {
    uint8_t len;
    uint8_t cmd; /* one of iop_msg_cmd_t */
    uint8_t payload[126];
};

#if defined(IOP_MESSAGE_C)
#define IOP_EXTERN
#else
#define IOP_EXTERN extern
#endif
IOP_EXTERN struct iop_globals iop_globals;

/***************************************************************
 * Function Prototypes
 ***************************************************************/

uint32_t iop_messages_init(ble_iop_connection_t *stored_cfg);

void iop_send_filename(char* filename, uint16_t length);

void iop_send_motion_toggle(bool enable);

void iop_send_record_toggle(bool enable, uint8_t *p_data);

void iop_send_recognition_toggle(bool enable, ble_pme_notif_t toggle_type);

void iop_send_audio_record_toggle(bool enable);

void iop_messages_send_next_motion();

void iop_send_sensor_config_msg(uint8_t dcl_cmd_id, uint8_t *cfg_data);


void iop_process_ble_commands( void );
/**
* @brief concatinate a series of strings as a string reply
*
* usage:  iop_reply_string( "your", "name", "here", NULL );
*
* Results in: "your name here"
*/
void iop_reply_string( const char *cp, ... );



#endif //__BLE_IOP_MESSAGES_H__
