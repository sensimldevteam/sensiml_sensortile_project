/*
 * All things 'sensor configuration' 
 * common to recognition and data collection
 */

#include "Fw_global_config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "RtosTask.h"
#include "iop_messages.h"
#include "ql_riff.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "dcl_commands.h"
//#include "FFE_AccelGyro.h"
#include "DataCollection.h"
#include "ql_fs.h"
//#include "ql_adcTask.h"
//#include "eoss3_hal_time.h"
//#include "eoss3_hal_rtc.h"
#include "dbg_uart.h"
     
#include "sensor_config.h"

SensorEnableStatus datacollection_sensor_status;
static int sensors_configured = 0;

/* in data capture mode, 
 * this is the default configuration
 */
const struct sensor_config_msg datacapture_default_config[] = {
	SENSOR_CONFIG_CLEAR_MSG(),
	SENSOR_CONFIG_IMU_MSG(SENSOR_ENG_VALUE_ACCEL,104,20),
	SENSOR_CONFIG_IMU_MSG(SENSOR_ENG_VALUE_GYRO,104,0),
	SENSOR_CONFIG_DONE_MSG()
};

struct sensor_config_msg sensor_config_msg;
     
/* given an END sample time, and a rate
 * determine when the first sample occured.
 */
void back_calculate_start_time( struct sensor_data *pInfo )
{
    int n_samples;
    int n_usecs;
    if( pInfo->time_start != 0 ){
        return;
    }

    /* calculate how many samples are present */
    n_samples = pInfo->n_bytes / pInfo->bytes_per_reading;
    
    /* Given the rate - calculate how many microseconds
     * this many samples took, rounding up
     */
    n_usecs = ((n_samples * 1000000) + (pInfo->rate_hz/2)) / pInfo->rate_hz;

    /* and thus the start time is the END minus that many uSecs */
    pInfo->time_start = pInfo->time_end - n_usecs;
}


void configure_all_sensors(void)
{
#if IMU_M4_DRIVERS
    sensor_accel_configure();
    sensor_gyro_configure();
    sensor_mag_configure();
#endif
#if LTC1859_DRIVER
    sensor_ltc1859_configure();
#endif
#if AUDIO_DRIVER
    sensor_audio_configure();
#endif
}

/*
 * Start or stop all sensor operation.
 * If the "is_start" is zero - we are stopping.
 *
 * Else, we are starting (non-zero)
 */
void
sensors_all_startstop( int is_start )
{

#if AUDIO_DRIVER
    sensor_audio_startstop( is_start );
#endif
#if IMU_M4_DRIVERS
    sensor_imu_startstop( is_start );
#endif
#if LTC1859_DRIVER
    sensor_ltc1859_startstop( is_start );
#endif    
}


/* clear stop all sensors */
void sensor_clear(SensorEnableStatus *pSensorStatus)
{
    memset( (void *)(pSensorStatus), 0, sizeof(*pSensorStatus) );
    
#if IMU_M4_DRIVERS
    sensor_accel_clear();
    sensor_gyro_clear();
    sensor_mag_clear();
#endif
#if AUDIO_DRIVER
    sensor_audio_clear();
#endif
#if LTC1859_DRIVER
    sensor_ltc1859_clear();
#endif
}


#if LTC1859_DRIVER
void sensor_ltc1859a_add(void)
{
    int x;
    int enable_bits;

    memset( (void *)(&ltc1859_task_config), 0,sizeof(ltc1859_task_config));
    
    ltc1859_task_config.frequency = 
        sensor_config_msg.unpacked.ltc1859_a.common.rate_hz;

    /* FIXME future support 8 channels */
    enable_bits = 0;
   
    for( x = 0 ; x < LTC1859_N_CHANNNELS  ; x++ ){
        ltc1859_task_config.chnl_commands[x] = 1; /* disable */
    }
    /* ble only supports channels 0 to 3 */
    for( x = 0 ; x < 4 ; x++ ){
        if( sensor_config_msg.unpacked.ltc1859_a.chnl_config[x] & 1 ){
            /* disabled */
        } else {
            ltc1859_task_config.chnl_commands[x] = 
                sensor_config_msg.unpacked.ltc1859_a.chnl_config[x];
            enable_bits |= (1 << x);
        }
    }
    
    if( (enable_bits >= 1) && (enable_bits  <= 0x0f) ){
        /* all is well */
    } else {
        enable_bits = 0;
    }

    ltc1859_task_config.channel_enable_bits = enable_bits;
    if( enable_bits ){
        sensor_ltc1859_configure();
    }
}
#endif



/* handle ADD from BLE or from RECOGNITION- */
void sensor_add(SensorEnableStatus *pSensorStatus)
{
  /* todo, add more sensors here */
    switch( sensor_config_msg.unpacked.sensor_common.sensor_id ){
#if IMU_M4_DRIVERS
    case SENSOR_ENG_VALUE_ACCEL:
        sensor_accel_add();
        pSensorStatus->isAccelEnabled = TRUE;
        pSensorStatus->isIMUEnabled = TRUE;
        break;
    case SENSOR_ENG_VALUE_GYRO:
        sensor_gyro_add();
        pSensorStatus->isIMUEnabled = TRUE;
        pSensorStatus->isGyroEnabled = TRUE;
        break;
    case SENSOR_ENG_VALUE_MAGNETOMETER:
        sensor_mag_add();
        pSensorStatus->isMagEnabled = TRUE;
        break;
#endif
#if AUDIO_DRIVER
    case SENSOR_AUDIO:
        pSensorStatus->isAudioEnabled = TRUE;
        sensor_audio_add();
        break;
#endif
#if LTC1859_DRIVER
    case SENSOR_ADC_LTC_1859_MAYHEW:
        pSensorStatus->isADCEnabled = TRUE;
        sensor_ltc1859a_add();
        break;
#endif
    default:
        dbg_str_hex32("sensor-add-unknown",sensor_config_msg.unpacked.sensor_common.sensor_id  );
        iop_set_error( IOP_ERR_UNKNOWN_SENSOR, sensor_config_msg.unpacked.sensor_common.sensor_id  );
        break;
    }
}


void sensor_config_apply_sequence( const struct sensor_config_msg *pConfigSequence, SensorEnableStatus *pSensorStatus )
{
    int n;
    
    /* we pre-increment, so back up one */
    pConfigSequence--;
    n = 0;
    do {
        n++;
        pConfigSequence++;
        sensor_config_msg = *pConfigSequence;
        
        switch( sensor_config_msg.msg_type ){
        case IOP_COLLECTION_CONFIG_DONE :
            configure_all_sensors();
            break;
        case IOP_COLLECTION_CLEAR_SENSORS:
            sensors_all_startstop( 0 );
            sensor_clear(pSensorStatus);
            break;
        case IOP_COLLECTION_ADD_SENSOR:
            sensor_add( pSensorStatus );
            break;
        default:
            dbg_fatal_error_int("invalid-sensor-sequence-cmd", (int)(sensor_config_msg.msg_type) );
            break;
        }
        if( n > 10 ){
            /* something is wrong, we probably should not have this many */
            /* did they forget to terminate the list? */
            dbg_fatal_error_int("invalid-sensor-sequence-cmd2", n);
        }
    }
    while( pConfigSequence->msg_type != IOP_COLLECTION_CONFIG_DONE )
        ;
    sensors_configured = 1;
}

void wait_for_sensor_config(void)
{
    while( ! sensors_configured ){
        vTaskDelay( 5 );
    }
}