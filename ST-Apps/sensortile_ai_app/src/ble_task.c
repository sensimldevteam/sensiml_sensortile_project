#include "Fw_global_config.h"
#include "cmsis_os.h"
#include "TargetFeatures.h"
#include "main.h"
#include "sensor_service.h"
#include "bluenrg_utils.h"
#include "ble_iop_messages.h"

TIM_HandleTypeDef TimCCHandle;

extern void InitTimers(void);

/* Private variables ---------------------------------------------------------*/
static volatile uint32_t SendEnv = 0;

uint8_t bdaddr[6];

/* Imported Variables -------------------------------------------------------------*/
extern uint8_t set_connectable;
extern int connected;
extern TIM_HandleTypeDef  TimHandle;

uint32_t ConnectionBleStatus  =0;

extern void CDC_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);


ble_iop_connection_t				m_iop_connection;  // Collection service and Device  Information service handles

extern tBleStatus add_pme_service(ble_iop_connection_t *m_iop_connection);
extern tBleStatus add_collection_service(ble_iop_connection_t *m_iop_connection);
extern tBleStatus QAI_addDeviceInformationService(void);

/**
  * @brief  Get data raw from sensors to queue
  * @param  thread not used
  * @retval None
  */
void Poll_BLE_Events(void const *argument)
{
  uint32_t StartTime;
  (void) argument;
  StartTime = HAL_GetTick();
  
  /* Infinite loop */
  while (1)
  { 
   /* Led Blinking when there is not a client connected */
    if(!connected) 
    {
      if(!TargetBoardFeatures.LedStatus) 
      {
        if(HAL_GetTick()-StartTime > 1000)
        {
          LedOnTargetPlatform();
          TargetBoardFeatures.LedStatus =1;
          StartTime = HAL_GetTick();
        }
      } 
      else 
      {
        if(HAL_GetTick()-StartTime > 50)
        {
          LedOffTargetPlatform();
          TargetBoardFeatures.LedStatus =0;
          StartTime = HAL_GetTick();
		  /* Update the BLE advertise data and make the Board connectable */
		  // Note: calling the setConnectable() api for advertising here is to allow HCI transction
		  // over SPI to through without it failing with timout. Otherwise if it called too quickly
		  // aci_gap_set_discoverable() fails with "0x0C: Command disallowed" status
          if(set_connectable)
          {
             if(setConnectable() == BLE_STATUS_SUCCESS)
             {
               set_connectable = FALSE;
             }
          }
        }
      }
    }
    
    /* handle BLE event */
    if(HCI_ProcessEvent) 
    {
      HCI_ProcessEvent=0;
      hci_user_evt_proc();
    }
    
    /* Wait for Interrupt */
    vTaskDelay(1);    //__WFI();
  }

}


/**
* @brief  Function for initializing timers for sending the information to BLE:
*  - 1 for sending MotionFX/AR/CP and Acc/Gyro/Mag
*  - 1 for sending the Environmental info
* @param  None
* @retval None
*/
void InitTimers(void)
{
  uint32_t uwPrescalerValue;
  
  /* Timer Output Compare Configuration Structure declaration */
  TIM_OC_InitTypeDef sConfig;
  
  /* Compute the prescaler value to have TIM3 counter clock equal to 10 KHz */
  uwPrescalerValue = (uint32_t) ((SystemCoreClock / 10000) - 1); 
  
  /* Set TIM1 instance (Motion)*/
  /* Set TIM1 instance */
  TimCCHandle.Instance = TIM1;
  TimCCHandle.Init.Period        = 65535;
  TimCCHandle.Init.Prescaler     = uwPrescalerValue;
  TimCCHandle.Init.ClockDivision = 0;
  TimCCHandle.Init.CounterMode   = TIM_COUNTERMODE_UP;
  if(HAL_TIM_OC_Init(&TimCCHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
  
  /* Configure the Output Compare channels */
  /* Common configuration for all channels */
  sConfig.OCMode     = TIM_OCMODE_TOGGLE;
  sConfig.OCPolarity = TIM_OCPOLARITY_LOW;
  
  /* Output Compare Toggle Mode configuration: Channel1 */
  sConfig.Pulse = uhCCR1_Val;
  if(HAL_TIM_OC_ConfigChannel(&TimCCHandle, &sConfig, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Configuration Error */
    Error_Handler();
  }  
  
}

/** @brief Initialize the BlueNRG Stack
* @param None
* @retval None
*/
void Init_BlueNRG_Stack(void)
{
  const char BoardName[11] = {DEVICE_NAME,0};
  uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
  int ret;
  uint8_t  hwVersion;
  uint16_t fwVersion;

#ifdef STATIC_BLE_MAC
  {
    uint8_t tmp_bdaddr[6]= {STATIC_BLE_MAC};
    int32_t i;
    for(i=0;i<6;i++)
      bdaddr[i] = tmp_bdaddr[i];
  }
#endif /* STATIC_BLE_MAC */

  /* Initialize the BlueNRG HCI */
  hci_init(HCI_Event_CB, NULL);

  /* get the BlueNRG HW and FW versions */
  getBlueNRGVersion(&hwVersion, &fwVersion);

  /*
  * Reset BlueNRG again otherwise we won't
  * be able to change its MAC address.
  * aci_hal_write_config_data() must be the first
  * command after reset otherwise it will fail.
  */
  hci_reset();

  HAL_Delay(100);

#ifndef STATIC_BLE_MAC
  /* Create a Unique BLE MAC */
  {
    bdaddr[0] = (STM32_UUID[1]>>24)&0xFF;
    bdaddr[1] = (STM32_UUID[0]    )&0xFF;
    bdaddr[2] = (STM32_UUID[2] >>8)&0xFF;
    bdaddr[3] = (STM32_UUID[0]>>16)&0xFF;
    bdaddr[4] = (((STLBLE_VERSION_MAJOR-48)*10) + (STLBLE_VERSION_MINOR-48)+100)&0xFF;
    bdaddr[5] = 0xC0; /* for a Legal BLE Random MAC */
  }
#else /* STATIC_BLE_MAC */

  ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET,
                                  CONFIG_DATA_PUBADDR_LEN,
                                  bdaddr);
  if(ret)
  {
    STLBLE_PRINTF("\r\nSetting Pubblic BD_ADDR failed\r\n");
    goto fail;
  }
#endif /* STATIC_BLE_MAC */

  ret = aci_gatt_init();
  if(ret)
  {
    STLBLE_PRINTF("\r\nGATT_Init failed\r\n");
    goto fail;
  }


  ret = aci_gap_init_IDB05A1(GAP_PERIPHERAL_ROLE_IDB05A1, 0,  strlen(BoardName), &service_handle, &dev_name_char_handle, &appearance_char_handle);

  if(ret != BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("\r\nGAP_Init failed\r\n");
    goto fail;
  }

  STLBLE_PRINTF("\n service_handle =%d,dev_name_char_handle =%d, appearance_char_handle=%d ", service_handle, dev_name_char_handle, appearance_char_handle);

#ifndef  STATIC_BLE_MAC
  ret = hci_le_set_random_address(bdaddr);

  if(ret)
  {
    STLBLE_PRINTF("\r\nSetting the Static Random BD_ADDR failed\r\n");
    goto fail;
  }
#endif /* STATIC_BLE_MAC */

   HAL_Delay(10);
  ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle, 0,
                                   strlen(BoardName), (uint8_t *)BoardName);
  if(ret)
  {
    STLBLE_PRINTF("\r\naci_gatt_update_char_value failed\r\n");
  }

   ret = aci_gap_set_auth_requirement(MITM_PROTECTION_NOT_REQUIRED,
                                     OOB_AUTH_DATA_ABSENT,
                                     NULL, 7, 16,
                                     USE_FIXED_PIN_FOR_PAIRING, 123456,
                                     NO_BONDING);
  if (ret != BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("\r\nGAP setting Authentication failed\r\n");
    goto fail;
  }

  STLBLE_PRINTF("SERVER: BLE Stack Initialized \r\n"
                "\t\tBoard type=%s HWver=%d, FWver=%d.%d.%c\r\n"
                  "\t\tBoardName= %s\r\n"
                    "\t\tBoardMAC = %x:%x:%x:%x:%x:%x\r\n\n",
                    "SensorTile",
                    hwVersion,
                    fwVersion>>8,
                    (fwVersion>>4)&0xF,
                    (hwVersion > 0x30) ? ('a'+(fwVersion&0xF)-1) : 'a',
                    BoardName,
                    bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]);

  /* Set output power level */
  aci_hal_set_tx_power_level(1,4);

  return;

fail:
  return;
}

/** @brief Initialize all the Custom BlueNRG services
* @param None
* @retval None
*/
void Init_BlueNRG_Custom_Services(void)
{
  int ret;

  memset(&m_iop_connection, 0, sizeof(m_iop_connection));
#if S3AI_FIRMWARE_IS_COLLECTION // Data collection mode
  // Add Data collection service
  ret = add_collection_service(&m_iop_connection);
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("Collection Service added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding Collection Service\r\n");
  }
#endif

#if S3AI_FIRMWARE_IS_RECOGNITION  
 //recognition mode
  ret = add_pme_service(&m_iop_connection);
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("PME  Service added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding PME Service \r\n");
  }
#endif
  ret = QAI_addDeviceInformationService();
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("Config  Service W2ST added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding Config Service W2ST\r\n");
  }

  

}


/**
* @brief  Output Compare callback in non blocking mode 
* @param  htim : TIM OC handle
* @retval None
*/
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
  uint32_t uhCapture=0;
  
  /* TIM1_CH1 toggling with frequency = 2Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
  {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_1, (uhCapture + uhCCR1_Val));
    SendEnv=1;
  }
}

/**
* @brief  Period elapsed callback in non blocking mode for Environmental timer
* @param  htim : TIM handle
* @retval None
*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if(htim == (&TimHandle)) 
  {
    CDC_TIM_PeriodElapsedCallback(htim);
  }
}

