#include "Fw_global_config.h"
#include <stdio.h>
#include <string.h>
#include "dcl_commands.h"
#include "iop_messages.h"
#include "stdarg.h"
#include "dbg_uart.h"
#include "eoss3_hal_time.h"
//#include "ql_adcTask.h"
#include "DataCollection.h"
#include "Sensor_Attributes.h"
//#include "FFE_AccelGyro.h"
#include "sensor_config.h"
#include "micro_tick64.h"
/*
 * This file is the "dispatch" for all SENSOR commands.
 * The MAJOR item SENSOR has already be dispatched.
 * Here, we are handling the MINOR sensor commands.
 */
#if S3AI_FIRMWARE_IS_COLLECTION


/* timestamp */
static void set_time_from_dcl( void )
{
    uint64_t v;
    eoss3_sys_settime( sensor_config_msg.unpacked.datetime.unix_time_seconds );
    v = sensor_config_msg.unpacked.datetime.unix_time_seconds;
    v = v * 1000000;
    xTaskSet_uSecCount(v);
}




/* return list of supported sensors */
static void get_list(void)
{
    uint32_t v;
    iop_globals.rsp_to_host.cmd = IOP_MSG_SENSOR_REPLY;
    iop_globals.rsp_to_host.len = 2 + sizeof(v)*3;
    iop_globals.rsp_to_host.payload[0] = IOP_MSG_SENSOR_REPLY;
    iop_globals.rsp_to_host.payload[1] = 3; /* 3 sensors */
#if FFE_DRIVERS
    v = SENSOR_ENG_VALUE_IMU_agm_base;
    v = v + SENSOR_ENG_VALUE_IMU_agm_a_bit;
    v = v + SENSOR_ENG_VALUE_IMU_agm_g_bit;
    /* mag sensor is not enabled yet */
    memcpy( &iop_globals.rsp_to_host.payload[2+(0*4)], &v, sizeof(v) );
#endif
#if AUDIO_DRIVER
    v = SENSOR_AUDIO;
    memcpy( &iop_globals.rsp_to_host.payload[2+(1*4)], &v, sizeof(v) );
#endif
#if LTC1859_DRIVER
    v = SENSOR_ADC_LTC_1859_MAYHEW;
    memcpy( &iop_globals.rsp_to_host.payload[2+(2*4)], &v, sizeof(v) );
#endif
}

/* dispatch entry */
struct sensor_cmd_dispatch_entry {
    int value;
    void (*handler)(void);
};

static void wrap_sensor_clear(void)
{
    sensor_clear( &datacollection_sensor_status );
}

static void wrap_sensor_add( void )
{
    sensor_add( &datacollection_sensor_status  );
}


/* sensor commands we support - in a table */
static struct sensor_cmd_dispatch_entry const scde_table[] = {
    //{ .value = IOP_COLLECTION_SET
    { .value = IOP_COLLECTION_CLEAR_SENSORS, wrap_sensor_clear },
    { .value = IOP_COLLECTION_ADD_SENSOR, wrap_sensor_add },
    { .value = IOP_COLLECTION_CONFIG_DONE, configure_all_sensors },
    { .value = IOP_COLLECTION_SET_UNIX_DATETIME, set_time_from_dcl },
    { .value = IOP_COLLECTION_GET_SENSOR_LIST, get_list },
    
    /* terminate */
    { .value = -1, .handler = NULL }
};

/* process the SENSOR major command
 * really all we do is dispatch the sub-command.
 */
void app_sensor_config(void)
{
    const struct sensor_cmd_dispatch_entry *pSCDE;
    
    sensor_config_msg.msg_type = iop_globals.u_cmd.as_u8[0];
    memcpy( (void *)( &sensor_config_msg.unpacked ), 
           /* remember, byte0 is the sub-cmd, we want byte 1 */
           iop_globals.u_cmd.as_u8+1, 
           20 );
    
    for( pSCDE = scde_table; pSCDE->handler != NULL ; pSCDE++ ){
        if( pSCDE->value == sensor_config_msg.msg_type ){
            (*(pSCDE->handler))();
            return;
        }
    }
    /* unknown */
    dbg_str("err-sensor-subcmd\n");
    iop_set_error( IOP_ERR_UNKNOWN_MINOR_CMD,sensor_config_msg.msg_type );
}

#endif
