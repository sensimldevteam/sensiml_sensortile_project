#include "Fw_global_config.h"

#include <stdint.h>
#include <stdio.h>

///< 0 => disable semihosting prints, 
///< 1 => enable semihosting prints
///< to generate binaries use 0 for this MACRO
///< Warning: never check-in with 1, as this would
///< cause hard-fault when used without a debugger
#define SEMIHOST_DEBUG_MODE   (0)

#if (SEMIHOST_DEBUG_MODE)
void __semihost_call(int service, uint32_t value)
{
#if defined (__IAR_SYSTEMS_ICC__)
	asm("bkpt 0xab");
#elif defined (__GNUC__)
    __asm__("bkpt 0xab");
#else
#error "Unknown compiler"
#endif
    return ;
}
#else
int __semihost_call(int service, uint32_t value)
{
  return -2;
}

int fputc(int ch, FILE * file)
{
  (void)ch;
  (void)file;
  return 0;
}

int fgetc(FILE * file)
{
  (void)file;
  return 0;
}


#endif
