/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : main.c
 *    Purpose: 
 *                                                          
 *=========================================================*/

#include "Fw_global_config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cli.h"
#include "dcl_commands.h"
#include "DataCollection.h"
#include "math.h"
#include "ql_bleTask.h"
#include "iop_messages.h"
#include "ble_collection_defs.h"
#include "ql_adcTask.h"

#include "QL_SensorCommon.h"
#include "ql_SensorHub_MemoryMap.h"
#include "QL_FFE_SensorConfig.h"
#include "sensor_cmds.h"
#include "ffe_ipc_lib.h"

#ifdef FFE_DRIVERS
#include "FFE_AccelGyro.h"
#endif

#include "dcl_commands.h"
#include "dbg_uart.h"

#if FEATURE_CLI_DEBUG_INTERFACE

static void set_dbg_flags( const struct cli_cmd_entry *pEntry )
{
  (void)pEntry;
#if DBG_FLAGS_ENABLE
  CLI_uint32_getshow( "debug-flags", &DBG_flags );
#endif
}

#if FFE_DRIVERS
static void do_watch_imu_data( const struct cli_cmd_entry *pEntry )
{
  (void)pEntry;
  watch_imu_data();
}
#endif

static void do_watch_ltc1859( const struct cli_cmd_entry *pEntry )
{
    const char *cp;
    const uint16_t *v;
    int n,x;
    int tmp;

    (void)(pEntry);
    v = ltc1859_watch_data;
    
    cp = CLI_peek_next_arg();
        
    if( cp ){
        sensor_ltc1859_startstop(0);
        
        /* let the adc stop */
        vTaskDelay(120);
        
        ltc1859_task_config.channel_enable_bits = 0;
        ltc1859_task_config.frequency = 1000;
        for( x = 0 ; x < LTC1859_MAX_CHANNELS ; x++ ){
            ltc1859_task_config.chnl_commands[x] = 1; /* disabled */
        }
        for( x = 0 ; x <LTC1859_MAX_CHANNELS ; x++ ){
            if( CLI_is_more_args() ){
                CLI_int_required( "cfg", &tmp );
                tmp = tmp & (~0x030);
                tmp = tmp | ((x << 4) & 0x070);
                ltc1859_task_config.chnl_commands[x] = tmp;
                if( tmp & 1 ){
                    /* channel is disabled */
                } else {
                    ltc1859_task_config.channel_enable_bits |= (1 << x);
                }
            }
            /* for now, we only do 4 bits */
            ltc1859_task_config.channel_enable_bits &= 0x0f;
        }
        if( ltc1859_task_config.channel_enable_bits == 0 ){
            CLI_error("must enable 1 channel\n");
        }
        sensor_ltc1859_configure();
        vTaskDelay( 10 );
        sensor_ltc1859_startstop(1);
        vTaskDelay( 10 );
        v = ltc1859_watch_data;
    if( v == NULL ){
            dbg_fatal_error("adc did not start\n");
        }
    }
    n = 0;
    for( x = 0 ; x < 8 ; x++ ){
        if( ltc1859_task_config.channel_enable_bits & (1<<x) ){
            n++;
        }
    }
    if( n == 0 ){
        CLI_printf("ADC is not configured\n");
        return;
    }
    
    /* round up to 32bit values */
    n = (n * 2); /* 2 bytes per */
    n = (n + 3) & (~3);
    n = n / 2;
    
    x = 0;
    for(;;){
        v = ltc1859_watch_data;
        if( v == NULL ){
            CLI_printf("\n\nADC STOPPED\n\n");
            break;
        }
        x -= 100;
        if( x < 0 ){
            x = 10000;
            CLI_printf("\nCfg: 0x%02x 0x%02x 0x%02x 0x%02x\n"
                       "Nchnls | v[0]  v[1]  v[2]  v[3] | uVolts ..\n",
                       ltc1859_task_config.chnl_commands[0],
                       ltc1859_task_config.chnl_commands[1],
                       ltc1859_task_config.chnl_commands[2],
                       ltc1859_task_config.chnl_commands[3]);
        }  
        CLI_printf("\r %d | %04x %04x %04x %04x | ", n, v[0], v[1], v[2], v[3] );
        CLI_printf(" % 8d | % 8d | % 8d | % 8d  ",
                   LTC1859_to_uVolts( ltc1859_task_config.chnl_commands[0], v[0] ),
                   LTC1859_to_uVolts( ltc1859_task_config.chnl_commands[1], v[1] ),
                   LTC1859_to_uVolts( ltc1859_task_config.chnl_commands[2], v[2] ),
                   LTC1859_to_uVolts( ltc1859_task_config.chnl_commands[3], v[3] ) );
        if( CLI_getkey(100) != EOF ){
            break;
        }
    }
    CLI_printf("\n\nADC Watch done\n\n");
}
            
   
        


#if FFE_DRIVERS
static void do_ffe_dump( const struct cli_cmd_entry *pEntry )
{
    (void)pEntry;
    struct QL_ExportSection *p;
    CLI_hexdump( SENSORHUB_MEMORYMAP_BASE_ADDR, (void *)(SENSORHUB_MEMORYMAP_BASE_ADDR), 256 );
    p = (struct QL_ExportSection *)(SENSORHUB_MEMORYMAP_BASE_ADDR);
    CLI_printf( "t=%d | r=%d | a=%d, c=%d\n", 
               p->ffe_tick_ms, p->m4_req_state, p->ffe_resp_state, p->ffe_interrupt_cmd );
}
#endif

const struct cli_cmd_entry my_main_menu[] = {
    CLI_CMD_SIMPLE( "flags", set_dbg_flags, "dbg-flags" ),
    CLI_CMD_SUBMENU( "std", cli_std_menu, "standard-cmds" ),
#if FEATURE_CLI_FILESYSTEM
    CLI_CMD_SUBMENU( "file", cli_file_menu, "file-commands" ),
#endif
    CLI_CMD_SUBMENU( "sensor", sensor_cmds, "sensor menu" ),
    CLI_CMD_SIMPLE( "watch-adc", do_watch_ltc1859, "watch adc data" ),
#if FFE_DRIVERS
    CLI_CMD_SIMPLE( "watch-imu", do_watch_imu_data, "watch data" ),
    CLI_CMD_SIMPLE( "ffe", do_ffe_dump, "dump-ffe"),
#endif    
    CLI_CMD_TERMINATE()
};

#endif