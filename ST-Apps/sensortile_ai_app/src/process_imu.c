/** @file process_imu.c */

/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : process_imu.c
 *    Purpose: 
 *                                                          
 *=========================================================*/

#include "Fw_global_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
   
#include "QL_SAL.h"
#include "QL_SensorIoctl.h"
#include "Sensor_Attributes.h"

#include "Recognition.h"
#include "iop_messages.h"
#include "dcl_commands.h"
#include "DataCollection.h"
#include "ble_collection_defs.h"

#include "process_ids.h"
#include "datablk_mgr.h"
#include "micro_tick64.h"

#include "dbg_uart.h"

#if (IMU_M4_DRIVERS == 1)

extern void configure_sensiml_imu_sensors(void);

/** IMU configuration for ACCEL, GYRO and MAG */
struct imu_config  imu_config;

#define MOTION_BUF_SIZE (SENSIML_FFE_MAX_BATCH_DATA_SZ * 2 )
static int              motion_count;
int                     motion_total;
/* FIX ME - should break this into accel & gyro buffers */
/* this will simlify saving and recognition code */
static ble_accel_gyro_t motion_buf[ MOTION_BUF_SIZE ];
int                      dup_imu_counter;

extern void RecognitionMotion_Batch_DataReadyMsg(void);


//static QL_SAL_SensorHandle_t sensorHandle_sensimlApp;
//unsigned int packetids_selected[MAX_SENSIML_PACKETS] = {QL_FFE_SENSOR_ID_ACCEL1, QL_FFE_SENSOR_ID_GYRO, 0, 0}; //only accel and Gyro

//static struct QL_SF_Ioctl_Set_Batch_Data_Buf batch_databuf_info;

xTaskHandle xHandleTaskFFESensors;
QueueHandle_t FFESensorsMsgQ;

/* Called by BLE DATASAVE or RECOG to "configure/apply" the mag sensor based on global */
void sensor_mag_configure(void)
{
    /* hear for completeness - work is done in accel */
}

/* Called by BLE DATASAVE or RECOG to "configure/apply" the gyro sensor based on global */
void sensor_gyro_configure(void)
{
    /* hear for completeness - work is done in accel */
}


/* Called by BLE DATASAVE or RECOG to start/stop the IMU sensors */
void sensor_imu_startstop( int is_start )
{
    is_start &= SW_ENABLE_FFE;
    imu_config.is_running = is_start;
    /* FIXME: Wake up the task and make it configure the FFE */
}

/* Called by BLE DATASAVE or RECOG to "configure/apply" the accel sensor based on global */
void sensor_accel_configure(void)
{
    
    /* disable both */
    Set_AccelGyro_SensorEnable( 0 );
    
    /* if only 1 is turned on - this platform requires BOTH */
    if( imu_config.accel.enabled ^ imu_config.gyro.enabled ){
#if  1
        /* same settings but we don't know range so choose default */
        /* copy config A -> B or B -> A */
        if( imu_config.accel.enabled ){
            /* gyro is not enabled */
            imu_config.gyro.enabled = 1;
            imu_config.gyro.rate_hz = imu_config.accel.rate_hz;
            imu_config.gyro.range = 0;
        } else {
            imu_config.accel.enabled = 1;
            imu_config.accel.rate_hz = imu_config.gyro.rate_hz;
            imu_config.accel.range = 20;
        } 
#else
        /* these must be the same */
        dbg_str("err: bad-imu-enable\n");
        /* pick one as the "extra info" */
        iop_set_error( IOP_ERR_INVALID_CONFIG, SENSOR_ENG_VALUE_ACCEL);
        return;
#endif
    }
    
    /* this platform has specific RATES for the IMU sensors validate them 
     * and calculate the BLE data rate now
     */
    int tmp;
    tmp = imu_config.accel.rate_hz;
    /* ble cannot go past 104 hz */
    if( tmp <= 104 ){
        ble_data_config.imu_data_rate_reload = 0;
    } else {
        /* calculate divisor */
        tmp = (tmp+103) / 104;
        ble_data_config.imu_data_rate_reload = tmp-1;
        ble_data_config.imu_data_rate_counter = 0;
        dbg_str_int("ble-imu-throttle", tmp );
    }
    
    /* if this is not enabled we are done */
    if( !imu_config.accel.enabled  ){
        return;
    }
    
   
    /* must both have the same rate */
    if( imu_config.accel.rate_hz != imu_config.gyro.rate_hz ){
        dbg_str("err: bad-imu-freq\n");
        iop_set_error( IOP_ERR_ILLEGAL_RATE, SENSOR_ENG_VALUE_ACCEL );
        return;
    }  

    /* this is the ffe level settings */
    configure_sensiml_imu_sensors();
}

/* disable the ACCEL sensor, called by BLE, DATASAVE or RECOG code */
void sensor_accel_clear(void)
{ 
    imu_config.accel.enabled = 0;
}

/* reads the BLE message and puts the accel config data away */
void sensor_accel_add(void)
{
    imu_config.accel.enabled = 1;
    imu_config.accel.rate_hz =sensor_config_msg.unpacked.imu_config.common.rate_hz ;
    imu_config.accel.range   = sensor_config_msg.unpacked.imu_config.sensor_range;
    switch( imu_config.accel.range ){
    case 20:
    case 40:
    case 80:
        break;
    default:
        dbg_str("err-accel-rate\n");
        iop_set_error( IOP_ERR_INVALID_PARAMETER, SENSOR_ENG_VALUE_ACCEL );
        break;
    } 
}

/* Disables the GYRO */
void sensor_gyro_clear( void )
{ 
    imu_config.gyro.enabled = 0;
}

/* reads the BLE message and puts the GYRO config data away */
void sensor_gyro_add(void)
{
    imu_config.gyro.enabled = 1;
    imu_config.gyro.rate_hz =sensor_config_msg.unpacked.imu_config.common.rate_hz ;
    imu_config.gyro.range   = sensor_config_msg.unpacked.imu_config.sensor_range;
}

/* disable the MAG sensor */
void sensor_mag_clear( void )
{
    imu_config.mag.enabled = 0;
}

/* reads the BLE message and puts the MAG sensor config data away */
void sensor_mag_add(void)
{
    if( sensor_config_msg.msg_type == IOP_COLLECTION_CLEAR_SENSORS ){
        return;
    }
    imu_config.mag.enabled = 1;
    imu_config.mag.rate_hz =sensor_config_msg.unpacked.imu_config.common.rate_hz ;
    imu_config.mag.range   = sensor_config_msg.unpacked.imu_config.sensor_range;
}

/* debug code from the CLI so you can watch the IMU values in debug */
void watch_imu_data(void)
{
    int ax,ay,az;
    int gx,gy,gz;
    ble_accel_gyro_t *pSample;
    int x;

    /* this is from the CLI, so we use cli print */
    CLI_printf("ACCEL | GYRO\n");
    for(;;){
        /* go back 16 samples */
#define AVG_SIZE 16
        
        /* where are we now */
        x = motion_count;
        /* go backwards */
        x -= AVG_SIZE;
        /* wrap */
        x = (x + MOTION_BUF_SIZE) % MOTION_BUF_SIZE;
        
        /* find start */
        pSample = &motion_buf[x];
    
        /* calculate an average value */
        ax = 0; ay = 0; az = 0;
        gx = 0; gy = 0; gz = 0;
        for( x = 0 ; x < 16 ; x++ ){
            ax += pSample->accel.x;
            ay += pSample->accel.y;
            az += pSample->accel.z;
            gx += pSample->gyro.x;
            gy += pSample->gyro.y;
            gz += pSample->gyro.z;
            pSample += 1;
            if( pSample >= &(motion_buf[MOTION_BUF_SIZE]) ){
                pSample = &motion_buf[0];
            }
        }
        ax /= AVG_SIZE;
        ay /= AVG_SIZE;
        az /= AVG_SIZE;
        gx /= AVG_SIZE;
        gy /= AVG_SIZE;
        gz /= AVG_SIZE;

        CLI_printf("\r% 5d % 5d % 5d | % 5d % 5d % 5d | %d  ",
                ax,ay,az,gx,gy,gz, motion_count );
        x = CLI_getkey( 100 );
        if( x != EOF ){
            break;
        }
    }
    CLI_printf("\nDone\n");
#undef AVG_SIZE
}

/* Set the batch size from the FFE, this also sets up the FFE memory buffers */
int g_imu_batch_size = 6; // default value
static QL_Status Set_Sensor_BatchSize(int size, int num_pkts)
{
    g_imu_batch_size = size;
    return QL_STATUS_OK;
}

int imu_batch_size_get()
{
  return g_imu_batch_size;
}

/* 
 * Configure the FFE side of the IMU sensors.
 */
void configure_sensiml_imu_sensors(void)
{
    QL_Status ret;
    int tmp;
    int batch_size;
    /* calculate batch size */
    if( imu_config.accel.rate_hz != imu_config.gyro.rate_hz ){
        dbg_str("ERROR: accel.rate_hz != gyro.rate.hz");
        dbg_str_int("accel.rate_hz", imu_config.accel.rate_hz);
        dbg_str_int(" gryo.rate_hz", imu_config.gyro.rate_hz);
    }
    int requested_freq;
    {
        if( imu_config.accel.rate_hz >= 1661 ){
            /* CLIP down to 1600 */
            imu_config.accel.rate_hz = 1660;
            imu_config.gyro.rate_hz = 1660;
            batch_size = 18;
        } else if( imu_config.accel.rate_hz >= 834 ){
            /* round up to 1660 */
            /* CLIP down to 1600 */
            imu_config.accel.rate_hz = 1660;
            imu_config.gyro.rate_hz = 1660;
            batch_size = 18;
        } else if( imu_config.accel.rate_hz >= 417 ){
            /* round up to 833 */
            imu_config.accel.rate_hz = 833;
            imu_config.gyro.rate_hz = 833;
            batch_size = 10;
        } else if( imu_config.accel.rate_hz >= 209 ){
            /* round up to 416 */
            imu_config.accel.rate_hz = 416;
            imu_config.gyro.rate_hz = 416;
            batch_size = 6;
        } else if( imu_config.accel.rate_hz >= 105 ){
            /* round up to 208 */
            imu_config.accel.rate_hz = 208;
            imu_config.gyro.rate_hz = 208;
            batch_size = 6;
        } else if( imu_config.accel.rate_hz >= 53 ){
            /* round up to 104 */
            imu_config.accel.rate_hz = 104;
            imu_config.gyro.rate_hz = 104;
            batch_size = 4;
        } else if( imu_config.accel.rate_hz >= 27 ){
            /* round up to 52 */
            imu_config.accel.rate_hz = 52;
            imu_config.gyro.rate_hz = 52;
            batch_size = 4;
        } else {
            /* default is 26 */
            imu_config.accel.rate_hz = 26;
            imu_config.gyro.rate_hz = 26;
            batch_size = 4;
        }
        requested_freq = imu_config.accel.rate_hz;
        switch( requested_freq ){
        case 52:
            requested_freq = 51; /* workaround */
            break;
        case 208:
            requested_freq = 207; /* workaround */
            break;
        default:
            /* no change */
            break;
        }
    }

   /*
    * Example code to enable/disable Accel and Gyro sensors and
    * updating attributes Range and ODR.
    */

    /* Enable Accel Sensor */
    ret = Sensor_Enable(QL_SAL_SENSOR_ID_ACCEL, ENABLE_SENSOR);
    QL_ASSERT(ret == QL_STATUS_OK);

    /* Enabling GYRO Sensor */
    ret = Sensor_Enable(QL_SAL_SENSOR_ID_GYRO, ENABLE_SENSOR);
    QL_ASSERT(ret == QL_STATUS_OK);

    switch( imu_config.accel.range ){
    default:
        dbg_fatal_error_hex32("invalid accel range", imu_config.accel.range );
        break;
        /* fallthrough */
    case 20: tmp = Accel_Sensor_Range_2G; break;
    case 40: tmp = Accel_Sensor_Range_4G; break;
    case 80: tmp = Accel_Sensor_Range_8G; break;
    case 160:  tmp = Accel_Sensor_Range_16G; break;
    }
    
    ret = Set_Sensor_Range(QL_SAL_SENSOR_ID_ACCEL, tmp);
    QL_ASSERT(ret == QL_STATUS_OK);

    switch( imu_config.gyro.range ){
    default:
        dbg_str_int("invalid-gyro-range",imu_config.gyro.range);
        tmp = Gyro_Sensor_Range_2000DPS;
        break;
        /* fallthrough */
    case 0: tmp = Gyro_Sensor_Range_2000DPS; break;
    case 1: tmp = Gyro_Sensor_Range_1000DPS; break;
    case 2: tmp = Gyro_Sensor_Range_500DPS; break;
    case 3: tmp = Gyro_Sensor_Range_245DPS; break;
    case 4: tmp = Gyro_Sensor_Range_125DPS; break;
    }
    
    Set_Sensor_BatchSize(batch_size, 2); //There are 2 pkts (Accel and Gyro) generated for each sample period        

    ret = Set_Sensor_Range(QL_SAL_SENSOR_ID_GYRO, tmp);
    QL_ASSERT(ret == QL_STATUS_OK);

    ret = Set_AccelGyro_SensorODR( requested_freq );
    QL_ASSERT(ret == QL_STATUS_OK);
}
#if 0
/* Time to configure the IMU subsystem. */
void sensor_imu_configure(void)
{
    //for sensortile testing only
    imu_config.accel.enabled = 1;
    imu_config.gyro.enabled = 1;
    imu_config.mag.enabled = 1;

    
    last_imu_timestamp  = 0;
    if( !imu_config.accel.enabled ){
        return;
    } 

    /* TODO: how do I configure the IMU here?
     * NOTE: today above configuration is hard coded
     * Future must support "reasonable configurations"
     *
     * Example A few different IMU frequencies.
     * Such as:  8khz vrs 16khyz
     *
     * Example: Different microphone configurations.
     * given that 0= microphone disabled.
     * and that 1 = microphone platform default.
     * Are there other microphone configurations?
     * NOTE: While the merced board may not
     * other platforms (boards) may have options.
     */
}
#endif
extern void send_message_to_datalog(int16_t *pbuffer);
#if 0 // for sampling rate validation
static uint32_t imu_sample_count = 0;
static uint32_t imu_time_prev = 0;
#endif
void imu_ai_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     )
{
    size_t nSamples;
    int    nChannels;
    int16_t* pBuffer;
    struct sensor_data sdi; 
    uint64_t  time_start, time_curr, time_end, time_incr;
    
    pBuffer = (int16_t *)(pIn->p_data);
    
    nSamples = pIn->dbHeader.numDataElements;
    nChannels = pIn->dbHeader.numDataChannels;

    /* if not enabled... then throw data away */
    if( !(imu_config.accel.enabled || 
          imu_config.mag.enabled || 
          imu_config.gyro.enabled 
          )
       )
    {
        return;
    }
  
    time_start = convert_to_uSecCount(pIn->dbHeader.Tstart);
    time_incr  = ((uint64_t)(pIn->dbHeader.Tend - pIn->dbHeader.Tstart) * 1000) / (nSamples / nChannels);
    time_curr  = time_start;
#if 0 // for sampling rate validation
    imu_sample_count += (nSamples / nChannels);
    if ( imu_sample_count >= 1000 ) {
      imu_time_prev = pIn->dbHeader.Tend - imu_time_prev;
      printf("imu_sample_count: %ld, time: %ld, diff = %ld, sample_rate: %ld\n", 
             imu_sample_count, pIn->dbHeader.Tend, imu_time_prev,
             (imu_sample_count * 1000 ) / (imu_time_prev) );
      imu_sample_count = 0;
      imu_time_prev = pIn->dbHeader.Tend;
    }
#endif
    for (int k = 0; k < nSamples; k += nChannels)
    {
        //send_message_to_datalog(pBuffer);
        time_end = (time_curr + time_incr);
        // send IMU data to recognition task
        memset( (void *)&(sdi), 0, sizeof(sdi) );
        sdi.bytes_per_reading = sizeof(ble_accel_gyro_t);  //sizeof( pStart->accel );
        sdi.n_bytes           = sizeof(ble_accel_gyro_t);  //sizeof( pStart->accel );
        sdi.rate_hz           = imu_config.accel.rate_hz;
        sdi.vpData            = (void *)pBuffer;
        sdi.sensor_id         = SENSOR_ENG_VALUE_IMU_agm_a_bit +
                                SENSOR_ENG_VALUE_IMU_agm_g_bit + 
                                SENSOR_ENG_VALUE_IMU_agm_base;
        sdi.time_start        = time_curr;
        sdi.time_end          = time_end;
#if S3AI_FIRMWARE_IS_RECOGNITION
        recog_data( &sdi );
#endif        

        // send IMU data to recognition task
        memset( (void *)&(sdi), 0, sizeof(sdi) );
        sdi.bytes_per_reading = sizeof(ble_xyz16_t);  //sizeof( pStart->accel );
        sdi.n_bytes           = sizeof(ble_xyz16_t);  //sizeof( pStart->accel );
        sdi.rate_hz           = imu_config.accel.rate_hz;
        sdi.vpData            = (void *)pBuffer;
        sdi.sensor_id         = SENSOR_ENG_VALUE_ACCEL;
        sdi.time_start        = time_curr;
        sdi.time_end          = time_end;   
#if S3AI_FIRMWARE_IS_COLLECTION
        data_save( &sdi );
#endif
        ble_send( &sdi );

        // send IMU data to recognition task
        memset( (void *)&(sdi), 0, sizeof(sdi) );
        sdi.bytes_per_reading = sizeof(ble_xyz16_t);  //sizeof( pStart->accel );
        sdi.n_bytes           = sizeof(ble_xyz16_t);  //sizeof( pStart->accel );
        sdi.rate_hz           = imu_config.gyro.rate_hz;
        sdi.vpData            = (void *)( (uint32_t)pBuffer + sizeof(ble_xyz16_t) );
        sdi.sensor_id         = SENSOR_ENG_VALUE_GYRO;
        sdi.time_start        = time_curr;
        sdi.time_end          = time_end;
#if S3AI_FIRMWARE_IS_COLLECTION    
        data_save( &sdi );
#endif
        ble_send( &sdi );
        time_curr = (time_curr + time_incr);
        
        pBuffer += nChannels ;
    }

    //back_calculate_start_time( &sdi );
    *pRet = NULL;
    return;
}

#endif // IMU_M4_DRIVERS