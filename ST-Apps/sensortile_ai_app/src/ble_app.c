/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : datablk_mgr.c
 *    Purpose: 
 *                                                          
 *=========================================================*/

/*
 * All things BLE at the Application level for S3AI
 */

#include "Fw_global_config.h"
#include "iop_messages.h"
#include "ble_iop_messages.h"
#include "DataCollection.h"
#include "dcl_commands.h"
#include "dbg_uart.h"
     
/* we collect IMU data here before sending data to the BLE interface
 * Only because historically we always sent IMU data as a pair of sensor valus.
 */

 
struct ble_data_config ble_data_config;

uint8_t *my_ble_callback( uint8_t *pData )
{
  /* BLE has sent us a new command */
  /* wack the old command */
  memset( (void *)(&iop_globals.cmd_from_host), 0, sizeof(iop_globals.cmd_from_host) );
    
  /* extract len & cmd */
  iop_globals.cmd_from_host.len = pData[0];
  iop_globals.cmd_from_host.cmd = pData[1];
  
  /* payload starts at data+2 */
  memcpy( iop_globals.cmd_from_host.payload, pData+2, iop_globals.cmd_from_host.len-2 );
  /* process the command */
  iop_process_command();
  return ((void *)(&iop_globals.rsp_to_host));
}
    

     
 struct ble_imu {
    int bits;
#define IMU_ACCEL_IDX 0
#define IMU_GYRO_IDX  1
    struct iop_xyz_data xyz[2];
};

static struct ble_imu ble_imu_data;

void ble_send( const struct sensor_data *pInfo )
{
  void *pData;

    do {
        /* if this is not enabled, skip */
        if( !(iop_globals.cur_status.status_bits & IOP_STATUS_BIT_sensor) ){
            break;
        }

	/* if this sensor is not selected, skip */
        if( pInfo->sensor_id != ble_data_config.sensor_data_id_major ){
            break;
        }

	/* send every value?
	 * Or send every Nth value
	 */
        if( ble_data_config.sensor_data_rate_reload != 0 ){
	  /* Send every Nth value */
            if( ble_data_config.sensor_data_rate_counter > 0 ){
                /* send every (n+1) reading */
                ble_data_config.sensor_data_rate_counter--;
                break;
            }
	    /* Time to send the data 
	     * reload the downcounter
	     */
            ble_data_config.sensor_data_rate_counter = 
                ble_data_config.sensor_data_rate_reload;
        } else {
            /* we send every sample, no reload */
        }
        /* TODO: Write this code to send this sample */
    } while(0)
        ;

    do {
      /* is the IMU data enabled or disabled to ble? */
        if( !(iop_globals.cur_status.status_bits &  IOP_STATUS_BIT_imu) ){
            break;
        }

        pData = NULL;
        if( pInfo->sensor_id == SENSOR_ENG_VALUE_GYRO ){
            pData = (void *)(&ble_imu_data.xyz[IMU_GYRO_IDX]);
            ble_imu_data.bits |= 1;
        } else if( pInfo->sensor_id == SENSOR_ENG_VALUE_ACCEL ){
            ble_imu_data.bits |= 2;
            pData = (void *)(&ble_imu_data.xyz[IMU_ACCEL_IDX]);
        } else {
            break;
        }
        if( pInfo->bytes_per_reading != 6 ){
            dbg_fatal_error("FIXME only supports 6bytes per reading here\n");
        }
	/* copy our data so we can send it with the other value */
        memcpy( pData, pInfo->vpData, 6 );
	
        /* need both values before we can send
         * FIXME - other platforms may have different needs
         * What if one sensor is disabled? What then?
	 * What if we only want to send ACCEL not GYRO?
         */

	/* 3 means both sensor data is present */
        if( ble_imu_data.bits != 3 ){
            break;
        }
	/* clear */
        ble_imu_data.bits = 0;

	/* every one? Or every Nth value one? */
        if( 0 == ble_data_config.imu_data_rate_reload ){
            /* always send */
        } else {
            /* send every (N+1)th reading */
            if( ble_data_config.imu_data_rate_counter > 0 ){
                ble_data_config.imu_data_rate_counter--;
                break;
            }
            ble_data_config.imu_data_rate_counter = 
                ble_data_config.imu_data_rate_reload;
        }
#if 0 /* DEBUG DATA */
        /* hard coded values for testing only */
        ble_imu_data.xyz[0].x = 1000;
        ble_imu_data.xyz[0].y = 2000;
        ble_imu_data.xyz[0].z = 3000;
        ble_imu_data.xyz[1].x = 4000;
        ble_imu_data.xyz[1].y = 5000;
        ble_imu_data.xyz[1].z = 6000;
#endif
        iop_send_motion_data(2, (struct iop_xyz_data *)(&ble_imu_data.xyz[0]));
    } while(0)
        ;
}
