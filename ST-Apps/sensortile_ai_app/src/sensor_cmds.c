#include "Fw_global_config.h"

#include "dcl_commands.h"
#include "iop_messages.h"
#include "cli.h"
#include "string.h"
#include "sensor_cmds.h"
#include "DataCollection.h"

#define cmd_reset cli_cmd_buff_reset
#define wr_u8     cli_cmd_buff_wr_u8
#define wr_u16    cli_cmd_buff_wr_u16
#define wr_u32    cli_cmd_buff_wr_u32

#if 0
/*
 * this is the CLI commands to simulate the BLE commands.
 */

static uint8_t raw_bytes[128];

/* clear the command buffer */
static void cmd_reset(void)
{
  memset( (void *)(&raw_bytes[0]), 0, sizeof(raw_bytes) );
}

/* Add an 8bit value to the COMMAND */
static void wr_u8( int v )
{
  /* length is byte 0 */
  raw_bytes[ 1 + raw_bytes[0] ] = (uint8_t)(v);
  raw_bytes[0] += 1;
}

/* Add an 16bit value to the COMMAND */
static void wr_u16( int v )
{
  wr_u8( v >> 0 );
  wr_u8( v >> 8 );
}
  
/* Add an 32bit value to the COMMAND */
static void wr_u32( uint32_t v )
{
  wr_u16( v >>  0 );
  wr_u16( v >> 16 );
}
#endif

/* we have constructed the packet
 * our next step is to pretend that we have received the SPI packet.
 */
static void do_dispatch( void )
{
  memcpy( (void *)(&iop_globals.cmd_from_host),
         raw_bytes,
         20 );

  /* call the command processor */
  iop_process_command();
}

/* configure sensors */
static void do_configure( const struct cli_cmd_entry *pEntry )
{
  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_CONFIG );
  wr_u8( IOP_COLLECTION_CONFIG_DONE );
  do_dispatch();
}

/* version command */
static void do_version( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);

  cmd_reset();
  wr_u8( IOP_MSG_GET_VERSION );
  do_dispatch();
}

/* request model GUID */
static void do_model_guid( const struct cli_cmd_entry *pEntry )
{
  (void)pEntry;
  cmd_reset();
  wr_u8( IOP_MSG_GET_MODEL_GUID );
  do_dispatch();
}

/* request compile string */
static void do_compilestamp( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);

  cmd_reset();
  wr_u8( IOP_MSG_GET_COMPDATETIME );
  do_dispatch();
}

/* sensor clear */
static void do_clear( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);

  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_CONFIG );
  wr_u8( IOP_COLLECTION_CLEAR_SENSORS );
  do_dispatch();
}

/* list sensors */
static void do_list( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);

  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_CONFIG );
  wr_u8( IOP_COLLECTION_GET_SENSOR_LIST );
  do_dispatch();
}

/* request and print status */
static void do_status( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);
  
  cmd_reset();
  wr_u8( IOP_MSG_GET_STATUS );
  CLI_printf("Status: bytes-stored: %d\n", iop_globals.cur_status.bytes_stored);
  CLI_printf("Status:   bits: %d\n", iop_globals.cur_status.status_bits);
  CLI_printf("Status:  sd-oe: %d\n", iop_globals.cur_status.sdcard_overrun_count);
  CLI_printf("Status: ble-oe: %d\n", iop_globals.cur_status.ble_overrun_count);
  CLI_printf("Status: ble-tx: %d\n", iop_globals.cur_status.ble_tx_packets);
  CLI_printf("Status: ble-rx: %d\n", iop_globals.cur_status.ble_rx_packets);
  CLI_printf("Status: ble-rx: %d\n", iop_globals.cur_status.ble_rx_packets);
  CLI_printf("Status: errs: %d error: %d\n", iop_globals.cur_status.error_counter, iop_globals.cur_status.error_num );
  do_dispatch();
}


/* common code for all sensor add commands */
static void do_sensor_add_common( uint32_t sensor_id )
{
  uint32_t rate;
  
  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_CONFIG );
  wr_u8( IOP_COLLECTION_ADD_SENSOR );
  wr_u32( sensor_id );
  CLI_uint32_required( "rate_hz", &rate );
  wr_u32( rate );
}

/* common for all imu sensors */
static void do_imu_common( uint32_t sensor_id )
{
  uint8_t range;
  /* all imu sensors with ENG values have this form:
   *  
   *  command RATE  RANGE
   */
  do_sensor_add_common( sensor_id );
  CLI_uint8_required( "range", &range );
  wr_u8( range );
  do_dispatch();
}

/* handle ACCEL */
static void do_imu_accel( const struct cli_cmd_entry *pEntry )
{
  /* handle accel enable/disable */
  do_imu_common( SENSOR_ENG_VALUE_ACCEL );
}


/* handle GYRO */
static void do_imu_gyro( const struct cli_cmd_entry *pEntry )
{
  /* handle gyro enable/disable */
  do_imu_common( SENSOR_ENG_VALUE_GYRO );
}

/* handle MAG sensor */
static void do_imu_mag( const struct cli_cmd_entry *pEntry )
{
  /* handle magometer enable/disable */
  do_imu_common( SENSOR_ENG_VALUE_MAGNETOMETER );
}

/* IMU sub commands */
static const struct cli_cmd_entry imu_cmds[] =
  {
   CLI_CMD_SIMPLE( "accel", do_imu_accel, "usage: rate range"),
   CLI_CMD_SIMPLE( "gyro", do_imu_gyro, "usage: rate range"),
   CLI_CMD_SIMPLE( "mag", do_imu_mag, "usage: rate range"),
   CLI_CMD_TERMINATE()
  };

/* ADC command */
static void do_ltc1859a( const struct cli_cmd_entry *pEntry )
{
  /* handle ltc1859a adc chip */
  (void)(pEntry);
  uint8_t ch_cfg;
  /*
  * usage:  COMMAND  RATE   CH0_CFG CH1_CFG CH2_CFG CH3_CHG
  *
  * Use 0xFF, or -1 to disable channel.
  */
  
  do_sensor_add_common(SENSOR_ADC_LTC_1859_MAYHEW);
  CLI_uint8_required( "chnl0-cfg", &ch_cfg );
  wr_u8( ch_cfg );
  CLI_uint8_required( "chnl1-cfg", &ch_cfg );
  wr_u8( ch_cfg );
  CLI_uint8_required( "chnl2-cfg", &ch_cfg );
  wr_u8( ch_cfg );
  CLI_uint8_required( "chnl3-cfg", &ch_cfg );
  wr_u8( ch_cfg );
  do_dispatch();
}

/* audio command */
static void do_audio( const struct cli_cmd_entry *pEntry )
{
  uint8_t v;
  /* usage: COMMAND RATE  NBITS MIC0 MIC1 MIC2 MIC3 MIC4 MIC5 MIC6 MIC7 */
  do_sensor_add_common(SENSOR_AUDIO );

  CLI_uint8_required( "nbits", &v );
  wr_u8( v );
  CLI_uint8_required( "mic0-cfg", &v );
  wr_u8( v );
  CLI_uint8_required( "mic1-cfg", &v );
  wr_u8( v );
  CLI_uint8_required( "mic2-cfg", &v );
  wr_u8( v );
  CLI_uint8_required( "mic3-cfg", &v );
  wr_u8( v );
  CLI_uint8_required( "mic4-cfg", &v );
  wr_u8( v );
  CLI_uint8_required( "mic5-cfg", &v );
  wr_u8( v );
  CLI_uint8_required( "mic6-cfg", &v );
  wr_u8( v );
  CLI_uint8_required( "mic7-cfg", &v );
  wr_u8( v );
  do_dispatch();
}

/* sensor types, add more sensors here */
static const struct cli_cmd_entry sensor_add_cmds[] =
  {
   CLI_CMD_SUBMENU( "imu", imu_cmds, "cofigure IMU sensors"),
   CLI_CMD_SIMPLE( "ltc1859a", do_ltc1859a, "configure ltc1859(a)"),
   CLI_CMD_SIMPLE( "audio", do_audio, "configure audio capture"),
   CLI_CMD_TERMINATE()
  };

/* set clock */
static void do_datetime( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);
  uint32_t v;

  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_CONFIG );
  wr_u8( IOP_COLLECTION_SET_UNIX_DATETIME );
  CLI_uint32_required( "datetime-seconds", &v);
  wr_u32( v );
  do_dispatch();
}

/* start saving */
static void do_start( const struct cli_cmd_entry *pEntry )
{
  (void)pEntry;
  char *cp;

  cmd_reset();
  wr_u8( IOP_MSG_STORAGE_START );
  /* technically, GUIDs are binary data but we are lazy for testing
   * and only allow the test process to enter ascii text 
   */
  CLI_string_ptr_required( "guid", &cp );
  while( *cp ){
    wr_u8( *cp );
    cp++;
  }
  do_dispatch();
}

/* stop saving */
static void do_stop( const struct cli_cmd_entry *pEntry )
{
  (void)pEntry;

  cmd_reset();
  wr_u8( IOP_MSG_STORAGE_STOP );
  do_dispatch();
}

/* turn on BLE imu data */
static void do_imu_start( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);
  cmd_reset();
  wr_u8( IOP_MSG_IMU_DATA_START );
  do_dispatch();
}

/* turn off BLE imu data */
static void do_imu_stop( const struct cli_cmd_entry *pEntry )
{
  (void)(pEntry);
  cmd_reset();
  wr_u8( IOP_MSG_IMU_DATA_STOP );
  do_dispatch();
}


/* select specific sensor by id */
static void do_ble_sensor_id( const struct cli_cmd_entry *pEntry )
{
  uint32_t id;
  CLI_uint32_getshow( "sensor-id", &id );

  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_SELECT_BY_ID );
  wr_u32( id );
  wr_u32( 0xFFFFFFFF ); /* all sub-channels */
  do_dispatch();
}


static void do_ble_sensor_start( const struct cli_cmd_entry *pEntry )
{
  /* turn on selected sensor over ble */
  (void)(pEntry);
  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_START );
  do_dispatch();
}

static void do_ble_sensor_stop( const struct cli_cmd_entry *pEntry )
{
  /* turn off selected sensor over ble */
  (void)(pEntry);
  cmd_reset();
  wr_u8( IOP_MSG_SENSOR_STOP );
  do_dispatch();
}


/* sub-command for selected sensor */
static struct cli_cmd_entry ble_sensor_menu[] = {
  CLI_CMD_SIMPLE( "sensor-id", do_ble_sensor_id, "select-ble-sensor" ),
  CLI_CMD_SIMPLE( "sensor-start", do_ble_sensor_start, "start-ble-sensor" ),
  CLI_CMD_SIMPLE( "sensor-stop", do_ble_sensor_stop, "stop-ble-sensor" ),
  CLI_CMD_TERMINATE()
};

/* command for data save filename */
static void do_filename_cmd( const struct cli_cmd_entry *pEntry )
{
  char *cp;
  (void)(pEntry);

  cmd_reset();
  CLI_string_ptr_required(  "filename", &cp );
  wr_u8( IOP_MSG_STORAGE_FILENAME);
  while( *cp ){
    wr_u8( *cp );
    cp++;
  }
  wr_u8(0);
  do_dispatch();
}

/* save flags */
static void do_file_config_cmd(  const struct cli_cmd_entry *pEntry )
{
  uint32_t v;
  
  cmd_reset();
  CLI_uint32_getshow( "config-value", &v );
  wr_u8( IOP_MSG_STORAGE_CONFIG );
  wr_u32( v );
  do_dispatch();
}

/* sensor sub menu */
const struct cli_cmd_entry sensor_cmds[] =
  {
   CLI_CMD_SIMPLE( "version", do_version, "get-version" ),
   CLI_CMD_SIMPLE( "compiler-stamp", do_compilestamp, "get-compile-timestamp"),
   CLI_CMD_SIMPLE( "model-guid", do_model_guid, "get guid of model" ),
   CLI_CMD_SIMPLE( "imu-start", do_imu_start, "start ble imu data" ),
   CLI_CMD_SIMPLE( "imu-stop", do_imu_stop, "stop ble imu data" ),
   CLI_CMD_SUBMENU(   "ble-sensor", ble_sensor_menu, "select sensor for ble data"),
   CLI_CMD_SIMPLE( "clear", do_clear, "clear sensors" ),
   CLI_CMD_SIMPLE( "list", do_list, "list sensors" ),
   CLI_CMD_SIMPLE( "status", do_status, "get status" ),
   CLI_CMD_SIMPLE( "done",   do_configure, "done-configuring" ),
   CLI_CMD_SUBMENU( "add", sensor_add_cmds, "add sensors" ),
   CLI_CMD_SIMPLE( "filename", do_filename_cmd, "set-filename-prefix" ),
   CLI_CMD_SIMPLE( "file-config", do_file_config_cmd, "set file config" ),
   CLI_CMD_SIMPLE( "datetime", do_datetime, "set datetime"),
   CLI_CMD_SIMPLE( "start", do_start, "start data collection"),
   CLI_CMD_SIMPLE( "stop", do_stop, "stop data collection" ),
   CLI_CMD_TERMINATE()
  };
