# SensiML SensorTile Project

This repository contains source code used to set up data collection and recognition in SensiML Software.

It does **not** contain the ST MicroElectronics Software Development Kit necessary for building the full applications.

## Tools used

The instructions for this guide were tested with the following:

| Software               | Version | Link                                                                                                                                      |
|------------------------|---------|-------------------------------------------------------------------------------------------------------------------------------------------|
| IAR Embedded Workbench | 7.60.2.1135  | [IAR Home](https://www.iar.com/)                                                                                                          |
| GNU Make For Windows   | 3.81    | [SourceForge](https://sourceforge.net/projects/gnuwin32/files/make/3.81)                                                                  |
| Arm GCC Toolchain (arm-none-eabi)      | 8.2.1 (8-2018-q4-major)  | [ARM Developer Center](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads/)                                             |
| SensorTile SDK         | 2.1.1   | [STMicro SensorTile Software](https://www.st.com/content/st_com/en/products/embedded-software/evaluation-tool-software/stsw-stlkt01.html) |


## Linking to the SensorTile SDK

After you have downloaded the SDK, you will need to let either IAR or GCC/Make know how to build the application.

### IAR

1. Navigate to ST-Apps/sensortile_ai_app/IAR_Project.

2. Find the file `sensortile_ai_app.custom_argvars` Here you will need to fill in the file data:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<iarUserArgVars>
    <group name="SENSIML" active="true">
        <variable>
            <name>STSW_STLKT01</name>
            <value>FILL_ME_IN</value>
        </variable>
    </group>
</iarUserArgVars>

```
3. Here you will put the path to where you have unzipped the SensorTile SDK where you see `FILL_ME_IN`
4. Load the workspace/project in IAR by opening the `sensortile_ai_app.eww` file in IAR.

### GCC

1. Navigate to ST-Apps/sensortile_ai_app/GCC_Project

2. Find the file `sensortile_ai_app.custom_argvars`

3. Uncomment the last line of the file:
```makefile
# Copy this file to sensortile_ai_app.custom_argvars

# Uncomment the following line, and edit the variable name STSW-STLKT01
# and modify its associated value to point to the location of the SensorTile SDK
# installation directory.

#export STSW_STLKT01=FILL_ME_IN
```
4. Here you will put the path to where you have unzipped the SensorTile SDK where you see `FILL_ME_IN`
5. From here, you should be able to run `make` and see the build.

## Building For data collection

1. Open the file `ST-Apps/sensortile_ai_app/inc/Fw_global_cfg.h`
2. Search for `#define S3AI_FIRMWARE_MODE      S3AI_FIRMWARE_MODE_COLLECTION` (Line 37)
3. Ensure this is the only mode defined in the file.
4. Build with your chosen build system.

## Building A Knowledge Pack

The slide demo is included as a Knowledge Pack for this repository.

To link your own Knowledge Pack, follow instructions on the [SensiML Support Page](https://sensiml.atlassian.net/wiki/spaces/SS/pages/214859780/Compiling+a+KnowledgePack+with+SensorTile)

