/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : TypeDefs.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef	__TYPEDEFS_H__
#define	__TYPEDEFS_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _MSC_VER
typedef __int16 int16_t;
typedef unsigned __int16 uint16_t;
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
typedef unsigned __int32 uintptr_t ;
#else
#include <stdint.h>
#endif

typedef	signed char				INT8 ;
typedef	unsigned char				UINT8 ;
typedef	int16_t					INT16 ;
typedef	uint16_t				UINT16 ;
typedef	int32_t					INT32 ;
typedef	uint32_t				UINT32 ;
typedef	long long				INT64 ;
typedef	unsigned long long			UINT64 ;
typedef	long long				INT40 ;
typedef	unsigned long long			UINT40 ;

#ifdef __cplusplus
}
#endif

#endif

