#include <stdio.h>
#include "core_cm4.h"

//#include <stdlib.h>
//#include <string.h>
#include <math.h>
//#include <sys/time.h>
#include "TypeDefs.h"
//#include "QL_InterpBy3.h"

//#include "Input1.h"
#include "1000Hz.h"
//#include "2000Hz.h"
//#include "3000Hz.h"
//#include "4000Hz.h"
//#include "5000Hz.h"
//#include "6000Hz.h"
//#include "7000Hz.h"
//#include "4000Hz_10s.h"

/*Cycles for 15ms                   0x00000000 addr  */
//#include "Alexa16k.h"             //121627       
//#include "Cancel16k.h"            //121627        
//#include "Equalizer16k.h"         //117715        
//#include "HelloBG16k.h"           //117715        
//#include "HelpMe16k.h"            //121627        
//#include "NextSong16k.h"          //121627        
//#include "OKQL16k.h"              //117715        
//#include "PauseMusic16k.h"        //117715        
//#include "PlayMusic16k.h"         //121627        
//#include "PreviousSong16k.h"      //121627        
//#include "Settings16k.h"          //121627        
//#include "StopMusic16k.h"         //  117715



//INT32 WorkBuffer [SRC_SCRATCH_MEMORY_SIZE] ;
//INT32 SRCMemory[SRC_STATE_MEMORY_SIZE] ;

extern int convert_16_to_48(int16_t *input_16K, int16_t *output_48K);
extern int init_interpolate_by_3(void);

void printOutputToTeraTerm(UINT8 * out, INT16 blockSizeInBytes)
{
  int i;
  for (i = 0; i < blockSizeInBytes; i++)
  {
      printf("%02x ",out[i]);
  }
}

void PrepareInput(UINT8* dst, UINT8* src, INT16 sizeInBytes)
{
  int i;
  for (i = 0; i<sizeInBytes; i++)
  {
      *dst = *src;
      dst++;
      src++;
  }
}

#define INPUT_BLK_SIZE  240 //240 SAMPLES = 15ms
INT16 input[INPUT_BLK_SIZE], src_txoutput[INPUT_BLK_SIZE * 3] ;
#define CYCLE_COUNT 0
int src_test_main()
{
	INT16 FrameSize;
	INT32 block_count ;
	INT32 InputSamplingRate = 16000, OutputSamplingRate = 48000 ;
        UINT8 *srcPtr;
        INT32 numBlocks;
        INT32 offset;

#if CYCLE_COUNT                
        //For cycle measurement.
        volatile unsigned int start, end;
        volatile unsigned int delay, totalCycles = 0;
        volatile unsigned int totalDelay = 0;
#endif        
	
	printf ("input samplerate = %d, output samplerate = %d\n", InputSamplingRate, OutputSamplingRate) ;

	//SRC_Init (SRCMemory, WorkBuffer, InputSamplingRate, OutputSamplingRate);
        init_interpolate_by_3();

	block_count = 0;

	FrameSize = INPUT_BLK_SIZE; //240 samples = 15 ms

	printf ("Init Done\n") ;

        //InputArray "b1" is in bytes so no of frames/blocks in input array as computed below
        numBlocks = sizeof(b1);
        numBlocks = numBlocks / (FrameSize * 2);
	//while(fread(input, sizeof(INT16), FrameSize, f_inp) == (INT16)FrameSize)
        for (block_count = 0; block_count < numBlocks; block_count++)
	{
                //printf("======No of blocks processed======: %d\n",block_count);
                offset = block_count * (FrameSize * 2);
                srcPtr = b1 + offset;
                
                PrepareInput((UINT8 *)input, (UINT8 *)srcPtr, (FrameSize * 2));
#if CYCLE_COUNT                
                start = DWT->CYCCNT;   
#endif                

		//N = SRC_Process (SRCMemory, input, src_txoutput, FrameSize);
                //convert_16_to_48(test_input_buffer, (&gDmaBuffer.mem[0][0] + i));
                convert_16_to_48(input, src_txoutput);  

#if CYCLE_COUNT                 
                end = DWT->CYCCNT;
                
                delay = end - start;
                totalCycles += delay;
                printf("\n clockfreq = %d",HAL_RCC_GetSysClockFreq());
                printf("\nCycles in SRC excutation = %d\n", delay);
                delay = ((delay * 1.0) / HAL_RCC_GetSysClockFreq()) * 1000;
                printf("delay in SRC excutation = %d\n", delay);
#endif
                //N = SRC_Process (SRCMemory, (INT16 *)input, src_txoutput, FrameSize);

		//fwrite(src_txoutput,sizeof(INT16), N, f_txout);
                //printOutputToTeraTerm((UINT8 *)input, (FrameSize * 2));
                printOutputToTeraTerm((UINT8 *)src_txoutput, (FrameSize * 2 * 3));
	}
	printf("\n===Total no of blocks processed===: %d\n",block_count);
#if CYCLE_COUNT 
        totalDelay = ((totalCycles * 1.0) / HAL_RCC_GetSysClockFreq()) * 1000;
        printf("\nTotal Cycles in SRC excutation = %d\n", totalCycles);
        printf("\nTotal delay in SRC excutation = %d\n", totalDelay);
#endif
        
	return 0 ;
}
