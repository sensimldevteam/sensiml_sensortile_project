#include "Fw_global_config.h"
#include "cli.h"
#include "dbg_uart.h"
#include "ymodem.h"
#include "ql_fs.h"
#include "stdio.h"
#include "stdint.h"
#include "assert.h"
#include "string.h"
#include "stdlib.h"
#include "ql_time.h"

struct ymodem ymodem_vars;
#define FN_MAX_LEN 32
static char rx_filename_buf[FN_MAX_LEN+1];

#define YMODEM_MAX_TRACE 100 // 100 is a good number for debug
#if YMODEM_MAX_TRACE > 0 
struct {
    uint32_t t;
    const char *s; uint32_t v;
} trace[YMODEM_MAX_TRACE];
static int trace_depth;
#endif

static void my_tx_byte( struct ymodem *pY, int b )
{
    (void)(pY);
    CLI_putc_raw( b );
}

static int my_rx_byte( struct ymodem *pY, int timeout )
{
    return CLI_getkey_raw( timeout );
}

static void my_log( struct ymodem *pY, const char *msg, uint32_t v )
{
    (void)(pY);
    (void)(msg);
    (void)(v);
#if YMODEM_MAX_TRACE > 0
    if( trace_depth < YMODEM_MAX_TRACE ){
        trace[trace_depth].t = (uint32_t)ql_lw_timer_start();
        trace[trace_depth].s = msg;
        trace[trace_depth].v = v;
        trace_depth++;
    }
#endif
}

static int my_fwrite( struct ymodem *pY )
{
	FF_FILE *fp;
	int tmp;
	if( pY->block_number == 0 ){
		tmp = strlen(pY->filename);
		if( tmp >= FN_MAX_LEN ){
			return -1;
		}
		strcpy( rx_filename_buf, pY->filename );
		fp = QLFS_fopen( QLFS_DEFAULT_FILESYTEM, rx_filename_buf, "wb" );
		if( fp ){
            pY->cookie = (intptr_t)fp;
            return 0;
        } else {
            my_log(pY,"open-failed",-1);
            return -1;
        }
    }
    
	fp = (FF_FILE *)(pY->cookie);
	if( fp == NULL ){
        my_log(pY,"no-open-file",-1);
		return -1;
	}
	if( pY->block_size == 0 ){
		/* close the file */
		QLFS_fclose( QLFS_DEFAULT_FILESYTEM, fp );
		return 0;
	}
	
    tmp = QLFS_fwrite( QLFS_DEFAULT_FILESYTEM, 
                   (FF_FILE *)(pY->cookie), 
                   pY->buffer,
                   1,
                   pY->block_size );
	if( tmp != pY->block_size ){
        my_log(pY,"write-failed",tmp);
		return -1;
	} else {
		return 0;
	}
}

static int my_fread( struct ymodem *pY )
{
    int r;
    
    if( pY->block_size == 0 ){
        return 0;
    }
    r = QLFS_fread( QLFS_DEFAULT_FILESYTEM, 
                   (FF_FILE *)(pY->cookie), 
                   pY->buffer,
                   1,
                   pY->block_size );
    /* success? */
    if( r == pY->block_size ){
        return 0;
    }
    if( r < 0 ){
        /* error */
        return -1;
    }
    /* parital */
    memset( pY->buffer+r, 0x1a, pY->block_size-r );
    return 0;
}

static void my_ymodem_setup(void)
{
    ymodem_init( &ymodem_vars );
    ymodem_vars.pFlog = my_log;
    ymodem_vars.pFwrite = my_fwrite;
    ymodem_vars.pFread  = my_fread;
    ymodem_vars.pRxByte = my_rx_byte;
    ymodem_vars.pTxByte = my_tx_byte;
}

static void dump_trace(void)
{
    #if YMODEM_MAX_TRACE > 0
    int x;
    for( x = 0 ; x < trace_depth ; x++ ){
        CLI_printf("%4d.%03d | %s: 0x%08x | %d\n",
            trace[x].t/1000,
            trace[x].t%1000,
            trace[x].s,
            trace[x].v,
            trace[x].v);
    }
    trace_depth = 0;
#endif
}

void CLI_ymodem_rx_command(const struct cli_cmd_entry  *pEntry)
{
    int tmp;
	my_ymodem_setup();
	
	CLI_printf("\n\nStart the transfer, ^X, ^X, ^X cancels\n");
	CLI_printf("The C's you see is the request to start the transfer...\n");
	tmp = ymodem_receive( &ymodem_vars );
    /* let teraterm gui go away */
	CLI_sleep(500);
	CLI_printf("Transfer complete result: %d\n", tmp );
    dump_trace();
}

void CLI_ymodem_tx_command(const struct cli_cmd_entry  *pEntry)
{
    int r;
    char *filename;
    FF_FILE *fp;

	my_ymodem_setup();
    
    CLI_string_ptr_required("filename", &filename );
    ymodem_vars.filename = filename;
    
    fp = QLFS_fopen( QLFS_DEFAULT_FILESYTEM, filename, "rb" );
    ymodem_vars.cookie = (intptr_t)fp;
    
    if( ymodem_vars.cookie == 0){
        CLI_error("no such file: %s\n", filename );
    }
    
    ymodem_vars.file_size_bytes = fp->ulFileSize;
    CLI_printf("\n");
    CLI_printf("YMODEM Send: %s\n",filename);
    CLI_printf("       Size: %d, %dK, %dM\n",
                       ymodem_vars.file_size_bytes,
                       ymodem_vars.file_size_bytes/1024,
                       ymodem_vars.file_size_bytes/(1024*1024));
    CLI_printf("Type: ^X 3 times to cancel\n");
    CLI_printf("Start transfer now...\n");

    /* assume 115.2K baud */
    r = ymodem_vars.file_size_bytes / 11520;
    /* assume 80% throughput */
    r = r * 2;
    CLI_printf("At 115200 baud expected transfer: %d seconds\n", r );
    r = ymodem_send(&ymodem_vars);
    QLFS_fclose( QLFS_DEFAULT_FILESYTEM, (FF_FILE *)(ymodem_vars.cookie));
    ymodem_vars.cookie = 0;
    
    /* let tera term window go away */
    CLI_sleep(500);
    dump_trace();
    
    if( r == 0 ){
        CLI_printf("Success\n");
    } else {
        CLI_error("Transfer failed\n");
    }
}
