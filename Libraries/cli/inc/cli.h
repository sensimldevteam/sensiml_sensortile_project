#if !defined(CLI_H)
#define CLI_H

#if !defined( _EnD_Of_Fw_global_config_h )
#error "Include Fw_global_config.h first"
#endif

#include "stdio.h"
#include "setjmp.h"
#include "stdint.h"
#include "stdarg.h"

#define _IN_CLI_H_

#include "cli/cli_structs.h"
#include "cli/cli_getkey.h"
#include "cli/cli_hw.h"
#include "cli/cli_misc.h"
#include "cli/cli_time.h"
#include "cli/cli_args.h"
#include "cli/cli_print.h"
#include "cli/cli_filesystem.h"

#undef _IN_CLI_H_

#define CLI_TASK_STACKSIZE  256
#define CLI_TASK_PRIORITY   3

/*
 * this is the CLI commands to simulate the commands.
 */

extern uint8_t raw_bytes[128];

/* clear the command buffer */
extern void cli_cmd_buff_reset(void);

/* Add an 8bit value to the COMMAND */
extern void cli_cmd_buff_wr_u8( int v );

/* Add an 16bit value to the COMMAND */
extern void cli_cmd_buff_wr_u16( int v );
  
/* Add an 32bit value to the COMMAND */
extern void cli_cmd_buff_wr_u32( uint32_t v );

/* Add an 64bit value to the COMMAND */
extern void cli_cmd_buff_wr_u64( uint64_t v );

/**
* @brief Create a CLI task, to process commands, use provided main menu
*/
void CLI_start_task(const struct cli_cmd_entry *pMainMenu);

/**
* @brief Handle keys as they are typed.
*
* This is called by the CLI_task to process keys. see CLI_start_task() for detals.
*/
void CLI_rx_byte(int keypressed);

/**
* @brief Dispatch the next command in the "path" list.
*
* Assumptions:
*    There are more parameters to be processed.
*    And the "top" of the cmd path stack has a pointer to a list of commands.
*/
void CLI_dispatch_command( void );

/* a few standard menu items */
extern const struct cli_cmd_entry cli_file_menu[];
extern const struct cli_cmd_entry cli_std_menu[];

void CLI_ymodem_tx_command(const struct cli_cmd_entry  *pEntry);
void CLI_ymodem_rx_command(const struct cli_cmd_entry  *pEntry);
#endif

