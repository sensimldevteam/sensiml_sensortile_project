/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : spi_flash.c
 *    Purpose: This file contains driver source code for Macronix MX25U3235FM2I SPI flash.
 *                                                          
 *=========================================================*/

#ifndef MEDIA_DRV_DEFINES_H
#define MEDIA_DRV_DEFINES_H

#define HUNDRED_64_BIT				      100ULL
#define BYTES_PER_MB				     (1024ull * 1024ull)
#define SECTORS_PER_MB				     (BYTES_PER_MB / SPIFLASH_DISK_SECTOR_SIZE)

typedef struct
{
	uint32_t total_size;
	uint32_t free_size;
}SPI_DISK_PRIVATE_DATA;

#endif  // MEDIA_DRV_SPI_SD_H
