/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : iop_messages.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __IOP_MESSAGES_H__
#define __IOP_MESSAGES_H__

#include <stdint.h>
#include "ble_collection_defs.h"
#include "ble_pme_defs.h"

#ifdef BLE_STACK_SUPPORT_REQD
#include "app_timer.h"
#endif

typedef enum{
    /* bidirectional packet type */
    IOP_MSG_GET_VERSION       = 0, 
    IOP_MSG_GET_COMPDATETIME  = 1,
    
    /* host asks device for model GUID */
    IOP_MSG_GET_MODEL_GUID   = 2,
    IOP_MSG_GET_STATUS       = 3,
    /* byte 0 is msg, byte 1 is the cmd the reply was for */
    IOP_MSG_MISC_REPLY = 4, /* device -> host */
    
    /* clear error status to zero */
    IOP_MSG_CLR_STATUS       = 5,
    
    /* 4 .. 10 unused */
    
    /* sending IMU data to the host */
    IOP_MSG_IMU_DATA_START      = 10, /* host ->device */
    IOP_MSG_IMU_DATA_STOP       = 11, /* host ->device */
    /* msg type for imu data to host */
    IOP_MSG_IMU_DATA            = 12, /* device -> host */
    
    /* select any sensor */
    IOP_MSG_SENSOR_SELECT_BY_ID = 20, /* host -> device, with sensor id, and channel */
    /* turn sensor on and off */
    IOP_MSG_SENSOR_START        = 21, /* host -> device */
    IOP_MSG_SENSOR_STOP         = 22, /* host -> device */
    /* msg type for selected sensor to host */
    IOP_MSG_SENSOR_DATA         = 23, /* device->host */
    /* 22 .. 29 unused */
    
	IOP_MSG_STORAGE_FILENAME = 30, /* host->device */
	IOP_MSG_STORAGE_CONFIG   = 31, /* host->device */
    IOP_MSG_STORAGE_START    = 32,
    IOP_MSG_STORAGE_STOP     = 33,
    
    IOP_MSG_RECO_CLASSIFICATION_START = 40,
    IOP_MSG_RECO_CLASSIFICATION_STOP = 41,
    IOP_MSG_RECO_CLASSIFICATION_DATA = 42, /* device -> host */
    
	IOP_MSG_RECO_FEATURES_START = 43,
	IOP_MSG_RECO_FEATURES_STOP = 44,
    IOP_MSG_RECO_FEATURE_DATA = 45, /* device -> host */
    
	IOP_MSG_RECO_SEGMENT_START = 46,
	IOP_MSG_RECO_SEGMENT_STOP = 47,
    IOP_MSG_RECO_SEGMENT_DATA = 48, /* device -> host */
    /* 38 39 not used */
    
    /* see dcl_commands */
    IOP_MSG_SENSOR_CONFIG = 50,
    IOP_MSG_SENSOR_REPLY  = 51, /* device -> host */

}iop_msg_cmd_t;

struct iop_raw_spi_message {
    uint8_t len;
    uint8_t cmd; /* one of iop_msg_cmd_t */
    uint8_t payload[126];
};

/*
* IMU data is in this form
*/
struct iop_xyz_data {
    uint16_t x, y, z;
};

struct iop_imu_data {
    /* this dummy value is here for alignment
    * and is special cased & removed later */
    uint8_t dummy_value;
    uint8_t n_values;
    struct iop_xyz_data motion[ 12 ];
};

struct iop_sensor_config {
    uint8_t sensor_cmd;
    uint8_t payload[19];
};


struct iop_status {
    /* number of bytes written to usb/sdcard */
    uint32_t bytes_stored;
    
    /* bit values for status bit s*/
#define IOP_STATUS_BIT_storage 0x0001 /* device is storing data to filesystem */
#define IOP_STATUS_BIT_imu     0x0002 /* sending IMU data to the BLE */
#define IOP_STATUS_BIT_sensor  0x0004 /* sending SELECTED sensor to BLE */
#define IOP_STATUS_BIT_reco    0x0008 /* recognition is enabled */
#define IOP_STATUS_BIT_reco_f  0x0010 /* recongition with features are enabled */
#define IOP_STATUS_BIT_reco_s  0x0020 /* recongition with segments are enabled */
#define IOP_STATUS_BIT_ble_oe  0x0080 /* BLE cannot keep up with the data, overrun error */
#define IOP_STATUS_BIT_sd_oe   0x0100 /* SD card cannot keep up, overr runrun error occured */
#define IOP_STATUS_BIT_any_err 0x0200 /* set if ANY error occured */
    uint32_t status_bits;
    
    /* packet counters, always increasing - DCL can use this to monitor health of BLE connection */
    uint8_t ble_rx_packets;
    uint8_t ble_tx_packets;
    
    /* overrun counters, see the "oe" status bits above */
    uint8_t ble_overrun_count;
    uint8_t sdcard_overrun_count;
    
    /* sticky error number, only saves first, until an ERROR clear occurs */
    uint8_t error_num;
    
    /* monotonic increasing error counter,
     * DCL can use this to determine if errors occured
     * and if more then 1 error occured */
    uint8_t error_counter;
};

/*
 * Some replies are simple, we use this structure for generic replies.
 * Example:  VERSIONSTRING reply uses this, as do a few others.
 */
struct iop_misc_reply {
    /* the command replying to */
    uint8_t cmd;
    union {
        /* data is a string or bytes */
        char    cbuf[100];
        uint8_t as_u8[100];
    } u;
};



enum iop_error_codes {
    
    /* all is well */
    IOP_ERR_OK                  = 0,
    
    /* an unknown command was sent */
    IOP_ERR_UNKNOWN_MAJOR_CMD       = 1,
    IOP_ERR_UNKNOWN_MINOR_CMD       = 2,
    
    /* no such sensor */
    IOP_ERR_UNKNOWN_SENSOR      = 3,
    
    /* sample rate is not supported */
    IOP_ERR_ILLEGAL_RATE        = 4,
    
    /* a parameter to this command/sensor is not supported */
    IOP_ERR_INVALID_PARAMETER   = 5,
    
    /* Combination of sensors, rates, and modes not supported */
    IOP_ERR_INVALID_CONFIG      = 6,
    
    /* something is wrong with the file system storage */
    IOP_ERR_FILESYSTEM          = 7,
    
};


/*
 * This is the task variables used by the IOP (ble command processing)
 */
#define IOP_MAX_BYTES 128 
struct iop_globals {
    struct iop_raw_spi_message  cmd_from_host;
    struct iop_raw_spi_message  rsp_to_host;
    struct iop_status           cur_status;
    
    int data_busy;  
    union iop_data_msg {
        uint8_t as_u8[IOP_MAX_BYTES];
        char    as_string[IOP_MAX_BYTES];
        uint32_t as_u32[ IOP_MAX_BYTES / 4 ];
        uint16_t as_u16[ IOP_MAX_BYTES / 2 ]; 
        struct iop_imu_data   imu_data;
	ble_pme_result_t pme_results;
	ble_pme_result_w_fv_t pme_fv_results;
    } u_data;
    

    union iop_aligned_msg {
        uint8_t as_u8[IOP_MAX_BYTES];
        char    as_string[IOP_MAX_BYTES];
        uint32_t as_u32[ IOP_MAX_BYTES / 4 ];
        uint16_t as_u16[ IOP_MAX_BYTES / 2 ]; 
        struct iop_misc_reply misc;
    } u_cmd, u_rsp;
};

#if defined(IOP_MESSAGE_C)
#define IOP_EXTERN
#else
#define IOP_EXTERN extern
#endif
IOP_EXTERN struct iop_globals iop_globals;

/**
* @brief set this status error code.
*/
void iop_set_error( int error_code, uint32_t more_info );

void iop_send_motion_data(int num, const struct iop_xyz_data * data);

void iop_send_pme_results(ble_pme_result_t * results);

void iop_send_feature_vector_results(ble_pme_result_w_fv_t * results);

void iop_msg_parse(struct iop_raw_spi_message *pRawMsg);

/* return 0 on success */
int app_datastorage_start_stop( int is_start, const uint8_t *pStartGuid, size_t guidlen );
void app_datastorage_set_filename( const char *filename );
void app_datastorage_set_flags( uint32_t flags );

void app_sensor_config(void);

void iop_process_command( void );
/**
* @brief concatinate a series of strings as a string reply
*
* usage:  iop_reply_string( "your", "name", "here", NULL );
*
* Results in: "your name here" 
*/
void iop_reply_string( const char *cp, ... );


#endif //__IOP_MESSAGES_H__
