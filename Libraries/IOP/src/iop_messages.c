
/*==========================================================
*                                                          
*-  Copyright Notice  -------------------------------------
*                                                          
*    Licensed Materials - Property of QuickLogic Corp.     
*    Copyright (C) 2019 QuickLogic Corporation             
*    All rights reserved                                   
*    Use, duplication, or disclosure restricted            
*                                                          
*    File   : iop_messages.c
*    Purpose: 
*                                                          
*=========================================================*/
#define IOP_MESSAGE_C 1
#include "Fw_global_config.h"
#include "iop_messages.h"
#include "ble_collection_defs.h"
#include "dcl_commands.h"
#include "ql_bleTask.h"
#include "Recognition.h"
#include "DataCollection.h"
#include "RtosTask.h"
#include "task.h"
#include <string.h>
#include <stdio.h>
#include "kb.h"
#include "dbg_uart.h"
#include "Recognition.h"
#include "sml_output.h"

int ble_motion_drop;

/* send this packet to the BLE */
static void my_ble_send( int cmd, int len, const void *data )
{
    uint8_t databuf[1];
    databuf[0] = cmd;
    SendToBLE( SEND_BLE_LARGE_BUFFER, 1, &databuf[0] );
    SendToBLE( SEND_BLE_IMMEDIATE, len, data );
}

//iop_connection_t * connection_data;


/* reply to host as a series of strings */
void iop_reply_string( const char *cp, ... )
{
    va_list ap;
    
    char *d;
    int n;
    int c;
    
    va_start(ap,cp);
    n = 0;
    d = iop_globals.u_rsp.misc.u.cbuf;
    
    while( (n < sizeof(iop_globals.u_rsp.misc.u.cbuf)) && (cp != NULL) ){
        c = *cp++;
        if( c == 0 ){
            c = ' ';
            cp = va_arg( ap, const char * );
        }
        *d++ = c;
        n++;
    }
}

/* handle MODEL GUID command (FUTURE) */
static void do_get_model_guid(void)
{
    const uint8_t *pGuid;
#if S3AI_FIRMWARE_IS_RECOGNITION
    /* we only send the first GUID */
    pGuid = sml_get_model_uuid_ptr(0);

#else
    /* all zeros is no-guid */
    static const uint8_t dummy_guid[16] = { 0 };
    pGuid = dummy_guid;
#endif
    memcpy( (void *)(&iop_globals.u_rsp.misc.u.as_u8[0]), pGuid, 16 );
    
    iop_globals.u_rsp.misc.cmd = iop_globals.cmd_from_host.cmd;
    iop_globals.rsp_to_host.cmd = IOP_MSG_MISC_REPLY;
    iop_globals.rsp_to_host.len = 20;
    memcpy( (void *)(&iop_globals.rsp_to_host.payload[0]),
           &(iop_globals.u_rsp.misc),
           20);
}

/* handle COMPILER date/time stamp command */
static void do_get_compdatetime( void )
{
    iop_reply_string( __DATE__, __TIME__ , NULL );
    iop_globals.u_rsp.misc.cmd = iop_globals.cmd_from_host.cmd;
    iop_globals.rsp_to_host.cmd = IOP_MSG_MISC_REPLY;
    iop_globals.rsp_to_host.len = 20;
    memcpy( (void *)(&iop_globals.rsp_to_host.payload[0]),
           &(iop_globals.u_rsp.misc),
           20);
}

/* handle get sw version command */
static void do_get_version( void )
{
    extern const char *SOFTWARE_VERSION_STR;
    
    iop_reply_string( SOFTWARE_VERSION_STR, NULL );
    iop_globals.u_rsp.misc.cmd = iop_globals.cmd_from_host.cmd;
    iop_globals.rsp_to_host.cmd = IOP_MSG_MISC_REPLY;
    iop_globals.rsp_to_host.len = 20;
    memcpy( (void *)(&iop_globals.rsp_to_host.payload[0]),
           &(iop_globals.u_rsp.misc),
           20);
    
}

/* an error has occured .. update error bits */
void iop_set_error( int errcode, uint32_t more_info )
{
    (void)(more_info);
    dbg_str_int("iop-error", errcode );
    if( 0 == iop_globals.cur_status.error_num ){
        iop_globals.cur_status.error_num  = errcode;
        
    }
    iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_any_err;
    iop_globals.cur_status.error_counter++;
}

/* start sending data - track overrun if needed */
static int send_data_start(void)
{
    if( iop_globals.data_busy ){
        iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_ble_oe;
        iop_globals.cur_status.ble_overrun_count += 1;
        return -1;
    }
    
    iop_globals.data_busy  = 1;
    memset( (void *)(&iop_globals.u_data), 0, sizeof( iop_globals.u_data ) );
    
    return 0;
}

static void send_data_end(void)
{
    iop_globals.data_busy  = 0;
}


void iop_send_motion_data(int num, const struct iop_xyz_data *pXYZ)
{
    if( send_data_start() < 0 ){
        ble_motion_drop += 1;
        return;
    }
    
    if( num > 6 ){
        num = 6;
        iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_ble_oe;
        iop_globals.cur_status.ble_overrun_count += 1;
    }
    
    my_ble_send( IOP_MSG_IMU_DATA,
                num * sizeof( struct iop_xyz_data ), 
                (void *)(pXYZ) );
    
    send_data_end();
}

void iop_send_pme_results(ble_pme_result_t * results)
{
#if 0
    if( !(iop_globals.cur_status.status_bits & IOP_STATUS_BIT_reco) ){
        return;
    }
#endif
    if( send_data_start() < 0 ){
        return ;
    }
    
    my_ble_send( IOP_MSG_RECO_CLASSIFICATION_DATA,
                sizeof(ble_pme_result_t),
                (void *)(results) );
    send_data_end();
}

void iop_send_feature_vector_results(ble_pme_result_w_fv_t * results)
{
    size_t sz;
#if 0
    if( !(iop_globals.cur_status.status_bits & IOP_STATUS_BIT_reco_f) ){
        return;
    }
#endif
    if( send_data_start() < 0 ){
        return;
    }
     sz = sizeof(ble_pme_result_w_fv_t);
    if( sz > 20 ){
        dbg_str_int("ble-feature-vector-truncated", sz );
        sz = 20;
    }
    
    my_ble_send( IOP_MSG_RECO_FEATURE_DATA,
                sz,
                (void *)results);
    send_data_end();
}

static void do_imu_start(void)
{
    iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_imu;
}

static void do_imu_stop(void)
{
    iop_globals.cur_status.status_bits &= ~IOP_STATUS_BIT_imu;
}

static void do_sensor_start(void)
{
    iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_sensor;
    /* TODO calculate the sensor divisor (reload counter value) 
     * Once the DCL selects a sensor 
     * We'll need to write the code to calculate the rate.
     */
}

static void do_sensor_stop(void)
{
    iop_globals.cur_status.status_bits &= ~IOP_STATUS_BIT_sensor;
}

#if S3AI_FIRMWARE_IS_RECOGNITION
static void do_reco_seg_stop(void)
{
    iop_globals.cur_status.status_bits &= ~IOP_STATUS_BIT_reco_s;
    recognition_startstop( RECOG_CMD_RECOGNIZE_STOP );
}
#endif

#if S3AI_FIRMWARE_IS_RECOGNITION
static void do_reco_seg_start(void)
{
    iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_reco_s;
    recognition_startstop( RECOG_CMD_RECOGNIZE_START_W_S );
}
#endif

#if S3AI_FIRMWARE_IS_RECOGNITION
static void do_reco_f_start(void)
{
    iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_reco_f;
    recognition_startstop(RECOG_CMD_RECOGNIZE_START_W_FV );
}
#endif

#if S3AI_FIRMWARE_IS_RECOGNITION
static void do_reco_f_stop(void)
{
    iop_globals.cur_status.status_bits &= ~IOP_STATUS_BIT_reco_f;
    recognition_startstop( RECOG_CMD_RECOGNIZE_STOP );
}
#endif

#if S3AI_FIRMWARE_IS_RECOGNITION
static void do_reco_start(void)
{
    iop_globals.cur_status.status_bits |= IOP_STATUS_BIT_reco;
    recognition_startstop( RECOG_CMD_RECOGNIZE_START );
}
#endif

#if S3AI_FIRMWARE_IS_RECOGNITION
static void do_reco_stop(void)
{
    iop_globals.cur_status.status_bits &= ~IOP_STATUS_BIT_reco;
    recognition_startstop( RECOG_CMD_RECOGNIZE_STOP );
}
#endif

#if S3AI_FIRMWARE_IS_COLLECTION
static void do_storage_stop(void)
{
    app_datastorage_start_stop(0, NULL, 0 );
}

static void do_storage_start(void)
{
    /* guids are 16 bytes */
    app_datastorage_start_stop(1, iop_globals.u_cmd.as_u8, 16);
}

#endif

static void do_sensor_id(void)
{
    switch( iop_globals.u_cmd.as_u32[0] ){
    default:
        /* unknown sensor */
        dbg_str_hex32("err-unknown-sensor", iop_globals.u_cmd.as_u32[0] );
        iop_set_error( IOP_ERR_INVALID_PARAMETER, iop_globals.u_cmd.as_u32[0] );
        break;
    case SENSOR_ENG_VALUE_ACCEL:
    case SENSOR_ENG_VALUE_GYRO:
    case SENSOR_ENG_VALUE_MAGNETOMETER:
    case SENSOR_AUDIO:
    case SENSOR_ADC_LTC_1859_MAYHEW:
        ble_data_config.sensor_data_id_major = iop_globals.u_cmd.as_u32[0];
        ble_data_config.sensor_data_id_minor = iop_globals.u_cmd.as_u32[0];
        break;
    }
}

static void do_clr_status(void)
{
    memset( &(iop_globals.cur_status), 0, sizeof(iop_globals.cur_status) );
}

static void do_get_status(void)
{
    iop_globals.rsp_to_host.cmd = IOP_MSG_MISC_REPLY;
    iop_globals.rsp_to_host.payload[0] = IOP_MSG_GET_STATUS;
    
    memcpy( (void *)(&iop_globals.rsp_to_host.payload[1]),
           &(iop_globals.cur_status),
           sizeof(iop_globals.cur_status));
    iop_globals.rsp_to_host.len = 1 + sizeof(iop_globals.cur_status);
}

#if S3AI_FIRMWARE_IS_COLLECTION
static void do_storage_filename( void )
{
    app_datastorage_set_filename( iop_globals.u_cmd.as_string );
}
#endif

#if S3AI_FIRMWARE_IS_COLLECTION
static void do_storage_config(void)
{
    app_datastorage_set_flags( iop_globals.u_cmd.as_u32[0] );
}
#endif

struct iop_dispatch {
    int cmd;
    void (*handler)(void);
};

/* ble command dispatch table */
static const struct iop_dispatch all_iop_cmds[] = {
    { .cmd = IOP_MSG_GET_VERSION, do_get_version },
    { .cmd = IOP_MSG_GET_COMPDATETIME, do_get_compdatetime },
    { .cmd = IOP_MSG_GET_MODEL_GUID, do_get_model_guid },
    { .cmd = IOP_MSG_GET_STATUS, do_get_status },
    { .cmd = IOP_MSG_CLR_STATUS, do_clr_status },
    { .cmd = IOP_MSG_IMU_DATA_STOP, do_imu_stop },
    { .cmd = IOP_MSG_IMU_DATA_START, do_imu_start },
    { .cmd = IOP_MSG_SENSOR_SELECT_BY_ID, do_sensor_id },
    { .cmd = IOP_MSG_SENSOR_START, do_sensor_start },
    { .cmd = IOP_MSG_SENSOR_STOP, do_sensor_stop },
#if S3AI_FIRMWARE_IS_COLLECTION
    { .cmd = IOP_MSG_STORAGE_FILENAME, do_storage_filename },
    { .cmd = IOP_MSG_STORAGE_CONFIG, do_storage_config },
    { .cmd = IOP_MSG_STORAGE_START, do_storage_start },
    { .cmd = IOP_MSG_STORAGE_STOP, do_storage_stop },
    { .cmd = IOP_MSG_SENSOR_CONFIG, .handler = app_sensor_config },
#endif
#if S3AI_FIRMWARE_IS_RECOGNITION
    
    { .cmd = IOP_MSG_RECO_CLASSIFICATION_START, do_reco_start },
    { .cmd = IOP_MSG_RECO_CLASSIFICATION_STOP, do_reco_stop },
    
    { .cmd = IOP_MSG_RECO_FEATURES_START, .handler = do_reco_f_start },
    { .cmd = IOP_MSG_RECO_FEATURES_STOP, .handler = do_reco_f_stop },
    
    
    { .cmd = IOP_MSG_RECO_SEGMENT_START, .handler = do_reco_seg_start },
    { .cmd = IOP_MSG_RECO_SEGMENT_STOP, .handler = do_reco_seg_stop },
#endif
    
    { .handler = NULL }
};

/* process a command from the CMLINE or from the BLE */
void iop_process_command( void )
{
    const struct iop_dispatch *pCmd;
    
    iop_globals.cur_status.ble_rx_packets += 1;
    
    if( DBG_flags & DBG_FLAG_ble_cmd ){
        dbg_str("iop-cmd\n");
        dbg_memdump8( 0, (void *)(&iop_globals.cmd_from_host), 64 );
    }
    
    memset( (void *)(&iop_globals.rsp_to_host), 0, sizeof(iop_globals.rsp_to_host) );
    
    memcpy( (void *)(&iop_globals.u_cmd), 
           (void *)(&iop_globals.cmd_from_host.payload[0]), 
           20 );
    
    pCmd = &all_iop_cmds[0];
    while(pCmd->handler){
        if( pCmd->cmd != iop_globals.cmd_from_host.cmd ){
            pCmd++;
            continue;
        } else {
            break;
        }
    }
    
    if( pCmd->handler ){
        (*(pCmd->handler))();
    } else {
        dbg_str("err-unknown-cmd\n");
        iop_set_error( IOP_ERR_UNKNOWN_MAJOR_CMD,iop_globals.cmd_from_host.cmd  );
    }
    if( DBG_flags & DBG_FLAG_ble_cmd ){
        dbg_putc('\n');
        if( iop_globals.rsp_to_host.len == 0 ){
            dbg_str("iop-no-rsp\n");
        } else {
            dbg_str("iop-cmd: response\n");
            dbg_memdump8( 0, &(iop_globals.rsp_to_host), 64 );
        }
    }
    
    if( iop_globals.rsp_to_host.len ){
        my_ble_send( iop_globals.rsp_to_host.cmd,
                    iop_globals.rsp_to_host.len,
                    (void *)(iop_globals.rsp_to_host.payload) );
    }
}

void iop_process_ble_commands( void )
{
    const struct iop_dispatch *pCmd;

    iop_globals.cur_status.ble_rx_packets += 1;


    memset( (void *)(&iop_globals.rsp_to_host), 0, sizeof(iop_globals.rsp_to_host) );

    memcpy( (void *)(&iop_globals.u_cmd),
           (void *)(&iop_globals.cmd_from_host.payload[0]),
           20 );

    pCmd = &all_iop_cmds[0];
    while(pCmd->handler){
        if( pCmd->cmd != iop_globals.cmd_from_host.cmd ){
            pCmd++;
            continue;
        } else {
            break;
        }
    }

    if( pCmd->handler ){
        (*(pCmd->handler))();

    }
}
