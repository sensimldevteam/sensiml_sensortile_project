/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : comm_command.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __COMM_COMMAND_H
#define __COMM_COMMAND_H

//#include "alg_types.h"
#include "string.h"
#include "eoss3_hal_spi.h"
#include "debug.h"
//#include "uart_txrx.h"
#include "eoss3_dev.h"
#include "eoss3_hal_timer.h"
//#include "m4_loader.h"
#include "Fw_global_config.h"

#define REBOOT_CAUSE_FLASHING           (0x2)
#define REBOOT_CAUSE                    (0xF)
#define REBOOT_STATUS_REG               (PMU->MISC_POR_3)


/* Communication commands with PC Tool */
/* string length for all commands received by S3 is now fixed (10 characters) */
/* String Length of commands*/
#define CMD_STRING_LENGTH               (11)                    

/* Initial Command*/
#define CMD_COMM_START                  "COMM_START\n"

/* BootMode commands */
#define CMD_BOOTMODE_UARTDOWNLOAD       "FLASH_MODE\n"
//#define CMD_BOOTMODE_COMBINEDIMG        "CMBND_MODE"
#if defined(RAMDUMP_SUPPORT)
#define CMD_BOOTMODE_RAMDUMP            "RDUMP_MODE\n"
#endif

#define CMD_VERIFY_IMAGE    			"VRFY_IMAGE\n"
#define CMD_NEXT_IMAGE                  "NEXT_IMAGE\n"
#define CMD_DEVICE_REBOOT               "DEV_REBOOT\n"
#if defined(RAMDUMP_SUPPORT)
#define RAMDUMP_FINISH_ACK              "RD_FIN_ACK"
#endif

/* Response from S3 to PC Tool */
#ifdef BL0
#define RESP_COMM_START                 "COMM_START_BL0_ACK"
#else
#define RESP_COMM_START                 "COMM_START_BL1_ACK"
#endif
#define RESP_BOOTMODE_UARTDOWNLOAD      "FLASH_MODE_ACK"
#define RESP_DEVICE_REBOOT              "DEV_REBOOT_ACK"
#define FLASH_STATUS_OK                 "FLASH_OK"
#define FLASH_STATUS_FAIL               "FLASH_FAIL"
#define YMOD_STATUS_FAIL                "YMOD_FAIL"
#if defined(RAMDUMP_SUPPORT)
#define RAMDUMP_FILE_SENT               "FILE_SENT"
#define RAMDUMP_FINISH                  "RDUMP_FIN"
#endif

/* Enclose msg to be displayed on Flah Tool between "$#$"*/
#define DISPLAY_MSG_TO_TOOL() do{ printf("$#$");}while(0);

#define MAX_RETRIES                     (3)
#define MAX_ERRORS_IN_MAIN              (3)


#define INFINITE_RETRIES                0xFFFFFFFF

enum BL1Task_state {UPDATE_MODE_UART_DWNLD = 1,UPDATE_MODE_RDUMP, UPDATE_MODE_FS, NORMAL_MODE};
enum cmdstate {RX_NO_COMMAND = 1, RX_INITIAL_COMMAND, RX_BOOTMODE_CMD};

void reboot_if_end_of_transfer(unsigned int SecDelayTime);
unsigned int poll_for_userinput(unsigned int SecDelayTime, enum cmdstate state, int retry_infinitely);
unsigned int findbootmode(void);

#endif //__COMM_COMMAND_H
