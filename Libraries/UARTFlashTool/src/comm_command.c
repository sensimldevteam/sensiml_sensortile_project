/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : comm_command.c
 *    Purpose: 
 *                                                          
 *=========================================================*/

#include "Fw_global_config.h"
#include "comm_command.h"
#if defined(RAMDUMP_SUPPORT)
#include "display_dumpimage.h"
#endif
#include "eoss3_hal_uart.h"
#include "dbg_uart.h"
//#include "BL1_Task.h"

extern unsigned char g_currentImgname[20];
extern unsigned int g_CurrentImgSize;
extern unsigned int g_Currentflashoffset;

#if defined(RAMDUMP_SUPPORT)

/*!
 * \fn		static inline unsigned int is_reboot_after_ramdump(void)
 * \brief	This function will check whether device booted because of ramsump .
 * \return      Booting Cause
 */
static inline unsigned int is_reboot_after_ramdump(void)
{
    return ((REBOOT_STATUS_REG & REBOOT_CAUSE) == REBOOT_CAUSE_HARDFAULT) ||
        ((REBOOT_STATUS_REG & REBOOT_CAUSE) == REBOOT_CAUSE_SOFTFAULT);
}
#endif

/*!
 * \fn		static inline unsigned int is_reboot_triggered_from_app_for_flashing(void)
 * \brief	This function will check wether devcie reset is triggered by M4 for flashing.
 * \return  Booting Cause
 */
static inline unsigned int is_reboot_triggered_from_app_for_flashing(void)
{
    return ((REBOOT_STATUS_REG & REBOOT_CAUSE) == REBOOT_CAUSE_FLASHING);
}
/*!
 * \fn		static inline void clear_reboot_status(void)
 * \brief	This function will clear the REBOOT_STATUS_REG after finding the BootReason.
 * \return  Resest Reboot Reg
 */

static inline void clear_reboot_status(void)
{
    REBOOT_STATUS_REG &= ~REBOOT_CAUSE;
}

/*!
 * \fn		static int getstring(unsigned char *strbuf, unsigned int size_str,
   \		unsigned int max_secs_delay_between_chars, int debug)
 * \brief	This function will get the Commands from FlashTool for flashing.
 * \param       strbuf   	--- pointer to cmd buffer
 * \param       size_str    --- size of the command
 * \param       max_secs_delay_between_chars     --- the delay between the command characters  
 * \param		debug		--- to enable / disable debug.
 * \return  The length of the Flash tool command.
 */
static int getstring(char *strbuf, unsigned int size_str, unsigned int max_secs_delay_between_chars, int debug)
{
    unsigned int msecs_delay_between_chars, msecs;
    int i = 0;
    if (!max_secs_delay_between_chars)
        msecs = msecs_delay_between_chars = 0xFFFFFFFF;
    else
        msecs = msecs_delay_between_chars = max_secs_delay_between_chars * 1000;

    while(msecs--) 
    {
        strbuf[i] = 0;
        if(uart_rx_available(UART_ID_BOOTLOADER)) //call only if there is data in rx buf
          uart_rx_raw_buf(UART_ID_BOOTLOADER, (uint8_t *)(&strbuf[i]), 1);
        if (strbuf[i]) {
#if 0
            if (debug)
                dbg("strbuf[%d] = %x[%c]\n", i, strbuf[i], strbuf[i]);
#endif
            msecs = msecs_delay_between_chars;
            i++;
            if ((i == size_str) || (strbuf[i - 1] == '\n') || (strbuf[i - 1] == '\r')) {
                strbuf[i] = '\0';
                return i;
            }
        }
        HAL_Delay_Init();
        HAL_DelayUSec(1000);    /* 1 millisecond */
    }
    return i;
}

/*!
 * \fn		unsigned int poll_for_userinput(unsigned int SecDelayTime)
 * \brief	This function will wait for command from the Flash Tool.
 * \param       SecDelayTime   	--- Time to wait for command from The Flash Tool.
 * \param       state --- command state (to identify which string to check for)
 * \param       retry_inifinitely  --- flag to set infinite retry attempts for communicating with Flash Tool.
 * \return  It return the Booting Cause
 */
unsigned int poll_for_userinput(unsigned int SecDelayTime, enum cmdstate state, int retry_infinitely)
{
    unsigned int    retries = MAX_RETRIES;
    unsigned int    expected_strlen;
    char   strbuf[CMD_STRING_LENGTH + 2];
    unsigned char   cmddebug = 0;
    int             ret = 0;
    int             error = 1;

    expected_strlen = CMD_STRING_LENGTH;
#if 0  
    if (is_reboot_triggered_from_app_for_flashing()) {
        /* if reboot triggered from M4APP for flashing there is no need
         * to wait for COMM_START from PC TOOL, since it has already been
         * received in APP
         */
        state = RX_INITIAL_COMMAND;
        dbg("Reboot Triggered from M4APP for flashing\n");
        clear_reboot_status();

    }
#endif
#if 0                                                                               /* SJ : Enable it once FS support is there */
    else if (!is_FS_created()) {
        dbg("File system not created\n");
        SecDelayTime = 0;   /* delay is zero will give max delay time */
        retry_infinitely = 1;
    }
#endif
    
    while(retries-- || retry_infinitely) {
        memset(strbuf, 0, sizeof(strbuf));
        ret = getstring(strbuf, expected_strlen, SecDelayTime, cmddebug);
        dbg_str_str("cmd", strbuf);

        if (state == RX_NO_COMMAND && ret == CMD_STRING_LENGTH) {
            if (strcmp(strbuf, CMD_COMM_START) == 0) {
                dbg_str("enter-command-mode\n");
                state = RX_INITIAL_COMMAND;
                
                retries = MAX_RETRIES;
                error = 1;
                printf(RESP_COMM_START);                                            /* Send response back to the Tool*/            
                continue;
            } else if (error++ >= MAX_ERRORS_IN_MAIN) {
                dbg_str_str("invalid-command, got",strbuf );
                dbg_str_str("expected", CMD_COMM_START );
                if (!retry_infinitely)
                    return 0;
            }
        }

        if (state == RX_INITIAL_COMMAND && ret == CMD_STRING_LENGTH) {
            //state = RX_BOOTMODE_CMD;
            if (strcmp(strbuf, CMD_BOOTMODE_UARTDOWNLOAD) == 0) {
                dbg_str("cmd-rx-bootmode\n");
                state = RX_BOOTMODE_CMD;
                return UPDATE_MODE_UART_DWNLD;

#if defined(RAMDUMP_SUPPORT)
            } else if (strcmp(strbuf, CMD_BOOTMODE_RAMDUMP) == 0) {
            //  dbg("Received valid Handshake for RAMDUMP\n");
                state = RX_BOOTMODE_CMD;
                return UPDATE_MODE_RDUMP ;
#endif
            } else if (error++ >= MAX_ERRORS_IN_MAIN) {
                dbg_str("invalid-boot-command\n");
                
                //state = RX_INITIAL_COMMAND;
                state = RX_NO_COMMAND;          // as part of recovery mechanism, go back to the RX_NO_COMMAND state to receive COMM_START handshake.
                if (!retry_infinitely)
                    return 0;
            }
        }
    }
    return 0;
}

#if 0
/*!
 * \fn		void reboot_if_end_of_transfer(unsigned int SecDelayTime)
 * \brief	This function will Process the Reboot/ Verify Image Command from the Flash Tool.
 * \param   SecDelayTime   	--- Time to wait for command from The Flash Tool.
 */
void reboot_if_end_of_transfer(unsigned int SecDelayTime)
{
    int             ret;
    int             retries = 5;
    int             error = 0;
    unsigned char   strbuf[CMD_STRING_LENGTH + 2];
    char dest_file[32];

    //dbg("waiting for reboot command\n");
    while(retries--)
    {
        memset(strbuf, 0, sizeof(strbuf));
        ret = getstring(strbuf, CMD_STRING_LENGTH, SecDelayTime, 0);
        dbg_str_str("command", strbuf );
        if (strcmp((char const *)strbuf, CMD_DEVICE_REBOOT) == 0)
        {
            printf(RESP_DEVICE_REBOOT);
            //dbg("Received Command to Reboot.....\n");
#if defined(FACTORY_IMAGE)
            if(prepare_for_reboot() < 0)
            {
              dbg("Failed to update boot info.....\n");
            }
#endif

            // wait for sometime, so that UART Tx completes, and Tool gets the ACK we sent!!
            HAL_Delay_Init();
            HAL_DelayUSec(10000);    /* 10 millisecond */

            NVIC_SystemReset();
        }
        else if (strcmp((const char*)strbuf, CMD_VERIFY_IMAGE) == 0)
        {
			// Read image from flash to SRAM
			//dbg("g_CurrentImgSize is %d %d \r\n",g_CurrentImgSize, ret);
            //if(spi_flash_read(g_Currentflashoffset, g_CurrentImgSize, ((UINT8_t*)FREE_SRAM_AREA), NULL) == FlashOperationSuccess) {
            if(spiflash_read_fast(g_Currentflashoffset, g_CurrentImgSize, ((UINT8_t*)FREE_SRAM_AREA), NULL) == FlashOperationSuccess)
            {
		        //dbg("%s binary copied from SPI Flash \r\n",g_currentImgname);	
	        }
	        else
	        {
		        err("Error in loading %s image from flash @ offset %x into SRAM at offset 0x%x \r\n", g_currentImgname,g_Currentflashoffset, FREE_SRAM_AREA);
		        return;
	        }
            // Concatening the Destination file name
            strcpy(dest_file,"VERIFY_");
            strcat(dest_file,(const char*)g_currentImgname);

			//dbg("g_CurrentImgSize 2 is %d \r\n",g_CurrentImgSize);
            // Upload image through UART
			uartUpload((unsigned char*)FREE_SRAM_AREA, dest_file, g_CurrentImgSize);
                        // Send "UPLOADDONE" to confirm upload of file completed
                        printf("UPLOADDONE");
			retries = 5;
                        error = 0;
                        SecDelayTime=5;
		}
        else if (strcmp((const char*)strbuf, CMD_NEXT_IMAGE) == 0)
        {
            error = 0;
            return;
        }
        else if (error++ >= MAX_ERRORS_IN_MAIN)
        {
            //dbg("Invalid Reboot command\n");
            return;
        }
    }
}
#endif

/*!
    \fn     int32_t is_FS_created(void)
    \return 1 if FileSystem exist, 0 otherwise
    \brief  To check if file system exist or not
*/
int32_t is_FS_created(void)
{
    return 1;
}
/*!
  \fn unsigned int findbootmode(void)
  \brief
    This Function determines the Booting Cause
	1. UPDATE_MODE: To enter into Update Mode
        UPDATE_MODE_UART_DWNLD - For UART Flashing
        UPDATE_MODE_FS- If FS does not exist
	2. NORMAL_MODE: Normal Boot Sequence of the Device.
*/
unsigned int findbootmode(void)
{
    /* fill up here in which sequence to check for bootmode */

    unsigned int    user_initiated_bootmode;
    unsigned int    delay = 3;
    enum cmdstate state = RX_NO_COMMAND;
    int retry_infinitely = 0;
    
    if (is_reboot_triggered_from_app_for_flashing()) {
        /* if reboot triggered from M4APP for flashing there is no need
         * to wait for COMM_START from PC TOOL, since it has already been
         * received in APP
         */
        dbg_str("Reboot Triggered from M4APP for flashing\n");
        clear_reboot_status();
        
        state = RX_INITIAL_COMMAND;  
    }
#if 1
    else if (!is_FS_created())
    {
        // FS not created.
        return UPDATE_MODE_FS;
     
    }
#endif
    
	user_initiated_bootmode = poll_for_userinput(delay,state,retry_infinitely);
    if (user_initiated_bootmode)
    {
        return user_initiated_bootmode;
    } 
    else
    {
        return NORMAL_MODE;
    }
    
}

