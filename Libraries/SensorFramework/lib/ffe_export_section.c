/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : ffe_export_section.c
 *    Purpose: 
 *                                                          
 *=========================================================*/
#include "Fw_global_config.h"
#include "QL_FFE_SensorConfig.h"

#if FFE_DRIVERS
struct QL_ExportSection QL_ExportSection;

void init_ffe_expo_section(void)
{
	
}
#endif
