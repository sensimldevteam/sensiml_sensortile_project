/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : QL_SAL.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __QL_SAL_H__
#define __QL_SAL_H__
#include "QL_Trace.h"
#include "QL_SensorCommon.h"

/*
 * Header File to declare the interface for Clients who wish to interact with the Sensors.
 * Each Client should:
 * 1. Attempt to obtain a handle to the Sensor using QL_SAL_SensorOpen()
 *		The client *must* check the return value(QL_STATUS_OK) to see if it could obtain a valid handle (QL_SAL_SensorHandle_t)
 * 2. Use the  QL_SAL_SensorRead() and QL_SAL_SensorIoctl() passing in the obtained handle.
 * 3. Once done with a sensor, release the obtained handle back to the framework using QL_SAL_SensorClose()
 *
 * Description of the IOCTLs available to the client varies with the particular Sensor, and has to be documented
 * for each Sensor Driver. Refer to the individual Sensor Driver documentation.
 */


/* opaque handle for clients for interaction with Sensors */
struct QL_SAL_SensorHandle;
typedef struct QL_SAL_SensorHandle *QL_SAL_SensorHandle_t;


/* Sensor Framework Functions for client interaction with Sensors */
QL_Status QL_SAL_SensorOpen(QL_SAL_SensorHandle_t *handle, unsigned int id);
QL_Status QL_SAL_SensorClose(QL_SAL_SensorHandle_t handle);
QL_Status QL_SAL_SensorRead(QL_SAL_SensorHandle_t handle, void *buf, int size);
QL_Status QL_SAL_SensorIoctl(QL_SAL_SensorHandle_t handle, unsigned int cmd, void *arg);

#endif /* __QL_SAL_H__ */