/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : QL_SDF_Mag.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef SENSORFRAMEWORK_DRIVERS_INC_QL_SDF_MAG_H_
#define SENSORFRAMEWORK_DRIVERS_INC_QL_SDF_MAG_H_
#include "QL_Trace.h"
QL_Status Mag_init();


#endif /* SENSORFRAMEWORK_DRIVERS_INC_QL_SDF_MAG_H_ */
