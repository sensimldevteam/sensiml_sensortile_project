/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : QL_SDF_Gyro.h
 *    Purpose: Contain private structures used by Gyro SDF drivers 
 *             which is compatible with SAL and SensorFrameworks
 *                                                          
 *=========================================================*/


#ifndef SENSORFRAMEWORK_DRIVERS_INC_QL_SDF_GYRO_H_
#define SENSORFRAMEWORK_DRIVERS_INC_QL_SDF_GYRO_H_



/*
 * Private Structure to maintain the states of the sensor, as well as the states of individual instances held by clients *
 * FFE_SENSOR_ID will need to be maintained - common for all instances
 * FFE_INSTANCE_STATE - individually for each instance
 */

#define CONFIG_MAX_FFE_GYRO_INSTANCE 2 //same as accel

/* represents an instance of the sensor */
typedef struct
{
	unsigned char state;
	/* do we need any other variables for each instance ? */
}
QL_Gyro_Ffe_Dev;

/*
 *
 * This is a private structure for the proxy driver running on M4.
 * The real driver is on FFE Side.
 * First member has to be QL_SDF_SensorDrv type as it is known by that type on the framework side.
 *
 */
typedef struct
{
	struct QL_SDF_SensorDrv drvData; 									/* Framework Side Representation */
	/* private stuff below */
	unsigned int  ffe_gyro_id;											/* FFE Side Sensor ID */
	QL_Gyro_Ffe_Dev devInstances[CONFIG_MAX_FFE_GYRO_INSTANCE];		/* instance specific data */
}
QL_Gyro_Ffe_Drv;

typedef struct {
	signed short dataX;
	signed short dataY;
	signed short dataZ;
}Gyro_data;


QL_Status Gyro_init();

#endif /* SENSORFRAMEWORK_DRIVERS_INC_QL_SDF_GYRO_H_ */
