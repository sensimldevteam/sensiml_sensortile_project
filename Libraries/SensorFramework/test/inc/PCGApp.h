/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : PCGApp.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef SENSORFRAMEWORK_TEST_INC_PCGAPP_H_
#define SENSORFRAMEWORK_TEST_INC_PCGAPP_H_

void PCGAppInit(void);
void StartPCG(void); 
void EnablePCG(void);

#endif /* SENSORFRAMEWORK_TEST_INC_PCGAPP_H_ */
