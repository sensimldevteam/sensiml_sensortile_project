/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : BatchSensorDataApp.h
 *    Purpose: 
 *                                                          
 *=========================================================*/


#ifndef SENSORFRAMEWORK_TEST_INC_BATCHSENSORDATAAPP_H_
#define SENSORFRAMEWORK_TEST_INC_BATCHSENSORDATAAPP_H_

QL_Status xStartBatchSensorData(void);
QL_Status xStopBatchSensorData(void);
QL_Status xFlushBatchSensorData(void);

#endif /* SENSORFRAMEWORK_TEST_INC_BATCHSENSORDATAAPP_H_ */
