/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : DoubleTapApp.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef SENSORFRAMEWORK_TEST_INC_DOUBLETAPAPP_H_
#define SENSORFRAMEWORK_TEST_INC_DOUBLETAPAPP_H_

void DoubleTapAppInit(void);
void StartDoubleTap(void);
void EnableDoubleTapApp(void);

#endif /* SENSORFRAMEWORK_TEST_INC_DOUBLETAPAPP_H_ */
