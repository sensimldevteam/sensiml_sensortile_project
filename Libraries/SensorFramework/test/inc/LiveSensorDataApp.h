/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : LiveSensorDataApp.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef SENSORFRAMEWORK_TEST_INC_LIVESENSORDATAAPP_H_
#define SENSORFRAMEWORK_TEST_INC_LIVESENSORDATAAPP_H_

portBASE_TYPE xInitLiveSensorDataHandlerTask();
portBASE_TYPE xInitBatchSensorDataHandlerTask();
void StartLiveSensorApp();

#endif /* SENSORFRAMEWORK_TEST_INC_LIVESENSORDATAAPP_H_ */
