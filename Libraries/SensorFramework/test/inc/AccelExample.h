/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : AccelExample.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef SENSORFRAMEWORK_TEST_INC_ACCELEXAMPLE_H_
#define SENSORFRAMEWORK_TEST_INC_ACCELEXAMPLE_H_
     
#define ACCEL_EXAMPLE_ON_M4_LIVE
//#define ACCEL_EXAMPLE_ON_M4_BATCH
//#define ACCEL_EXAMPLE_ON_FFE_LIVE
//#define ACCEL_EXAMPLE_ON_FFE_BATCH

void InitAccelExample(void);
void StartAccelExample(void);
void EnableAccelExample(void);

#endif /* SENSORFRAMEWORK_TEST_INC_DOUBLETAPAPP_H_ */
