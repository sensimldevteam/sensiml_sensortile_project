/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : DoubleTapLib.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef DOUBLE_TAP_H_
#define DOUBLE_TAP_H_

int CheckDoubleTap_200(float AccelData[]);
int CheckDoubleTap_400(float AccelData[]);

#endif /* DOUBLE_TAP_H_ */
