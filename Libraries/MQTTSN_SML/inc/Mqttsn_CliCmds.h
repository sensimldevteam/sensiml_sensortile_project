#if !defined(MQTTSN_CLICMDS_H)
#define MQTTSN_CLICMDS_H

#include "cli.h"

/* list of cli commands to simulate MQTTSN commands */
extern const struct cli_cmd_entry mqttsn_cmds[] ;
void CLI_ProcessDataIn(uint8_t *pData, uint32_t numBytes);

#endif