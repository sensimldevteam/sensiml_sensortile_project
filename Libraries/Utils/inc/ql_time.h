/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : ql_time.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __QL_TIME_H
#define __QL_TIME_H

#define USE_RTC
#include <time.h>

//struct tm
//{       /* date and time components */
//	int tm_sec;
//	int tm_min;
//	int tm_hour;
//	int tm_mday;
//	int tm_mon;
//	int tm_year;
//	int tm_wday;
//	int tm_yday;
//	int tm_isdst;
//};
//Tim (FAT) typedef signed long time_t;

struct tm *ql_gmtime_r(const time_t *timer, struct tm *tmbuf);
struct tm *ql_localtime_r(const time_t *timer, struct tm *tmbuf);
time_t mktime(struct tm *tmbuf);
time_t ql_time(time_t *t);
int ql_sys_settime(time_t secs_since_epoch);
void periodic_cb(void);
void update_calendar(void);

intptr_t ql_lw_timer_start( void );
int      ql_lw_timer_is_expired( intptr_t token, int msecs );
int      ql_lw_timer_remain( intptr_t token, int msecs );
int      ql_lw_timer_consumed( intptr_t token );


#endif	/* __QL_TIME_H */