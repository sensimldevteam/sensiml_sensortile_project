/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : circularBuffQueue.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __CIRCULAR_QUEUE_H_
#define __CIRCULAR_QUEUE_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef enum
{
    EMPTY = 0,
    FULL = 1,
    BUSY = 2
}BUFF_STATUS;

// Structure to define buffer node in the queue
typedef struct
{
	UINT8_t * pDataBuffPtr; // pointer to buffer
	int status;                  // status whether it is busy or free
} bufferNode;

typedef struct 
{
	int readPtr;       // pointer to read buffer
	int writePtr;      // pointer to write buffer
	int maxElements;   // maximum nu ber of elements in the buffer
	bufferNode	*bufferNodeArr;
}circularBuffQueue;

extern void initCircularBuff(circularBuffQueue *node, UINT8_t *pBuffPtr, size_t buffLen, int chunkSize);
extern UINT8_t* getNextWritePtr(circularBuffQueue *node);
extern UINT8_t* getReadBuffPtr(circularBuffQueue *node);
extern void freeReadBuffPtr(circularBuffQueue *node, unsigned char* pBuffPtr);
extern void printQueue(circularBuffQueue *node);

#endif //__CIRCULAR_QUEUE_H_
