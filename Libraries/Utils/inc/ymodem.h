#if !defined(YMODEM_H)
#define YMODEM_H

#include <stdint.h>

/* http://www.blunk-electronic.de/train-z/pdf/xymodem.pdf */

struct ymodem;

/* tx a byte raw to the uart */
typedef void ymodem_tx_byte_func( struct ymodem *pY, int byte_to_send );

/* wait for a byte & return byte (0x00 to 0xff), if timeout return -1 */
typedef int  ymodem_rx_byte_func( struct ymodem *pY, int msec_timeout );

/* used for ymodem receive file {Future work} */
typedef int  ymodem_fwrite_func( struct ymodem *pYmodem );

/* used for ymodem send, read next block from file */
typedef int  ymodem_fread_func( struct ymodem *pYmodem );

/* pointer to this can be null
 * This function should do something like:  
 *      printf( "%s: 0x%08x\n", msg, value );
 */
typedef void  ymodem_log_func( struct ymodem *pYmodem, const char *msg, uint32_t value );


struct ymodem {
    /* used internally */
    intptr_t magic;
    
	/* for use by the app */
	intptr_t             cookie;
	/* can be set by app to force API to give up */
	int                  give_up;

    /* data buffer Ymodem uses 1K packets */
    uint8_t buffer[1024];
    
    /* current block size */
    int     block_size; /* 128 or 1024 */
    
    /* filename is transmitted in the block 0
     * Filename must be short (less then 100 bytes)
     */
    const char *filename;
	/* Ymodem supports batch operations.
	 * This increases for each file received.
	 */
	int file_number;

    /*
     * Size of the transfer
     */
    uint32_t file_size_bytes;
    
    /* Total bytes transfered (not including block0) */
    uint32_t bytes_transfered;
    
    /* current block number in transit, 0...HUGE */
    uint32_t block_number;
    
	/* used to determine if dup block */
	int      rx_expecting_block;
	int      rx_received_block;

    /* used internally to unget a byte from uart */
    int      unget_c;
    
    /* current retry count */
    int      retry_count;
    
    /* will give up after this many retries */
    int      retry_limit;
    
	/* block0 is has an extra length of time */
	int      tx_block0_limit;
	int      rx_block0_limit;

	/* how long to wait for an ack, before retrying */
	int tx_ack_timeout_msecs;

	/* how long to wait when sending block0 before timeout */
	int tx_start_timeout_msecs;

	/* at end of tx, how long to wait for eot */
	int tx_eot_timeout_msecs;

	/* when receiving - timeout within blocks */
	int rx_short_timeout_msecs;

	/* when receiving for a block to start */
	int rx_long_timeout_msecs;

	/* flush timeout, when flushing input */
	int rx_flush_timeout_msecs;
    
    /* pointer to putchar (RAW do not perform CR LF) */
    ymodem_tx_byte_func *pTxByte;
    
    /* pointer to getchar RAW, with timeout */
    ymodem_rx_byte_func *pRxByte;
    
    /* used during receive operation
     * APP must examine "block_number"
	 * If block_number==0
	 * 1) p->filename points to "p->buffer"
	 *    Filename is stored here.
	 *    This will be overwritten in future calls.
	 * 2) p->file_size_bytes = SOMEVALUE
	 *      if 0, size is unknown
	 *      Else: size is known
	 * Open the file and return 0.
	 * If openfails, return non-zero (-1)
	 *
	 * Else:
	 *
	 * If p->block_size == 0
	 *    Then end of this file transfer.
	 * Else:
	 *    Note: per protocol rules p->block_size may
	 *    change on every call.
	 *
	 *    Write bytes to the file
	 *    if success, return 0
	 *    if error, return -1
     *
     * Return 0 success, non-zero is error
     */
    ymodem_fwrite_func  *pFwrite;
    
    /* Read next block from file, return 0 success, non-zero error */
    ymodem_fread_func   *pFread;
    
    /* can be null, perform logging for debug reasons */
    ymodem_log_func    *pFlog;
};

/* init the structure with defaults */
void ymodem_init( struct ymodem *pYmodem );

/* Step 1: call ymodem_init()
 * Step 2: setup your function pointers
 *         setup cookies as needed
 *         Adjust defaults,
 *
 * Step 3: Call ymodem_send() to transmit the file
 *  Success = return 0.
 *  All others are considered errors
 */
int ymodem_send( struct ymodem *pYmodem );

/* Step 1: call ymodem_init()
 * Step 2: setup your function pointers
 *         setup cookies as needed
 *         Adjust defaults,
 *
 * Step 3: Call ymodem_receive() to receive a file
 *
 * Note: Remote provides the filename.
 */
int ymodem_receive( struct ymodem *pYmodem );

#endif
