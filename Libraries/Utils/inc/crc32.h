#ifndef _CRC32_GCC_TABLE_H
#define _CRC32_GCC_TABLE_H

//Use the gdb crc function directly
extern unsigned int xcrc32 (const unsigned char *buf, int len, unsigned int init);

//Or use the QuickAI Wrapper functions

//Initiliaze the static crc variable
extern void init_crc32(void);

//Compute the crc on the input 8-bit data buffer
extern void update_crc32(const unsigned char *buf, int count);

//Return the current computed value
extern unsigned int get_crc32(void);

//Set the start value for crc 
extern void set_crc32(unsigned int crc);

#endif
