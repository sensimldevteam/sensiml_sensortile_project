/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : common.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef _common_h
#define _common_h

#define FALSE		0
#define TRUE		1

#endif /* !_common_h */
