#include "ymodem.h"
#include "stdio.h"
#include "stdint.h"
#include "assert.h"
#include "string.h"
#include "stdlib.h"

/* http://www.blunk-electronic.de/train-z/pdf/xymodem.pdf */

/*
** NOTE: Currently YMODEM RX is disabled, for several reasons:
**
** 1) The filesystem code prints random debug messages to the console.
**
** 2) These messages interfere with the YMODEM RECEIVE protocol.
**
** 3) For transmit, it's not a problem ...
**
** THUS - Ymodem RX is here, but it is not fully debugged.
*/
#define YMODEM_RX   0


#define SOH 0x01
#define STX 0x02
#define EOT 0x04
#define ACK 0x06
#define NAK 0x15 /* control-V */
#define CAN 0x18 /* control-X */
#define CRC  'C' /* a crc packet is identified by the letter C */

static void magic_check( struct ymodem *pY )
{
    assert( pY->magic == (intptr_t)(&ymodem_init) );
}

void ymodem_init( struct ymodem *pY )
{
    memset( (void *)(pY), 0, sizeof(*pY) );
    pY->magic = (intptr_t)(&ymodem_init);
    pY->retry_limit = 5;
	pY->rx_long_timeout_msecs = 3 * 1000;
	pY->rx_short_timeout_msecs = 250;
	pY->rx_flush_timeout_msecs = 100;
	pY->rx_block0_limit = 15;

	pY->tx_block0_limit = 15;
	pY->tx_ack_timeout_msecs = 3 * 1000;
	pY->tx_start_timeout_msecs = 2 * 1000;
	pY->tx_eot_timeout_msecs = 1 * 1000;
    magic_check(pY);
}

static void log_msg( struct ymodem *pY, const char *msg, uint32_t value )
{
    if( pY->pFlog ){
        (*(pY->pFlog))( pY, msg, value );
    }
}

#if YMODEM_RX
static int checksum(struct ymodem *pY)
{
	int tmp;
	int x;

	tmp = 0;
	for (x = 0; x < pY->block_size; x++) {
		tmp += pY->buffer[x];
	}
	tmp = tmp & 0x0ff;
	// log_msg(pY, "chksum", tmp);
	return tmp;
}
#endif

static void tx_byte( struct ymodem *pY, int c )
{
    c = c & 0x0ff;
    (*(pY->pTxByte))(pY, c );
}

static int rx_ready( struct ymodem *pY, int timeout_msecs )
{
    if( pY->unget_c == -1 ){
        pY->unget_c = (*(pY->pRxByte))(pY,timeout_msecs);
    }

    return (pY->unget_c != -1);
}

static int rx_byte( struct ymodem *pY, int timeout_msecs )
{
    int c;

    c = -1;
    if( rx_ready(pY,timeout_msecs)){
        c = pY->unget_c;
        pY->unget_c = -1;
    }
    return c;
}

static int rx_log( struct ymodem *pY, const char *why, int timeout_msecs )
{
    int c;
	if (pY->give_up) {
		return -1;
	}

    c = rx_byte(pY,timeout_msecs);
    // log_msg( pY, why, c );
    return c;
}

static void rx_flush( struct ymodem *pY )
{
	/* throw things away until timeout occurs */
    while( rx_ready(pY, pY->rx_flush_timeout_msecs) ){
        pY->unget_c = -1;
    }
    pY->unget_c = -1;
}

static uint16_t crc16_citt(struct ymodem *pY)
{
    int  crc;
    int b;
    int x;
    crc = 0;
    for( x = 0 ; x < pY->block_size ; x++ ){

        crc = crc ^ (int) pY->buffer[x] << 8;
        b = 8;
        do
        {
            if (crc & 0x8000)
                crc = crc << 1 ^ 0x1021;
            else
                crc = crc << 1;
        } while(--b)
            ;
    }
    crc = crc & 0x0ffff;
    // log_msg(pY,"crc", crc );
    return (crc);
}

/* send 1 packet (this does not do handshake it just sends) */
static void tx_packet( struct ymodem *pY)
{
	int crc;
    int x;

    log_msg(pY,"tx-block", pY->block_number );
   	crc = crc16_citt(pY);

    /* chooe packet header type */
	if( pY->block_size == 128 ){
        tx_byte(pY,SOH);
	} else {
        tx_byte(pY,STX);
	}
	tx_byte(pY, pY->block_number );
    /* NOTE: Page 16 of spec has typo in block #2 byte */
	tx_byte(pY, ~(pY->block_number) );

	for ( x=0 ; x < pY->block_size ; x++) {
		tx_byte(pY, pY->buffer[x]);
	}
	tx_byte(pY,crc >> 8);
	tx_byte(pY,crc >> 0);
    // log_msg(pY,"tx crcH", crc >> 8);
    // log_msg(pY,"tx crcL", crc >> 0);
}


/* send a block and wait for ack */
static int transfer_block( struct ymodem *pY )
{
    int tmp;

    log_msg(pY,"start-block",pY->block_number);
    pY->retry_count = 0;

    for(;;){
		if (pY->give_up) {
			return -1;
		}

        tx_packet(pY);

        tmp = rx_log(pY,"wait-ack",pY->tx_ack_timeout_msecs);
		if (pY->give_up) {
			return -1;
		}

        /* success? */
        if( tmp == ACK ){
            return 0;
        }

        if( tmp == CAN ){
            log_msg(pY,"cancel",__LINE__);
            return -1;
        }

        /* garbage? */
        log_msg( pY,"tx-block-retry", pY->retry_count );
        pY->retry_count += 1;
        if( pY->retry_count >= pY->retry_limit ){
            return -1;
        }
        log_msg( pY,"tx-block-ack-garbage", tmp );
    }
}


/* ymodem block 0 holds filename and length as ascii
 * total string is less then 128 bytes (protocol requirement)
 */
static void format_block0( struct ymodem *pY )
{
    const char *rd;
    char *wr;
    uint32_t tmp;
    int x;
    char ascii[16];


    pY->block_size = 128;
    /* from the spec
     * The sending program shall send the pathname (file name) in block 0.
     * + The pathname shall be a null terminated ASCII string as described below.
     *
     *   For those who are too lazy to read the entire document:
     *   + Unless specifically requested, only the file name portion is sent.
     *   + No drive letter is sent.
     *   + Systems that do not distinguish between upper and lower case
     *    letters in filenames shall send the pathname in lower case only.
     *  + The receiving program shall use this pathname for the received file
     *    name, unless explicitly overridden.
     *
     * Elsewhere in the spec it says the length is sent as decimal(ascii)
     * after the filename null byte.
     */

    memset( pY->buffer, 0, sizeof(pY->buffer) );
    wr = (char *)(pY->buffer);
    rd = pY->filename;
    /* copy and lower case */
    do {
        tmp = *rd++;
        if( (tmp >= 'A') && (tmp <= 'Z') ){
            tmp = tmp - 'A' + 'a';
        }
        *wr++ = tmp;
    } while( tmp )
        ;

    /* inline: "sprintf( "%d", filesize ); */
    x = 1;
    ascii[0] = '0';
    tmp = pY->file_size_bytes;
    /* this produces in reverse order */
    if( tmp ){
        x--;
        while( tmp ){
            ascii[x] = '0' + (tmp % 10);
            tmp = tmp / 10;
            x++;
        }
    }
    /* point at last char */
    x--;
    /* unreverse */
    while( x >= 0 ){
        *wr++ = ascii[x];
        x--;
    }
}

/* send first block */
static int send_block0( struct ymodem *pY )
{
    int tmp;
    int can_count;
    /* Figure 3 in spec - wait for start request */
    /* transfer must start within 10 seconds */

    pY->retry_count = 0;
    can_count = 0;
    for(;;){
        tmp = rx_log(pY, "wait-block0-start", pY->tx_start_timeout_msecs);

		if (pY->give_up) {
			return -1;
		}

        if( tmp == 'C' ){
            break;
        }
        if( tmp == CAN ){
            can_count++;
            if( can_count >= 3 ){
                pY->give_up = 1;
                log_msg(pY,"cancel",__LINE__);
                return -1;
            }
        }

        pY->retry_count += 1;
        if( pY->retry_count < pY->tx_block0_limit ){
            log_msg(pY,"wait-block0-retry",pY->retry_count);
        } else {
            return -1;
        }
    }

    pY->retry_count = 0;
    pY->block_number = 0;
    format_block0(pY);

    return transfer_block( pY );
}


static int tx_next_block( struct ymodem *pY )
{
    int tmp;

	if (pY->give_up) {
		return -1;
	}

    tmp = pY->file_size_bytes - pY->bytes_transfered;
    log_msg(pY,"bytes-remain",tmp);
    if( tmp <= 0 ){
        return 0;
    }
    if( tmp > 1024 ){
        tmp = 1024;
    }
    if( tmp < 1024 ){
        tmp = 128;
    }

    pY->block_size = tmp;
    tmp = (*(pY->pFread))( pY );
    if( tmp != 0 ){
        log_msg(pY,"read-error",tmp);
        return tmp;
    }

    return transfer_block(pY);
}



static int send_data_blocks( struct ymodem *pY )
{
    int tmp;

    pY->retry_count = 0;
    /* wait start */
    for(;;){
        tmp = rx_log(pY,"wait-data-start",pY->tx_start_timeout_msecs);
		if (pY->give_up) {
			return -1;
		}

        if( tmp == 'C' ){
            break;
        }
        if( tmp == CAN ){
            log_msg( pY, "cancel", __LINE__);
            return -1;
        }

        pY->retry_count++;
        if( pY->retry_count >= pY->retry_limit ){
            return -1;
        }
    }
    log_msg(pY,"start-body",__LINE__);

    /* body */
    /* start with block 1 */
    pY->block_number = 1;

    for(;;){

        /* done? */
        tmp = pY->file_size_bytes - pY->bytes_transfered;
        if( tmp <= 0 ){
            return 0;
        }

        /* tx block */
        tmp = tx_next_block(pY);

		if (pY->give_up) {
			return -1;
		}

        if( tmp != 0 ){
            return tmp;
        }
        pY->bytes_transfered += pY->block_size;
        pY->block_number += 1;
    }
}

/* send the EOT at end of transfer */
static int send_eot( struct ymodem *pY, int expected_rx )
{
    int tmp;

    pY->retry_count = 0;
    for(;;){
		if (pY->give_up) {
			return -1;
		}

        tx_byte( pY, EOT );

        /* we expect a NAK or NAK - see figure 3 of spec */
        tmp = rx_log( pY, "eot-ack", pY->tx_eot_timeout_msecs);
        if( tmp == expected_rx ){
            return 0;
        }

        pY->retry_count += 1;
        if( pY->retry_count >= pY->retry_limit ){
            return -1;
        }
    }
}

static int send_empty_filename(struct ymodem *pY)
{
    /* From the spec:
     * The end of a transfer session shall be signified by a null (empty)
     * pathname, this pathname block shall be acknowledged the same as other
     * pathname blocks.
     */
    memset( pY->buffer, 0, sizeof(pY->buffer) );
    pY->block_size = 128;
    pY->block_number = 0;
    return transfer_block(pY);
}

static void tx_cancel(struct ymodem *pY)
{
	tx_byte(pY, CAN);
	tx_byte(pY, CAN);
	tx_byte(pY, CAN);
}


int ymodem_send( struct ymodem *pY )
{
    int tmp;

    log_msg( pY, "ymodem-send-start",0);

    rx_flush(pY);

    do {
		if (pY->give_up) {
			return -1;
		}

        tmp = send_block0( pY );
        if( tmp != 0 ){
            break;
        }

        tmp = send_data_blocks(pY);
        if( tmp != 0 ){
            break;
        }

        /* first is a NAK */
        tmp = send_eot( pY, NAK );
        if( tmp != 0 ){
            break;
        }

        /* then an ack */
        tmp = send_eot( pY, ACK );
        if( tmp != 0 ){
            break;
        }

        /* then the empty file */
        tmp = send_empty_filename(pY);
        if( tmp != 0 ){
            break;
        }
        log_msg( pY,"ymodem-send-success",0);
        return 0; /* success */
    }while(0)
        ;
    log_msg(pY, "ymodem-send-fail",tmp);
	tx_cancel(pY);

    return tmp;
}

#if YMODEM_RX
static int rx_block_header(struct ymodem *pY)
{
	int tmp;
	tmp = -1;
	switch (tmp) {
	case SOH:
		tmp = 128;
		break;
	case STX:
		tmp = 1024;
		break;
	case EOT:
		tmp = 0;
		break;
    case CAN:
        tmp = 0;
        pY->give_up = 1;
        break;
	default:
		/* garbage? */
		break;
	}
	if (tmp > 0) {
		pY->block_size = tmp;
		tmp = 0;
	}
	return tmp;
}
#endif

#if YMODEM_RX
static int rx_block_num(struct ymodem *pY)
{
	int tmp, tmp2;;
	tmp = rx_byte(pY, pY->rx_short_timeout_msecs);
	if (tmp < 0) {
		return -1;
	}
	tmp2 = rx_byte(pY, pY->rx_short_timeout_msecs);
	if (tmp2 < 0) {
		return -1;
	}
	tmp2 = (tmp2 + 255) & 0x0ff;
	if (tmp != tmp2) {
		return -1;
	}
	pY->rx_received_block = tmp;
	return 0;
}
#endif

#if YMODEM_RX
static int rx_block_body( struct ymodem *pY )
{
	int x, tmp;

	for (x = 0; x < pY->block_size; x++) {
		tmp = rx_byte(pY, pY->rx_short_timeout_msecs);
		if (tmp < 0) {
			return -1;
		}
		pY->buffer[x] = (uint8_t)(tmp & 0x0ff);
	}
	return 0;
}
#endif

#if YMODEM_RX
static int rx_checksum(struct ymodem *pY)
{
	int tmp;
	int tmp2;
	tmp = rx_byte(pY, pY->rx_short_timeout_msecs);
	if (tmp < 0) {
		return tmp;
	}
	tmp2 = checksum(pY);
	if (tmp2 == tmp) {
		/* all is well */
		return 0;
	}
	else {
		log_msg(pY, "chksum-bad-expected", tmp);
		return -1;
	}
}
#endif

#if YMODEM_RX
static int rx_crc(struct ymodem *pY)
{
	int tmp, tmp2;
	tmp = rx_byte(pY,pY->rx_short_timeout_msecs);
	if (tmp < 0) {
		return -1;
	}

	tmp2 = rx_byte(pY,pY->rx_short_timeout_msecs);
	if (tmp2 < 0) {
		return -1;
	}
	tmp = tmp << 8;
	tmp = tmp | tmp2;
	tmp2 = crc16_citt(pY);
	if (tmp == tmp2) {
		return 0;
	}
	log_msg(pY, "crc-expected", tmp);
	return -1;
}
#endif

#if YMODEM_RX
static int rx_block(struct ymodem *pY)
{
	int tmp;

	/* read the block header */
	tmp = rx_block_header(pY);
    if( pY->give_up ){
        goto fail_flush;
    }
	if (tmp < 0) {
		goto fail_flush;
	}
	if (pY->block_size == 0) {
		/* End of transmission */
		tx_byte(pY, EOT);
		return 0;
	}

	tmp = rx_block_num(pY);
	if (tmp < 0) {
		goto fail_flush;
	}

	tmp = rx_block_body(pY);
	if (tmp < 0) {
		goto fail_flush;
	}

	if (pY->block_size == 128) {
		tmp = rx_checksum(pY);
	}
	else {
		tmp = rx_crc(pY);
	}
	if( tmp == 0 ){
		return 0;
	}
fail_flush:
	rx_flush(pY);
	return -1;
}
#endif

#if YMODEM_RX
/* request block */
static int request_block(struct ymodem *pY, int limit )
{
	int tmp;
	pY->retry_count = 0;

	for (;;) {
		tx_byte(pY, 'C');

		tmp = rx_block(pY);
        if( tmp == 0 ){
            return 0;
        }
		if (pY->give_up) {
			return -1;
		}

		pY->retry_count++;
		if (pY->retry_count >= limit ) {
			return -1;
		}
	}
}
#endif

#if YMODEM_RX
/* is this a duplicate block? */
static int rx_is_duplicate(struct ymodem *pY)
{
	int tmp;

	/* what is the previous block number? */
	tmp = (pY->rx_received_block - 1) & 0x0ff;

	/* did we just receive the same one? */
	if (tmp == pY->rx_received_block) {
		log_msg(pY, "dup", __LINE__);
		return 1;
	}
	else {
		return 0;
	}
}
#endif

#if 0
static int rx_one_block(struct ymodem *pY)
{
	int tmp;
	for (;;) {
		if (pY->give_up) {
			return -1;
		}
		tmp = rx_block(pY);

		if (pY->give_up || (tmp < 0)) {
			return -1;
		}
		if (pY->block_size == 0) {
			/* END */
			return 0;
		}

		if (!rx_is_duplicate(pY)) {
			break;
		}
		tx_byte(pY, ACK);
		pY->retry_count++;
		if (pY->retry_count < pY->retry_limit) {
			continue;
		}
		else {
			/* give up */
			return -1;
		}
	}

	if (pY->rx_expecting_block == pY->rx_received_block) {
		return 0;
	}
	log_msg(pY, "blk-seq-error", __LINE__);
	return -1;
}
#endif

#if YMODEM_RX
static int rx_blocks_2_to_N(struct ymodem *pY)
{
	int tmp;
	/* blocks 1..N */
	for (;;) {
		if (pY->give_up) {
			return -1;
		}
		tmp = rx_one_block(pY);
		if (pY->give_up) {
			return -1;
		}

		if (tmp < 0) {
			return -1;
		}

		if (pY->block_size == 0) {
			/* end */
			return 0;
		}

		tmp = (*(pY->pFwrite))(pY);
		if (tmp != 0) {
			pY->give_up = 1;
            return tmp;
        }

		/* reset retrys */
		pY->retry_count = 0;

		pY->block_number += 1;
		pY->rx_expecting_block += 1;
		pY->rx_expecting_block &= 0x0ff;
	}
}
#endif

#if YMODEM_RX
static int receive_blocks_1_to_N(struct ymodem *pY)
{
	int tmp;
	pY->retry_count = 0;

	/* we start with block 1 */
	pY->block_number = 1;
	pY->rx_expecting_block = 1;

	/* we don't use block0 retry because
	 * the process should be running smoothly now
	 */
	tmp = request_block(pY, pY->retry_limit);
	if (tmp == -1) {
		/* failed */
		return tmp;
	}

	if (pY->rx_received_block != pY->rx_expecting_block) {
		/* things are just wrong */
		log_msg(pY, "block-seq-err1", pY->rx_expecting_block);
		return -1;
	}
	/* ok we have block 1 */

	/* save */
	tmp = (*(pY->pFwrite))(pY);

	if (pY->give_up || (tmp < 0)) {
		pY->give_up = 1;
		return -1;
	}
	pY->retry_count = 0;

	/* send ACK after save */
	tx_byte(pY, ACK);

	pY->block_number += 1;
	pY->rx_expecting_block = 0x0ff & (pY->rx_expecting_block + 1);

	tmp = rx_blocks_2_to_N(pY);
	return tmp;
}
#endif

#if YMODEM_RX
static void parse_block0(struct ymodem *pY)
{
	int tmp;
	char *cp;

	pY->file_size_bytes = 0;
	pY->filename = (char *)(&(pY->buffer[0]));

	/* go past the filname */
	cp = strchr(pY->filename, 0);
	cp++;

	/* parse filesize, inline avoid strtol() */
	for (;;) {
		tmp = *cp - '0';
		cp++;
		if( (tmp >= 0) && (tmp < 9) ){
			pY->file_size_bytes *= 10;
			pY->file_size_bytes += tmp;
			continue;
		}
		/* not a digit, we must be at the end of the size */
		break;
	}
	/* we do not parse dates and times and modes */
}
#endif

#if YMODEM_RX
int ymodem_receive(struct ymodem *pY)
{
	int tmp;

   rx_flush( pY );

	/* ymodem is actually batch mode */
	pY->file_number = 0;
	for (;;) {
		if (pY->give_up) {
			return -1;
		}

		/* we want block 0 */
		pY->block_number = 0;
		tmp = request_block(pY, pY->rx_block0_limit);
		if (tmp != 0) {
			return tmp;
		}
		/* ack the block0 */
		tx_byte(pY, ACK);

		if (pY->buffer[0] == 0) {
			/* empty filename means no more */
			break;
		}

		parse_block0(pY);
		log_msg(pY, pY->filename, pY->file_size_bytes);

		/* let client choose filename */
		tmp = (*(pY->pFwrite))(pY);
		if (tmp != 0) {
			pY->give_up = 1;
			return tmp;
		}
		if (pY->give_up) {
			return -1;
		}

		tmp = receive_blocks_1_to_N(pY);
		if (tmp != 0) {
			tx_cancel(pY);
			return tmp;
		}

		/* next file */
		pY->file_number += 1;
	}

	/* done success */
	return 0;

}
#endif
