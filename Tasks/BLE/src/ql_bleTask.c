/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : ql_bleTask.c
 *    Purpose: 
 *                                                          
 *=========================================================*/

#include "Fw_global_config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ql_bleTask.h"
//#include "eoss3_dev.h"
//#include "s3x_clock_hal.h"
//#include "s3x_clock.h"
#include "RtosTask.h"
#include "dbg_uart.h"
#include "qlsh_commands.h"
     
//#include <math.h>
//#include "timers.h"
//#include "QL_Trace.h"

#ifdef __GNUC__

uint8_t __attribute__((section (".BLE_SPI_TX_LOCATION"))) ble_spi_tx_data[256]; //max 256 bytes will be read from this location
uint8_t __attribute__((section (".BLE_SPI_RX_LOCATION"))) ble_spi_rx_data[256];//max 256 bytes will be written to this location

//uint8_t __attribute__((at(0x20063800))) ble_spi_tx_data[256]; //max 256 bytes will be read from this location
//uint8_t __attribute__((at(0x20063C00))) ble_spi_rx_data[256];//max 256 bytes will be written to this location
     
//uint8_t *ble_spi_tx_data = (uint8_t *)(0x20063800);
//uint8_t *ble_spi_rx_data = (uint8_t *)(0x20063C00);
     
#else //IAR
// These addresses are fixed for communication between BLE and S3 using QL SPI
#pragma default_variable_attributes = @ "BLE_SPI_TX_LOCATION"
uint8_t ble_spi_tx_data[256]; //max 256 bytes will be read from this location
#pragma default_variable_attributes =

#pragma default_variable_attributes = @ "BLE_SPI_RX_LOCATION"
uint8_t ble_spi_rx_data[256];//max 256 bytes will be written to this location
#pragma default_variable_attributes =

#endif

#define MAX_BLE_SPI_TX_SIZE   (120) //to avoid frequent interrupts send bigger buffers
#define BLE_RX_BUF_SIZE  (2*256)  //twice size of spi transfer 
#define BLE_TX_BUF_SIZE  (2*256)  //twice size of spi transfer 
static uint8_t ble_tx_data[BLE_TX_BUF_SIZE]; //circular buffer 
static uint8_t ble_rx_data[BLE_RX_BUF_SIZE]; //circular buffer

/* transfer counters for debug */
static int ble_tx_rcount; //tx read count
static int ble_tx_wcount; //tx write count
static int ble_rx_rcount; //rx read count
static int ble_rx_wcount; //rx write count

//for debug
static int ble_tx_pkts;
static int ble_rx_pkts;


xTaskHandle xHandleTaskBLE;
QueueHandle_t BLE_MsgQ;
static int8_t BLETaskReady=0;

/* track how many packets we had to drop */
int ble_drop_counter;

/*
* This module takes the input data for transmit to BLE over QL SPI bus. 
* The data is copied into a circular buffer and a message sent to the BLE Task.
* It is the BLE task responsibility to copy the data to the shared memory
* between BLE and S3 and generate interrupt to BLE.
*
* type = 0 - immediately send
* type = 1 - send bigger chunks to reduce over head 
*/

int SendToBLE( int typecode, int length, const uint8_t *data)
{
    UINT32_t uiErrCode = eQL_ERR_MSG_SEND;
    struct xQ_Packet bleMsg;
    int i;
    int byte_count;

    if( uxQueueSpacesAvailable( BLE_MsgQ ) == 0 ){
      ble_drop_counter++;
      if( DBG_flags & DBG_FLAG_q_drop ){
        dbg_str_int("ble-drop", ble_drop_counter );
      }
      return uiErrCode;
    }
    
    for(i=0; i< length; i++)
    {
        ble_tx_data[ble_tx_wcount++] = *data++;
        if(ble_tx_wcount >= BLE_TX_BUF_SIZE)
            ble_tx_wcount = 0;
    }
    //check how many bytes are available for transmit
    byte_count = ble_tx_wcount - ble_tx_rcount;
    if(byte_count <= 0 )
       byte_count = BLE_TX_BUF_SIZE - byte_count;
    
    if(typecode == SEND_BLE_LARGE_BUFFER && byte_count < MAX_BLE_SPI_TX_SIZE)
        return uiErrCode;

    //send only if the task is ready to receive messages
    if(!CheckBLETaskReady())
        return uiErrCode;
    
    bleMsg.ucCommand = BLE_CMD_DATA_TXMIT;
    if(xQueueSend(BLE_MsgQ, ( void * ) &bleMsg, 100) != pdPASS)
        uiErrCode = eQL_ERR_MSG_SEND;

    ble_tx_pkts++; //dbg check
    if( DBG_flags & DBG_FLAG_ble_background){
       if( (ble_tx_pkts & 0x7F) == 0){ //once every 128pkts
        dbg_str_int("tx-ble", ble_tx_pkts);
       }
    }
    
    return uiErrCode;
}
/*
* This module copies the data from QL SPI buffer shared between BLE and S3.
* The data is copied into a circular buffer and a message is sent the BLE Task.
* The shared data uses a Simple Pkt format.
*    Byte 0 = length 1 - 255
*    Byte 1- 255= Data
*/
void SendBLERxDataReady(void)
{
    struct xQ_Packet bleMsg;
    int i;
    unsigned int count = ble_spi_rx_data[0];
    //if((count != 0) && count < 0xFF)
    for(i=0; i< count; i++)
    {
        ble_rx_data[ble_rx_wcount++] = ble_spi_rx_data[i+1];
        if(ble_rx_wcount >= BLE_RX_BUF_SIZE)
            ble_rx_wcount = 0;
    }
    //dbg_str_int("ble-len", count );
    //dbg_memdump8( 0, ble_spi_rx_data,  32 );
    
    bleMsg.ucCommand = BLE_CMD_DATA_RCVD;

    /* tell ble task layer there is a new BLE packet to process */
    addPktToQueueFromISR_BLE(&bleMsg);
    
    ble_rx_pkts++; //dbg check
    if((ble_tx_pkts & 0x0F) == 0) //once every 16pkts
      dbg_str_int("rx-ble", ble_rx_pkts);
}

/*  Add Msg to the BLE queue from ISR */
UINT32_t addPktToQueueFromISR_BLE(struct xQ_Packet *pxMsg)
{
  
	UINT32_t uiErrCode = eQL_SUCCESS;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if( xQueueSendFromISR( BLE_MsgQ, ( void * )pxMsg, &xHigherPriorityTaskWoken ) != pdPASS )
		uiErrCode = eQL_ERR_MSG_SEND;

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	return uiErrCode;
}
/*
* To prevent sending of messages when BLE Task is not ready. Else msgQ will overflow.
*/
int CheckBLETaskReady(void)
{
  return (int) BLETaskReady;
}
/*
* Write the data to be transmitted to BLE in the shared memory
*
*/
void transmitBLEData(void)
{
    int i = ble_tx_wcount;
    int j = 1;
    int k = ble_tx_rcount; //take only current data

#if 0
    int count = 0;
    while(count < 10)
    {
    //transmit only if the previous interrupt is cleared
    if(INTR_CTRL->SOFTWARE_INTR_2)
          vTaskDelay(2);
        count++;
        
        //if more than 20 msec print a message and give up
        if(count >= 10)
        {
            printf("Error-INTR_2 ");
        return;
        }
    }
#endif
    
    while(k != i)
    {
        ble_spi_tx_data[j++] =  ble_tx_data[k++];
        if(k >= BLE_TX_BUF_SIZE)
            k = 0;
        //send a maximum of predefined bytes at a time
        if(j >= (MAX_BLE_SPI_TX_SIZE+1))
        {
            //Note: It is users responsibility to make sure the data is less than maximum
            // for immediate transfers
          break;
        }
    }
    
    //update readcount only if we have enough data
//    if(j < (MAX_BLE_SPI_TX_SIZE +1))
    if(j == 1)    
        return;
    ble_tx_rcount = k;
    ble_spi_tx_data[0] = j-1; //first byte is the size

    /* tell BLE to go pick up the data */
    GenerateInterruptToBLE();
    
    return;
    
}

//copy the received data into buffer and call the funciton set for processing 
static uint8_t bleRxBuffer[BLE_RX_BUF_SIZE/2]; //note: only max of 256 since SPI buf size set to 256
static void (*bleRxCallbackFunc)(uint8_t *buffer) = 0;;
void receiveBLEData(void)
{
    int i = ble_rx_wcount;
    uint8_t b;
    int j = 0;
    while(ble_rx_rcount != i)
    {
        b = ble_rx_data[ble_rx_rcount++];
        bleRxBuffer[j+1] = b;
        
        if(ble_rx_rcount >= BLE_RX_BUF_SIZE)
            ble_rx_rcount = 0;
        j++;
    }
    if( DBG_flags & DBG_FLAG_ble_details ){
        dbg_str_int("ble-rx-cnt", j);
    }
    bleRxBuffer[0] = j;
    if(bleRxCallbackFunc){
      /*
       * This is where incomming commands are processed by the app
       */
      bleRxCallbackFunc(bleRxBuffer);
    }
    return;
}

//User should set the callback function for received data over BLE
void setBLERxCallback(void (*RxCallbackFunc)(uint8_t *buffer))
{
  bleRxCallbackFunc = RxCallbackFunc;
}

/* BLE task for communicating to nRF51822 */
void bleTaskHandler(void *pParameter)
{
    BaseType_t qret;
    unsigned int bleTaskStop = 0;
    struct xQ_Packet bleMsg;
    
    //int hwState = QL_STATUS_OK;

    ble_tx_rcount = 0;
    ble_tx_wcount = 0;
    ble_rx_rcount = 0;
    ble_rx_wcount = 0;

    wait_ffe_fpga_load();

    //clear the Msg Q buffer 
    memset(&bleMsg, 0, sizeof(bleMsg));

    BLETaskReady = 1; 
   
    while(!bleTaskStop) {

      qret = xQueueReceive(BLE_MsgQ, &bleMsg, BLE_MSGQ_WAIT_TIME);
      configASSERT(qret == pdTRUE);


      switch(bleMsg.ucCommand) {

      case BLE_CMD_START: 
        configASSERT(bleMsg.ucData[0]);
        break;

      case BLE_CMD_TEST: 
        configASSERT(bleMsg.ucData[0]);
        break;

      case BLE_CMD_DATA_TXMIT: 
        //configASSERT(bleMsg.ucData[0]);
        transmitBLEData();
        break;

      case BLE_CMD_DATA_RCVD: 
        //configASSERT(bleMsg.ucData[0]);
        receiveBLEData();
        break;

        
      default :
        printf("BLE: Unknown message\n");
        break;
          
      }
      
    }
                                          
    return; 
                                          
} /* bleTaskHandler() */

/* Setup msg queue and Task Handler for Bluetooth Task */
signed portBASE_TYPE StartRtosTaskBLE( void)
{
    static UINT8_t ucParameterToPass;

    BLETaskReady = 0; 
    /* Create queue for Bluetooth Task */
    BLE_MsgQ = xQueueCreate( BLE_QUEUE_LENGTH, sizeof(struct xQ_Packet) );
    vQueueAddToRegistry( BLE_MsgQ, "Ble_Q" );
    configASSERT( BLE_MsgQ );
    
    /* Create BLE Task */
    xTaskCreate ( bleTaskHandler, "BluetoothTaskHandler", STACK_SIZE_ALLOC(STACK_SIZE_TASK_BLE),  &ucParameterToPass, PRIORITY_TASK_BLE, &xHandleTaskBLE);
    configASSERT( xHandleTaskBLE );
    
    return pdPASS;
}


