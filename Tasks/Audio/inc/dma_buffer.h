/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : dma_buffer.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef HAL_INC_DMA_BUFFER_H_
#define HAL_INC_DMA_BUFFER_H_

#include <stdbool.h>

// Sampling Frequency
#define FS              (16000)

// Base Frame Size gives number of samples in 1 msec
#define BASE_FRAME_SIZE ((FS)/1000)  // 16 samples in 1ms

// Frame size in msec
#define FRAME_SIZE_MS   (15)   // 1 Frame duration = 15 ms

// Frame size gives number of samples in one frame
#define FRAME_SIZE      ((FRAME_SIZE_MS)*(BASE_FRAME_SIZE)) // 1 Frame size = 15*16 = 240 (samples = 480 bytes)


// DMA block size of single buffer size for BLOCKINT to be fired
#define DMA_SINGLE_BUFFER_SIZE (FRAME_SIZE)

// # of DMA buffers

#define DMA_NUMBER_OF_BUFFERS  (15) // 200 bricks =3s of audio data

// DMA multi buffer size for WRAPINT to be fired
#define DMA_MULTI_BUFFER_SIZE ((DMA_NUMBER_OF_BUFFERS) * (DMA_SINGLE_BUFFER_SIZE))

/*
 * DMA buffer where audio data will be captured.
 */
typedef struct dma_buffer_s {
   uint16_t head;
   uint16_t head2;
   int16_t  mem[DMA_NUMBER_OF_BUFFERS][DMA_SINGLE_BUFFER_SIZE];
   uint16_t tail;
   uint16_t tail2;
   uint16_t count;
   bool     overflow;
   uint8_t  spare1;
   uint16_t spare2;
} dma_buffer_t;


extern dma_buffer_t gDmaBuffer;


void init_dmaBuffer();


#endif /* HAL_INC_DMA_BUFFER_H_ */
