/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : ql_audioTask.c
 *    Purpose: 
 *                                                          
 *=========================================================*/
#include "Fw_global_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ql_audioTask.h"
#include "qlsh_commands.h"
#include "eoss3_dev.h"
//#include "eoss3_hal_rcc.h"
#include "eoss3_hal_audio_config.h"  // to remove warnings
#include "eoss3_hal_audio.h"	// to be included after eoss3_hal_audio_config.h
//#include "eoss3_hal_audio_config.h"
#include "dma_buffer.h"
#include "eoss3_hal_leds.h"
#include "s3x_clock_hal.h"
#include "s3x_clock.h"
#include "RtosTask.h"
//#include "ql_apTask.h"
//#include "ql_uart.h"
//#include "voiceControlSim.h"
#include <math.h>
#include "qlsh_commands.h"
#include "dcl_commands.h"
#include "sensor_config.h"
     
//user application should processing this

//#define TIMEOUT_DBG

#ifdef TIMEOUT_DBG
#include <eoss3_hal_pads.h>
#include <eoss3_hal_pad_config.h>
#include <eoss3_hal_gpio.h>
#endif

#include "timers.h"

//#define	PDM_REG_READ

/*
 * TODO: and FIXME:  Create  "struct audio_task_vars"
 * TODO: Remove various voice system stuff from here (simplify code)
 * TODO: Make configurable, ie: 48khz, 16khz, 22.1khz etc.
 * TODO: Make start/stop work
 */

#ifdef ENABLE_VOICE_SOLUTION

xTaskHandle xHandleTaskAudio;
QueueHandle_t xHandleQueueAudio;
int xAudioQueueOverflow;

int set_lpsdoff_mode = 1; //1 = do not turn off DMA


void pdmVoiceConfigVal(enum VOICE_CONFIG_SCENARIO field, uint32_t value);
void pdmCoreConfigVal(enum PDM_CORE_CONFIG_REG field, uint32_t value);

//static Audio_Event_output audioEventPacket = {	.ts = 0, .eventID = SM_VOICE_TRIG, .dummy = 0, .eventValue = 1 };

/*! \struct VoiceSysInfo
    \brief Container format to hold Voice system information
 */
VoiceSysInfo VoiceSystemInfo = {
    .sys_mode = START_UP,
    .sys_state = POWER_DN,
    .lpsd_cnt = 0,
    .ptrAudioBuffStart = (int16_t *)NULL, // &gDmaBuffer.mem[0],
    .AudioBuffLen = DMA_NUMBER_OF_BUFFERS,
    .numBricksIn = 0,
    .numBricksOut = 0,
};

/* GLOBAL VARIABLES */
bool                      audio_interrupt_enabled = false;

//The following variables are updated in efuse library
int Sensory_SFT_En = 0;
int Sensory_SFT_UDT = 0;
int Sensory_EFT_UDP = 0;
int Sensory_FT_UDT_PSCMD = 1;

/* LOCAL STATIC VARIABLES */
static audio_handle_t     audioHandle = NULL;
static enum AudioMode     sys_mode = START_UP;
static enum AudioState sys_state = POWER_DN;
static uint32_t           lpsd_cnt = 0;
static uint16_t           brick_cnts = 0;
static bool               low_power_disabled = false;
static bool               config_done = false;
static bool               enable_audio = true;
static bool               enable_lpsd = true;
static bool               lpsd_trig = false;



static uint16_t           dmaBufCurrIndx = 0;
static uint16_t           dmaBufCnt = 0;
static bool               audio_streaming = false;

static void setupStop( void );


/*
 * This method sleeps the required audio power domains
 */
void sleepAudioPD(void)
{
	const UINT32_t kAD0PD = (1<<0);
	const UINT32_t kAD1PD = (1<<1);
	const UINT32_t kAD3PD = (1<<3);
	const UINT32_t kAD5PD = (1<<5);
	PMU->AUDIO_SW_PD |= (kAD0PD | kAD1PD | kAD3PD | kAD5PD);
	PMU->AUD_SRAM_SW_WU = 0;
}


/*
 * This method wakes up the required audio clocks
 */
void enableAudioClocks(bool enable_audio, bool enable_lpsd ) {
    #if (PDM_MODE_SEL == 0)
    S3x_Clk_Enable(S3X_PDM_LEFT);
    #else
    S3x_Clk_Enable(S3X_PDM_STEREO);
    #endif
    S3x_Clk_Enable(S3X_LPSD);
    return;
}


/**
 * This method shuts down the audio clocks
 */
void disableAudioClocks(void)
{
    #if (PDM_MODE_SEL == 0)
    S3x_Clk_Disable(S3X_PDM_LEFT);
    #else
    S3x_Clk_Disable(S3X_PDM_STEREO);
    #endif
    S3x_Clk_Disable(S3X_LPSD);
}

/*
 * This method wakes up the required audio power domains
 */
void wakeupAudioPD(bool enable_audio, bool enable_lpsd)
{
	// Turn on only the required domains (covers dma, pdm2pcm_left, lpsd_hw, apb_slave)
	const UINT32_t kAD0PD = (1<<0);  // DMAC, AHB
	const UINT32_t kAD1PD = (1<<1);  // PDM2PC_Left, FIFO_0
	const UINT32_t kAD2PD = (1<<2); // PDM2PC_Right, FIFO_1
	const UINT32_t kAD3PD = (1<<3);  // LPSD_HW
	const UINT32_t kAD4PD = (1<<4); // I2S Master
	const UINT32_t kAD5PD = (1<<5);  // APB Slave, Mode detect, Bypass
    
#if (PDM_MODE_SEL == 0) // Mono mode
    PMU->AUD_SRAM_SW_WU = (kAD0PD | kAD1PD | kAD3PD | kAD5PD);
	PMU->AUDIO_SW_PD = (kAD2PD | kAD4PD); // ensure unused PDs off
#else                   // Stereo mode
    PMU->AUD_SRAM_SW_WU = (kAD0PD | kAD1PD | kAD2PD | kAD3PD | kAD5PD);
	PMU->AUDIO_SW_PD = (kAD4PD); // ensure unused PDs off
#endif
}

// setup system for Stop state
static void setupStop( void )
{
    stopAudioCore(audioHandle);
    sleepAudioPD();
    low_power_disabled = false;

    disableAudioClocks();
    disableAudioNvicInterrupts();
    audio_interrupt_enabled = false;
    audio_streaming = false;
    lpsd_trig = false;

    stopDma();

}


void enableAudioNvicInterrupts(uint32_t sys_mode) {
	if( audio_interrupt_enabled == false )
    {
        if(sys_mode == LPSD) {
            enableLPSDInt();
        }
        enableDmaInt();
        audio_interrupt_enabled = true;
	}
}

// setup system for Wait for Trig state
static void setupWaitForTrig( void ) {
    // if first time here after config, come out of low power
    if( low_power_disabled == false ) {
        wakeupAudioPD(enable_audio, enable_lpsd);
        startAudioCore(audioHandle);
        low_power_disabled = true;
        enableAudioClocks(enable_audio, enable_lpsd);
    }
    
    enableAudioNvicInterrupts(sys_mode);
    //enableAudioClocks(enable_audio, enable_lpsd);
    audio_interrupt_enabled = true;
    audio_streaming = false;
    //lpsd_trig = false;
    //stopDma();
    // if lpsd is already ON, proceed with KPD without waiting for next lpsd ON
    if(lpsd_trig ) {
        sys_state = ACTIVE;
    } 
}

// setup system for Wait for Audio state
void setupWaitForAudio( void )
{
    // if first time here after config, come out of low power
    if( low_power_disabled == false ) {
        wakeupAudioPD(enable_audio, enable_lpsd);
        startAudioCore(audioHandle);
        low_power_disabled = true;
        enableAudioClocks(enable_audio, enable_lpsd);
    }


    //enableAudioClocks(enable_audio, enable_lpsd);
    enableAudioNvicInterrupts(sys_mode);


    audio_interrupt_enabled = true;
    if( audio_streaming == false ) {
        //start DMA
        brick_cnts = 0;
        dmaBufCurrIndx = 0;
        dmaBufCnt = 0;
        startDma();
        audio_streaming = true;
    }

}

// process LPSD ON detection event based on system mode
static void processLPSDon( void ) 
{
    //	printf("\n sys_state in LPSD on %d , sys_mode %d \n",sys_state,sys_mode);
    lpsd_cnt++;
    lpsd_trig = true;
    if( SW_ENABLE_AUDIO && sys_mode != START_UP && sys_state == WAIT_FOR_LPSD) {
        sys_state = ACTIVE;
        //start DMA
        brick_cnts = 0;
        dmaBufCurrIndx = 0;
        dmaBufCnt = 0;
        startDma();
    }  
}

// process LPSD OFF detection event based on system mode
static void processLPSDoff( void ) {
    if(set_lpsdoff_mode == 1)
      return;
    lpsd_cnt++;
    //	printf("\n sys_state in LPSD off %d , sys_mode %d \n",sys_state,sys_mode);
    if( sys_mode == LPSD ) {
        stopDma();
        lpsd_trig = false;
        sys_state = WAIT_FOR_LPSD;
    }  
}

void addPktToQueueFromISR_Audio(struct xQ_Packet *pxMsg)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if( xQueueIsQueueFullFromISR( xHandleQueueAudio ) ){
        xAudioQueueOverflow++;
        return;
    }
    
	xQueueSendFromISR( xHandleQueueAudio, ( void * )pxMsg, &xHigherPriorityTaskWoken );

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

/* Audio algorithm task */
void audioTaskHandler(void *pParameter)
{
    //////////////////////////////////////////////
    /* QL variables declaration */
    struct xQ_Packet   receivedMsg;
    BaseType_t         xResult = pdFAIL;
    int16_t           *dmaBufPtr;

    wait_ffe_fpga_load();
    
    wait_for_sensor_config();
    
    /* QL variables declaration END */
    
#ifdef PDM_REG_READ
    uint32_t PdmCoreConfig_PGA_L;                                      // to remove warnings
    uint32_t PdmCoreConfig_PGA_R;
    uint32_t PdmCoreConfig_Soft_Mute;
#endif
    
    
    
    
    
    memset(&receivedMsg,0,sizeof(struct xQ_Packet));
    
    
    // startup Audio system in low power
    //audioHandle = getAudioCoreHandle(DigitalMic_PDM);
    //initAudioCore(audioHandle);
#ifdef QLSRC
    init_interpolate_by_3();	// Initialization call of QLSRC, should be called only once
#endif
    
    
    // Get Voice system into low power mode
    //sys_mode = START_UP; // wait for Host to config operating mode
    //sys_state = LOW_POWER; // start in low power mode till config happens
    
    S3x_Register_Qos_Node(S3X_LPSD);

    if( SW_ENABLE_AUDIO ){
        /* nothing to do */
    } else {
        QL_Audio_Voice_Stop();
    }
    
    //////////////////////////////////////////////////////////////////
    /* QL start of while loop */
    while(1)
    {
        /* QL read message from Message queue */
        xResult = xQueueReceive( xHandleQueueAudio, &( receivedMsg ), portMAX_DELAY);       
        
        if( xResult != pdPASS ) {           
            continue;
        }
            // Process messages/data received from ISR Task (M4 events)
        if(receivedMsg.ucSrc == AUDIO_ISR_MESSAGE)
            {
                /* Parse input message and process the request */
                switch( receivedMsg.ucCommand ) {

            case eCMD_AUDIO_DATA_READY: 
                    configASSERT(0); //  using the new datablock architecture, data processing is not in the control task.
                    break;
                    
                case eCMD_AUDIO_LPSDT: // LPSD On/Off trigger happened
                    if( receivedMsg.ucData[1] == 1 ) { // LPSD ON Trigger
                        //printf("Event: LPSD ON\r\n");
                        if( sys_mode == CONTINUOUS ) {
                            // if streaming audio, ignore lpsd on/ff
                            break;
                        }
                        processLPSDon();
                    }
                    else { // LPSD OFF Trigger
                        //printf("Event: LPSD OFF\r\n");
                        if( sys_mode == CONTINUOUS ) {
                            // if streaming audio, ignore lpsd on/ff
                            break;
                        }
                        processLPSDoff();
                    }
                    if(lpsd_cnt > 255)
                        lpsd_cnt = 1;
                    break;
                    
                default:
                    QL_LOG_WARN("Undef cmd %d\n", receivedMsg.ucCommand);
                    break;
                } /* switch( receivedMsg.ucCommand) from eISR_Task  */
                
            } /* End of Process messages/data received from ISR Task (M4 events) */
            
            
            
            //Also update VoiceInfo if needed
            VoiceSystemInfo.lpsd_cnt = lpsd_cnt;
            VoiceSystemInfo.sys_mode = sys_mode;
            VoiceSystemInfo.sys_state = sys_state;
            
            
    } /* QL end of while loop */  
}

//signed portBASE_TYPE StartRtosTaskAudio( unsigned portBASE_TYPE uxPriority)
signed portBASE_TYPE StartRtosTaskAudio( void)                                  // to remove warnings      uxPriority not used in the function
{
    static UINT8_t ucParameterToPass;
    /* Create queue for AP Task */
    xHandleQueueAudio = xQueueCreate( AUDIO_QUEUE_LENGTH, sizeof(struct xQ_Packet) );
    if(xHandleQueueAudio == 0)
    {
        return pdFAIL;
    }
    vQueueAddToRegistry( xHandleQueueAudio, "Audio_Q" );
    /* Create AP Task */
    xTaskCreate (audioTaskHandler, "AudioTaskHandler", STACK_SIZE_ALLOC(STACK_SIZE_TASK_AUDIO), &ucParameterToPass, PRIORITY_TASK_AUDIO, &xHandleTaskAudio);
    configASSERT( xHandleTaskAudio );
    return pdPASS;
}

void QL_Audio_StartUp(void) {
    // Get Voice system into low power mode
    sys_mode = START_UP; // wait for Host to config operating mode
    sys_state = LOW_POWER; // start in low power mode till config happens
    
    S3x_Register_Qos_Node(S3X_LPSD);
    
    // startup Audio system in low power
    audioHandle = getAudioCoreHandle(DigitalMic_PDM);
    initAudioCore(audioHandle);
}

short QL_Audio_Config(unsigned char ucCfgCmd) 
{
    // rule out unexpected mode/state for this command
    if( sys_state != LOW_POWER || sys_mode != START_UP || config_done == true ) {
//        VLOG_DBG("\n invalid command ,eSUBCMD_AV_VOICE_CONFIG , sys_state = %d ,sys_mode %d ,config_done %d \n",sys_state,sys_mode,config_done);
        return(-1);
    }
    // Else, config the system
    config_done = true;
    enable_audio = true;
    enable_lpsd = true;
    
    switch(ucCfgCmd)
    {
    case eCMD_DATA_AV_VOICE_CFG_LPSD_ONLY: /* Config to LPSD ON/OFF Trigger mode*/
        // LPSD trig only config
        sys_mode = LPSD;
        break;
        
    default:
        //send ERR event back
        config_done = false;
        enable_audio = false;
        enable_lpsd = false;
        return(-1);
        break;
    }
    
    // get out of low power mode and wait for host AP
    if( true ) { //config_done == true ) {
        // after config is done, wait for host for next action
        sys_state = WAIT_FOR_LPSD;
        lpsd_cnt = 0;
        lpsd_trig = false;
        VoiceSystemInfo.numBricksIn = 0;
        VoiceSystemInfo.numBricksOut = 0;
    }
    return(0);
}

void QL_Audio_Voice_Stop(void) {
    /*Ask EOSS3 to stop and go in power save mode*/
    // Stop LPSD/KPT/AUD and get in to Low power mode
    sys_state = LOW_POWER;
    sys_mode = START_UP;
    // stop and go to low power state
    config_done = false;
    enable_audio = false;
    enable_lpsd = false;
    
    setupStop();
    S3x_Clear_Qos_Req(S3X_LPSD, MIN_CPU_FREQ);
}

short QL_Audio_VoiceWaitForTrigger(void) 
{
    /*Put EOSS3 in Trigger(Phrase or Lpsd) detect state*/
    // wait for trig
    sys_state = WAIT_FOR_LPSD;
    setupWaitForTrig();
    
    return(0);
}

BaseType_t getLPSDMode(void)
{
    return set_lpsdoff_mode;
}



#endif 							/* ENABLE_VOICE_SOLUTION */
